require "application_system_test_case"

class ModuleActivitiesTest < ApplicationSystemTestCase
  setup do
    @module_activity = module_activities(:one)
  end

  test "visiting the index" do
    visit module_activities_url
    assert_selector "h1", text: "Module Activities"
  end

  test "creating a Module activity" do
    visit module_activities_url
    click_on "New Module Activity"

    fill_in "Activity", with: @module_activity.activity_id
    fill_in "Fecha activity", with: @module_activity.fecha_activity
    fill_in "Fecha limit payment", with: @module_activity.fecha_limit_payment
    fill_in "Name", with: @module_activity.name
    click_on "Create Module activity"

    assert_text "Module activity was successfully created"
    click_on "Back"
  end

  test "updating a Module activity" do
    visit module_activities_url
    click_on "Edit", match: :first

    fill_in "Activity", with: @module_activity.activity_id
    fill_in "Fecha activity", with: @module_activity.fecha_activity
    fill_in "Fecha limit payment", with: @module_activity.fecha_limit_payment
    fill_in "Name", with: @module_activity.name
    click_on "Update Module activity"

    assert_text "Module activity was successfully updated"
    click_on "Back"
  end

  test "destroying a Module activity" do
    visit module_activities_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Module activity was successfully destroyed"
  end
end
