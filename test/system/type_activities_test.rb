require "application_system_test_case"

class TypeActivitiesTest < ApplicationSystemTestCase
  setup do
    @type_activity = type_activities(:one)
  end

  test "visiting the index" do
    visit type_activities_url
    assert_selector "h1", text: "Type Activities"
  end

  test "creating a Type activity" do
    visit type_activities_url
    click_on "New Type Activity"

    fill_in "Description", with: @type_activity.description
    fill_in "Id", with: @type_activity.id
    fill_in "Name", with: @type_activity.name
    click_on "Create Type activity"

    assert_text "Type activity was successfully created"
    click_on "Back"
  end

  test "updating a Type activity" do
    visit type_activities_url
    click_on "Edit", match: :first

    fill_in "Description", with: @type_activity.description
    fill_in "Id", with: @type_activity.id
    fill_in "Name", with: @type_activity.name
    click_on "Update Type activity"

    assert_text "Type activity was successfully updated"
    click_on "Back"
  end

  test "destroying a Type activity" do
    visit type_activities_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Type activity was successfully destroyed"
  end
end
