require "application_system_test_case"

class ConcertedQualificationsTest < ApplicationSystemTestCase
  setup do
    @concerted_qualification = concerted_qualifications(:one)
  end

  test "visiting the index" do
    visit concerted_qualifications_url
    assert_selector "h1", text: "Concerted Qualifications"
  end

  test "creating a Concerted qualification" do
    visit concerted_qualifications_url
    click_on "New Concerted Qualification"

    fill_in "Assistance first partial", with: @concerted_qualification.assistance_first_partial
    fill_in "Assistance second partial", with: @concerted_qualification.assistance_second_partial
    fill_in "Assistance third partial", with: @concerted_qualification.assistance_third_partial
    fill_in "Assistence final", with: @concerted_qualification.assistence_final
    fill_in "Extraordinary", with: @concerted_qualification.extraordinary
    fill_in "Modality", with: @concerted_qualification.modality_id
    fill_in "Ordinary", with: @concerted_qualification.ordinary
    fill_in "Schedule", with: @concerted_qualification.schedule_id
    fill_in "Score final", with: @concerted_qualification.score_final
    fill_in "Score first partial", with: @concerted_qualification.score_first_partial
    fill_in "Score second partial", with: @concerted_qualification.score_second_partial
    fill_in "Score third partial", with: @concerted_qualification.score_third_partial
    fill_in "Semester", with: @concerted_qualification.semester
    fill_in "Student", with: @concerted_qualification.student_id
    click_on "Create Concerted qualification"

    assert_text "Concerted qualification was successfully created"
    click_on "Back"
  end

  test "updating a Concerted qualification" do
    visit concerted_qualifications_url
    click_on "Edit", match: :first

    fill_in "Assistance first partial", with: @concerted_qualification.assistance_first_partial
    fill_in "Assistance second partial", with: @concerted_qualification.assistance_second_partial
    fill_in "Assistance third partial", with: @concerted_qualification.assistance_third_partial
    fill_in "Assistence final", with: @concerted_qualification.assistence_final
    fill_in "Extraordinary", with: @concerted_qualification.extraordinary
    fill_in "Modality", with: @concerted_qualification.modality_id
    fill_in "Ordinary", with: @concerted_qualification.ordinary
    fill_in "Schedule", with: @concerted_qualification.schedule_id
    fill_in "Score final", with: @concerted_qualification.score_final
    fill_in "Score first partial", with: @concerted_qualification.score_first_partial
    fill_in "Score second partial", with: @concerted_qualification.score_second_partial
    fill_in "Score third partial", with: @concerted_qualification.score_third_partial
    fill_in "Semester", with: @concerted_qualification.semester
    fill_in "Student", with: @concerted_qualification.student_id
    click_on "Update Concerted qualification"

    assert_text "Concerted qualification was successfully updated"
    click_on "Back"
  end

  test "destroying a Concerted qualification" do
    visit concerted_qualifications_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Concerted qualification was successfully destroyed"
  end
end
