require "application_system_test_case"

class EducationLevelsTest < ApplicationSystemTestCase
  setup do
    @education_level = education_levels(:one)
  end

  test "visiting the index" do
    visit education_levels_url
    assert_selector "h1", text: "Education Levels"
  end

  test "creating a Education level" do
    visit education_levels_url
    click_on "New Education Level"

    fill_in "Name", with: @education_level.name
    fill_in "Status", with: @education_level.status
    click_on "Create Education level"

    assert_text "Education level was successfully created"
    click_on "Back"
  end

  test "updating a Education level" do
    visit education_levels_url
    click_on "Edit", match: :first

    fill_in "Name", with: @education_level.name
    fill_in "Status", with: @education_level.status
    click_on "Update Education level"

    assert_text "Education level was successfully updated"
    click_on "Back"
  end

  test "destroying a Education level" do
    visit education_levels_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Education level was successfully destroyed"
  end
end
