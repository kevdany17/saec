require "application_system_test_case"

class TypePaymentsTest < ApplicationSystemTestCase
  setup do
    @type_payment = type_payments(:one)
  end

  test "visiting the index" do
    visit type_payments_url
    assert_selector "h1", text: "Type Payments"
  end

  test "creating a Type payment" do
    visit type_payments_url
    click_on "New Type Payment"

    fill_in "Name", with: @type_payment.name
    click_on "Create Type payment"

    assert_text "Type payment was successfully created"
    click_on "Back"
  end

  test "updating a Type payment" do
    visit type_payments_url
    click_on "Edit", match: :first

    fill_in "Name", with: @type_payment.name
    click_on "Update Type payment"

    assert_text "Type payment was successfully updated"
    click_on "Back"
  end

  test "destroying a Type payment" do
    visit type_payments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Type payment was successfully destroyed"
  end
end
