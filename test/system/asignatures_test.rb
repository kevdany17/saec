require "application_system_test_case"

class AsignaturesTest < ApplicationSystemTestCase
  setup do
    @asignature = asignatures(:one)
  end

  test "visiting the index" do
    visit asignatures_url
    assert_selector "h1", text: "Asignatures"
  end

  test "creating a Asignature" do
    visit asignatures_url
    click_on "New Asignature"

    fill_in "Academic hours", with: @asignature.academic_hours
    fill_in "Asignature name", with: @asignature.asignature_name
    fill_in "Asignature type", with: @asignature.asignature_type
    fill_in "Classroom", with: @asignature.classroom
    fill_in "Credits", with: @asignature.credits
    fill_in "Independent hours", with: @asignature.independent_hours
    fill_in "Key", with: @asignature.key
    fill_in "Semester", with: @asignature.semester
    click_on "Create Asignature"

    assert_text "Asignature was successfully created"
    click_on "Back"
  end

  test "updating a Asignature" do
    visit asignatures_url
    click_on "Edit", match: :first

    fill_in "Academic hours", with: @asignature.academic_hours
    fill_in "Asignature name", with: @asignature.asignature_name
    fill_in "Asignature type", with: @asignature.asignature_type
    fill_in "Classroom", with: @asignature.classroom
    fill_in "Credits", with: @asignature.credits
    fill_in "Independent hours", with: @asignature.independent_hours
    fill_in "Key", with: @asignature.key
    fill_in "Semester", with: @asignature.semester
    click_on "Update Asignature"

    assert_text "Asignature was successfully updated"
    click_on "Back"
  end

  test "destroying a Asignature" do
    visit asignatures_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Asignature was successfully destroyed"
  end
end
