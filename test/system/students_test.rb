require "application_system_test_case"

class StudentsTest < ApplicationSystemTestCase
  setup do
    @student = students(:one)
  end

  test "visiting the index" do
    visit students_url
    assert_selector "h1", text: "Students"
  end

  test "creating a Student" do
    visit students_url
    click_on "New Student"

    fill_in "Actual job", with: @student.actual_job
    fill_in "Civil status", with: @student.civil_status
    fill_in "Cycle", with: @student.cycle_id
    check "Disability" if @student.disability
    fill_in "Disability name", with: @student.disability_name
    check "Foreign language" if @student.foreign_language
    fill_in "Foreign language name", with: @student.foreign_language_name
    fill_in "Inscription type", with: @student.inscription_type
    fill_in "Job center", with: @student.job_center
    fill_in "Job center address", with: @student.job_center_address
    fill_in "Nacionality", with: @student.nacionality
    fill_in "Registration", with: @student.registration
    fill_in "Scolarship", with: @student.scolarship_id
    click_on "Create Student"

    assert_text "Student was successfully created"
    click_on "Back"
  end

  test "updating a Student" do
    visit students_url
    click_on "Edit", match: :first

    fill_in "Actual job", with: @student.actual_job
    fill_in "Civil status", with: @student.civil_status
    fill_in "Cycle", with: @student.cycle_id
    check "Disability" if @student.disability
    fill_in "Disability name", with: @student.disability_name
    check "Foreign language" if @student.foreign_language
    fill_in "Foreign language name", with: @student.foreign_language_name
    fill_in "Inscription type", with: @student.inscription_type
    fill_in "Job center", with: @student.job_center
    fill_in "Job center address", with: @student.job_center_address
    fill_in "Nacionality", with: @student.nacionality
    fill_in "Registration", with: @student.registration
    fill_in "Scolarship", with: @student.scolarship_id
    click_on "Update Student"

    assert_text "Student was successfully updated"
    click_on "Back"
  end

  test "destroying a Student" do
    visit students_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Student was successfully destroyed"
  end
end
