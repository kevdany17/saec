require "application_system_test_case"

class PayConceptsTest < ApplicationSystemTestCase
  setup do
    @pay_concept = pay_concepts(:one)
  end

  test "visiting the index" do
    visit pay_concepts_url
    assert_selector "h1", text: "Pay Concepts"
  end

  test "creating a Pay concept" do
    visit pay_concepts_url
    click_on "New Pay Concept"

    fill_in "Cost", with: @pay_concept.cost
    fill_in "Description", with: @pay_concept.description
    fill_in "Name", with: @pay_concept.name
    click_on "Create Pay concept"

    assert_text "Pay concept was successfully created"
    click_on "Back"
  end

  test "updating a Pay concept" do
    visit pay_concepts_url
    click_on "Edit", match: :first

    fill_in "Cost", with: @pay_concept.cost
    fill_in "Description", with: @pay_concept.description
    fill_in "Name", with: @pay_concept.name
    click_on "Update Pay concept"

    assert_text "Pay concept was successfully updated"
    click_on "Back"
  end

  test "destroying a Pay concept" do
    visit pay_concepts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Pay concept was successfully destroyed"
  end
end
