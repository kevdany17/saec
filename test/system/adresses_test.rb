require "application_system_test_case"

class AdressesTest < ApplicationSystemTestCase
  setup do
    @adress = adresses(:one)
  end

  test "visiting the index" do
    visit adresses_url
    assert_selector "h1", text: "Adresses"
  end

  test "creating a Adress" do
    visit adresses_url
    click_on "New Adress"

    fill_in "City", with: @adress.city
    fill_in "Code postal", with: @adress.code_postal
    fill_in "Colony", with: @adress.colony
    fill_in "Municipality", with: @adress.municipality
    fill_in "Number in", with: @adress.number_in
    fill_in "Number out", with: @adress.number_out
    fill_in "State", with: @adress.state
    fill_in "Status", with: @adress.status
    fill_in "Street", with: @adress.street
    fill_in "User", with: @adress.user_id
    click_on "Create Adress"

    assert_text "Adress was successfully created"
    click_on "Back"
  end

  test "updating a Adress" do
    visit adresses_url
    click_on "Edit", match: :first

    fill_in "City", with: @adress.city
    fill_in "Code postal", with: @adress.code_postal
    fill_in "Colony", with: @adress.colony
    fill_in "Municipality", with: @adress.municipality
    fill_in "Number in", with: @adress.number_in
    fill_in "Number out", with: @adress.number_out
    fill_in "State", with: @adress.state
    fill_in "Status", with: @adress.status
    fill_in "Street", with: @adress.street
    fill_in "User", with: @adress.user_id
    click_on "Update Adress"

    assert_text "Adress was successfully updated"
    click_on "Back"
  end

  test "destroying a Adress" do
    visit adresses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Adress was successfully destroyed"
  end
end
