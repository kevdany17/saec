require "application_system_test_case"

class SchedulesTest < ApplicationSystemTestCase
  setup do
    @schedule = schedules(:one)
  end

  test "visiting the index" do
    visit schedules_url
    assert_selector "h1", text: "Schedules"
  end

  test "creating a Schedule" do
    visit schedules_url
    click_on "New Schedule"

    fill_in "Asignature", with: @schedule.asignature_id
    fill_in "Ciclye", with: @schedule.ciclye_id
    check "Conflict" if @schedule.conflict
    fill_in "Day", with: @schedule.day
    fill_in "Group", with: @schedule.group
    fill_in "Installations", with: @schedule.installations
    fill_in "Study plan", with: @schedule.study_plan_id
    fill_in "Teacher", with: @schedule.teacher_id
    fill_in "Time table", with: @schedule.time_table_id
    click_on "Create Schedule"

    assert_text "Schedule was successfully created"
    click_on "Back"
  end

  test "updating a Schedule" do
    visit schedules_url
    click_on "Edit", match: :first

    fill_in "Asignature", with: @schedule.asignature_id
    fill_in "Ciclye", with: @schedule.ciclye_id
    check "Conflict" if @schedule.conflict
    fill_in "Day", with: @schedule.day
    fill_in "Group", with: @schedule.group
    fill_in "Installations", with: @schedule.installations
    fill_in "Study plan", with: @schedule.study_plan_id
    fill_in "Teacher", with: @schedule.teacher_id
    fill_in "Time table", with: @schedule.time_table_id
    click_on "Update Schedule"

    assert_text "Schedule was successfully updated"
    click_on "Back"
  end

  test "destroying a Schedule" do
    visit schedules_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Schedule was successfully destroyed"
  end
end
