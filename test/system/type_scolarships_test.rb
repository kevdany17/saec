require "application_system_test_case"

class TypeScolarshipsTest < ApplicationSystemTestCase
  setup do
    @type_scolarship = type_scolarships(:one)
  end

  test "visiting the index" do
    visit type_scolarships_url
    assert_selector "h1", text: "Type Scolarships"
  end

  test "creating a Type scolarship" do
    visit type_scolarships_url
    click_on "New Type Scolarship"

    fill_in "Name", with: @type_scolarship.name
    fill_in "Status", with: @type_scolarship.status
    click_on "Create Type scolarship"

    assert_text "Type scolarship was successfully created"
    click_on "Back"
  end

  test "updating a Type scolarship" do
    visit type_scolarships_url
    click_on "Edit", match: :first

    fill_in "Name", with: @type_scolarship.name
    fill_in "Status", with: @type_scolarship.status
    click_on "Update Type scolarship"

    assert_text "Type scolarship was successfully updated"
    click_on "Back"
  end

  test "destroying a Type scolarship" do
    visit type_scolarships_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Type scolarship was successfully destroyed"
  end
end
