require "application_system_test_case"

class LevelStudiesTest < ApplicationSystemTestCase
  setup do
    @level_study = level_studies(:one)
  end

  test "visiting the index" do
    visit level_studies_url
    assert_selector "h1", text: "Level Studies"
  end

  test "creating a Level study" do
    visit level_studies_url
    click_on "New Level Study"

    fill_in "Name", with: @level_study.name
    fill_in "Status", with: @level_study.status
    fill_in "Type", with: @level_study.type
    click_on "Create Level study"

    assert_text "Level study was successfully created"
    click_on "Back"
  end

  test "updating a Level study" do
    visit level_studies_url
    click_on "Edit", match: :first

    fill_in "Name", with: @level_study.name
    fill_in "Status", with: @level_study.status
    fill_in "Type", with: @level_study.type
    click_on "Update Level study"

    assert_text "Level study was successfully updated"
    click_on "Back"
  end

  test "destroying a Level study" do
    visit level_studies_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Level study was successfully destroyed"
  end
end
