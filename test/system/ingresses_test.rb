require "application_system_test_case"

class IngressesTest < ApplicationSystemTestCase
  setup do
    @ingress = ingresses(:one)
  end

  test "visiting the index" do
    visit ingresses_url
    assert_selector "h1", text: "Ingresses"
  end

  test "creating a Ingress" do
    visit ingresses_url
    click_on "New Ingress"

    fill_in "Date", with: @ingress.date
    fill_in "Debit", with: @ingress.debit_id
    fill_in "Type pay", with: @ingress.type_pay
    click_on "Create Ingress"

    assert_text "Ingress was successfully created"
    click_on "Back"
  end

  test "updating a Ingress" do
    visit ingresses_url
    click_on "Edit", match: :first

    fill_in "Date", with: @ingress.date
    fill_in "Debit", with: @ingress.debit_id
    fill_in "Type pay", with: @ingress.type_pay
    click_on "Update Ingress"

    assert_text "Ingress was successfully updated"
    click_on "Back"
  end

  test "destroying a Ingress" do
    visit ingresses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ingress was successfully destroyed"
  end
end
