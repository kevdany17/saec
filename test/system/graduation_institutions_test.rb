require "application_system_test_case"

class GraduationInstitutionsTest < ApplicationSystemTestCase
  setup do
    @graduation_institution = graduation_institutions(:one)
  end

  test "visiting the index" do
    visit graduation_institutions_url
    assert_selector "h1", text: "Graduation Institutions"
  end

  test "creating a Graduation institution" do
    visit graduation_institutions_url
    click_on "New Graduation Institution"

    fill_in "Average", with: @graduation_institution.average
    fill_in "Name", with: @graduation_institution.name
    fill_in "Specialty", with: @graduation_institution.specialty
    fill_in "Status", with: @graduation_institution.status
    fill_in "Student", with: @graduation_institution.student_id
    click_on "Create Graduation institution"

    assert_text "Graduation institution was successfully created"
    click_on "Back"
  end

  test "updating a Graduation institution" do
    visit graduation_institutions_url
    click_on "Edit", match: :first

    fill_in "Average", with: @graduation_institution.average
    fill_in "Name", with: @graduation_institution.name
    fill_in "Specialty", with: @graduation_institution.specialty
    fill_in "Status", with: @graduation_institution.status
    fill_in "Student", with: @graduation_institution.student_id
    click_on "Update Graduation institution"

    assert_text "Graduation institution was successfully updated"
    click_on "Back"
  end

  test "destroying a Graduation institution" do
    visit graduation_institutions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Graduation institution was successfully destroyed"
  end
end
