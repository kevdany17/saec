require "application_system_test_case"

class CostsTest < ApplicationSystemTestCase
  setup do
    @cost = costs(:one)
  end

  test "visiting the index" do
    visit costs_url
    assert_selector "h1", text: "Costs"
  end

  test "creating a Cost" do
    visit costs_url
    click_on "New Cost"

    fill_in "Inscription lic", with: @cost.inscription_lic
    fill_in "Inscription master", with: @cost.inscription_master
    fill_in "Reinscription lic", with: @cost.reinscription_lic
    fill_in "Reinscription master", with: @cost.reinscription_master
    fill_in "Tuition lic", with: @cost.tuition_lic
    fill_in "Tuition master", with: @cost.tuition_master
    click_on "Create Cost"

    assert_text "Cost was successfully created"
    click_on "Back"
  end

  test "updating a Cost" do
    visit costs_url
    click_on "Edit", match: :first

    fill_in "Inscription lic", with: @cost.inscription_lic
    fill_in "Inscription master", with: @cost.inscription_master
    fill_in "Reinscription lic", with: @cost.reinscription_lic
    fill_in "Reinscription master", with: @cost.reinscription_master
    fill_in "Tuition lic", with: @cost.tuition_lic
    fill_in "Tuition master", with: @cost.tuition_master
    click_on "Update Cost"

    assert_text "Cost was successfully updated"
    click_on "Back"
  end

  test "destroying a Cost" do
    visit costs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cost was successfully destroyed"
  end
end
