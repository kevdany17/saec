require "application_system_test_case"

class ConceptPaymentsTest < ApplicationSystemTestCase
  setup do
    @concept_payment = concept_payments(:one)
  end

  test "visiting the index" do
    visit concept_payments_url
    assert_selector "h1", text: "Concept Payments"
  end

  test "creating a Concept payment" do
    visit concept_payments_url
    click_on "New Concept Payment"

    fill_in "Cost", with: @concept_payment.cost
    fill_in "Description", with: @concept_payment.description
    fill_in "Name", with: @concept_payment.name
    click_on "Create Concept payment"

    assert_text "Concept payment was successfully created"
    click_on "Back"
  end

  test "updating a Concept payment" do
    visit concept_payments_url
    click_on "Edit", match: :first

    fill_in "Cost", with: @concept_payment.cost
    fill_in "Description", with: @concept_payment.description
    fill_in "Name", with: @concept_payment.name
    click_on "Update Concept payment"

    assert_text "Concept payment was successfully updated"
    click_on "Back"
  end

  test "destroying a Concept payment" do
    visit concept_payments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Concept payment was successfully destroyed"
  end
end
