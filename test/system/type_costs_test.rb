require "application_system_test_case"

class TypeCostsTest < ApplicationSystemTestCase
  setup do
    @type_cost = type_costs(:one)
  end

  test "visiting the index" do
    visit type_costs_url
    assert_selector "h1", text: "Type Costs"
  end

  test "creating a Type cost" do
    visit type_costs_url
    click_on "New Type Cost"

    fill_in "Description", with: @type_cost.description
    fill_in "Id", with: @type_cost.id
    fill_in "Name", with: @type_cost.name
    click_on "Create Type cost"

    assert_text "Type cost was successfully created"
    click_on "Back"
  end

  test "updating a Type cost" do
    visit type_costs_url
    click_on "Edit", match: :first

    fill_in "Description", with: @type_cost.description
    fill_in "Id", with: @type_cost.id
    fill_in "Name", with: @type_cost.name
    click_on "Update Type cost"

    assert_text "Type cost was successfully updated"
    click_on "Back"
  end

  test "destroying a Type cost" do
    visit type_costs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Type cost was successfully destroyed"
  end
end
