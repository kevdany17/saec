require "application_system_test_case"

class ModuleactivitiesTest < ApplicationSystemTestCase
  setup do
    @moduleactivity = moduleactivities(:one)
  end

  test "visiting the index" do
    visit moduleactivities_url
    assert_selector "h1", text: "Moduleactivities"
  end

  test "creating a Moduleactivity" do
    visit moduleactivities_url
    click_on "New Moduleactivity"

    fill_in "Activity", with: @moduleactivity.activity_id
    fill_in "Fecha activity", with: @moduleactivity.fecha_activity
    fill_in "Fecha limit payment", with: @moduleactivity.fecha_limit_payment
    fill_in "Name", with: @moduleactivity.name
    click_on "Create Moduleactivity"

    assert_text "Moduleactivity was successfully created"
    click_on "Back"
  end

  test "updating a Moduleactivity" do
    visit moduleactivities_url
    click_on "Edit", match: :first

    fill_in "Activity", with: @moduleactivity.activity_id
    fill_in "Fecha activity", with: @moduleactivity.fecha_activity
    fill_in "Fecha limit payment", with: @moduleactivity.fecha_limit_payment
    fill_in "Name", with: @moduleactivity.name
    click_on "Update Moduleactivity"

    assert_text "Moduleactivity was successfully updated"
    click_on "Back"
  end

  test "destroying a Moduleactivity" do
    visit moduleactivities_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Moduleactivity was successfully destroyed"
  end
end
