require "application_system_test_case"

class LevelAcademicsTest < ApplicationSystemTestCase
  setup do
    @level_academic = level_academics(:one)
  end

  test "visiting the index" do
    visit level_academics_url
    assert_selector "h1", text: "Level Academics"
  end

  test "creating a Level academic" do
    visit level_academics_url
    click_on "New Level Academic"

    fill_in "Inscription cost", with: @level_academic.inscription_cost
    fill_in "Name", with: @level_academic.name
    fill_in "Number tuition", with: @level_academic.number_tuition
    fill_in "Rvoe", with: @level_academic.rvoe
    fill_in "Status", with: @level_academic.status
    fill_in "Type", with: @level_academic.type
    click_on "Create Level academic"

    assert_text "Level academic was successfully created"
    click_on "Back"
  end

  test "updating a Level academic" do
    visit level_academics_url
    click_on "Edit", match: :first

    fill_in "Inscription cost", with: @level_academic.inscription_cost
    fill_in "Name", with: @level_academic.name
    fill_in "Number tuition", with: @level_academic.number_tuition
    fill_in "Rvoe", with: @level_academic.rvoe
    fill_in "Status", with: @level_academic.status
    fill_in "Type", with: @level_academic.type
    click_on "Update Level academic"

    assert_text "Level academic was successfully updated"
    click_on "Back"
  end

  test "destroying a Level academic" do
    visit level_academics_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Level academic was successfully destroyed"
  end
end
