require "application_system_test_case"

class SessionModulesTest < ApplicationSystemTestCase
  setup do
    @session_module = session_modules(:one)
  end

  test "visiting the index" do
    visit session_modules_url
    assert_selector "h1", text: "Session Modules"
  end

  test "creating a Session module" do
    visit session_modules_url
    click_on "New Session Module"

    fill_in "Date", with: @session_module.date
    fill_in "Module activity", with: @session_module.module_activity_id
    click_on "Create Session module"

    assert_text "Session module was successfully created"
    click_on "Back"
  end

  test "updating a Session module" do
    visit session_modules_url
    click_on "Edit", match: :first

    fill_in "Date", with: @session_module.date
    fill_in "Module activity", with: @session_module.module_activity_id
    click_on "Update Session module"

    assert_text "Session module was successfully updated"
    click_on "Back"
  end

  test "destroying a Session module" do
    visit session_modules_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Session module was successfully destroyed"
  end
end
