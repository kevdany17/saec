require "application_system_test_case"

class ScolarshipsTest < ApplicationSystemTestCase
  setup do
    @scolarship = scolarships(:one)
  end

  test "visiting the index" do
    visit scolarships_url
    assert_selector "h1", text: "Scolarships"
  end

  test "creating a Scolarship" do
    visit scolarships_url
    click_on "New Scolarship"

    fill_in "Amount", with: @scolarship.amount
    fill_in "Description", with: @scolarship.description
    fill_in "Scolarship type", with: @scolarship.scolarship_type_id
    fill_in "Status", with: @scolarship.status
    click_on "Create Scolarship"

    assert_text "Scolarship was successfully created"
    click_on "Back"
  end

  test "updating a Scolarship" do
    visit scolarships_url
    click_on "Edit", match: :first

    fill_in "Amount", with: @scolarship.amount
    fill_in "Description", with: @scolarship.description
    fill_in "Scolarship type", with: @scolarship.scolarship_type_id
    fill_in "Status", with: @scolarship.status
    click_on "Update Scolarship"

    assert_text "Scolarship was successfully updated"
    click_on "Back"
  end

  test "destroying a Scolarship" do
    visit scolarships_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Scolarship was successfully destroyed"
  end
end
