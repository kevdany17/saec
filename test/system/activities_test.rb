require "application_system_test_case"

class ActivitiesTest < ApplicationSystemTestCase
  setup do
    @activity = activities(:one)
  end

  test "visiting the index" do
    visit activities_url
    assert_selector "h1", text: "Activities"
  end

  test "creating a Activity" do
    visit activities_url
    click_on "New Activity"

    fill_in "Cycle", with: @activity.cycle_id
    fill_in "Description", with: @activity.description
    fill_in "Finish date", with: @activity.finish_date
    fill_in "Inscription", with: @activity.inscription
    fill_in "Name", with: @activity.name
    fill_in "Number tuition", with: @activity.number_tuition
    fill_in "Start date", with: @activity.start_date
    fill_in "Status", with: @activity.status
    fill_in "Total hours", with: @activity.total_hours
    fill_in "Tuition", with: @activity.tuition
    fill_in "Type activity", with: @activity.type_activity_id
    fill_in "Type cost", with: @activity.type_cost_id
    click_on "Create Activity"

    assert_text "Activity was successfully created"
    click_on "Back"
  end

  test "updating a Activity" do
    visit activities_url
    click_on "Edit", match: :first

    fill_in "Cycle", with: @activity.cycle_id
    fill_in "Description", with: @activity.description
    fill_in "Finish date", with: @activity.finish_date
    fill_in "Inscription", with: @activity.inscription
    fill_in "Name", with: @activity.name
    fill_in "Number tuition", with: @activity.number_tuition
    fill_in "Start date", with: @activity.start_date
    fill_in "Status", with: @activity.status
    fill_in "Total hours", with: @activity.total_hours
    fill_in "Tuition", with: @activity.tuition
    fill_in "Type activity", with: @activity.type_activity_id
    fill_in "Type cost", with: @activity.type_cost_id
    click_on "Update Activity"

    assert_text "Activity was successfully updated"
    click_on "Back"
  end

  test "destroying a Activity" do
    visit activities_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Activity was successfully destroyed"
  end
end
