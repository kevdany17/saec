require 'test_helper'

class IngressesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ingress = ingresses(:one)
  end

  test "should get index" do
    get ingresses_url
    assert_response :success
  end

  test "should get new" do
    get new_ingress_url
    assert_response :success
  end

  test "should create ingress" do
    assert_difference('Ingress.count') do
      post ingresses_url, params: { ingress: { date: @ingress.date, debit_id: @ingress.debit_id, type_pay: @ingress.type_pay } }
    end

    assert_redirected_to ingress_url(Ingress.last)
  end

  test "should show ingress" do
    get ingress_url(@ingress)
    assert_response :success
  end

  test "should get edit" do
    get edit_ingress_url(@ingress)
    assert_response :success
  end

  test "should update ingress" do
    patch ingress_url(@ingress), params: { ingress: { date: @ingress.date, debit_id: @ingress.debit_id, type_pay: @ingress.type_pay } }
    assert_redirected_to ingress_url(@ingress)
  end

  test "should destroy ingress" do
    assert_difference('Ingress.count', -1) do
      delete ingress_url(@ingress)
    end

    assert_redirected_to ingresses_url
  end
end
