require 'test_helper'

class AsignaturesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @asignature = asignatures(:one)
  end

  test "should get index" do
    get asignatures_url
    assert_response :success
  end

  test "should get new" do
    get new_asignature_url
    assert_response :success
  end

  test "should create asignature" do
    assert_difference('Asignature.count') do
      post asignatures_url, params: { asignature: { academic_hours: @asignature.academic_hours, asignature_name: @asignature.asignature_name, asignature_type: @asignature.asignature_type, classroom: @asignature.classroom, credits: @asignature.credits, independent_hours: @asignature.independent_hours, key: @asignature.key, semester: @asignature.semester } }
    end

    assert_redirected_to asignature_url(Asignature.last)
  end

  test "should show asignature" do
    get asignature_url(@asignature)
    assert_response :success
  end

  test "should get edit" do
    get edit_asignature_url(@asignature)
    assert_response :success
  end

  test "should update asignature" do
    patch asignature_url(@asignature), params: { asignature: { academic_hours: @asignature.academic_hours, asignature_name: @asignature.asignature_name, asignature_type: @asignature.asignature_type, classroom: @asignature.classroom, credits: @asignature.credits, independent_hours: @asignature.independent_hours, key: @asignature.key, semester: @asignature.semester } }
    assert_redirected_to asignature_url(@asignature)
  end

  test "should destroy asignature" do
    assert_difference('Asignature.count', -1) do
      delete asignature_url(@asignature)
    end

    assert_redirected_to asignatures_url
  end
end
