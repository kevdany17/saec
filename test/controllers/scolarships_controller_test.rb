require 'test_helper'

class ScolarshipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @scolarship = scolarships(:one)
  end

  test "should get index" do
    get scolarships_url
    assert_response :success
  end

  test "should get new" do
    get new_scolarship_url
    assert_response :success
  end

  test "should create scolarship" do
    assert_difference('Scolarship.count') do
      post scolarships_url, params: { scolarship: { amount: @scolarship.amount, description: @scolarship.description, scolarship_type_id: @scolarship.scolarship_type_id, status: @scolarship.status } }
    end

    assert_redirected_to scolarship_url(Scolarship.last)
  end

  test "should show scolarship" do
    get scolarship_url(@scolarship)
    assert_response :success
  end

  test "should get edit" do
    get edit_scolarship_url(@scolarship)
    assert_response :success
  end

  test "should update scolarship" do
    patch scolarship_url(@scolarship), params: { scolarship: { amount: @scolarship.amount, description: @scolarship.description, scolarship_type_id: @scolarship.scolarship_type_id, status: @scolarship.status } }
    assert_redirected_to scolarship_url(@scolarship)
  end

  test "should destroy scolarship" do
    assert_difference('Scolarship.count', -1) do
      delete scolarship_url(@scolarship)
    end

    assert_redirected_to scolarships_url
  end
end
