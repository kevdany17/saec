require 'test_helper'

class TypeCostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @type_cost = type_costs(:one)
  end

  test "should get index" do
    get type_costs_url
    assert_response :success
  end

  test "should get new" do
    get new_type_cost_url
    assert_response :success
  end

  test "should create type_cost" do
    assert_difference('TypeCost.count') do
      post type_costs_url, params: { type_cost: { description: @type_cost.description, id: @type_cost.id, name: @type_cost.name } }
    end

    assert_redirected_to type_cost_url(TypeCost.last)
  end

  test "should show type_cost" do
    get type_cost_url(@type_cost)
    assert_response :success
  end

  test "should get edit" do
    get edit_type_cost_url(@type_cost)
    assert_response :success
  end

  test "should update type_cost" do
    patch type_cost_url(@type_cost), params: { type_cost: { description: @type_cost.description, id: @type_cost.id, name: @type_cost.name } }
    assert_redirected_to type_cost_url(@type_cost)
  end

  test "should destroy type_cost" do
    assert_difference('TypeCost.count', -1) do
      delete type_cost_url(@type_cost)
    end

    assert_redirected_to type_costs_url
  end
end
