require 'test_helper'

class LevelAcademicsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @level_academic = level_academics(:one)
  end

  test "should get index" do
    get level_academics_url
    assert_response :success
  end

  test "should get new" do
    get new_level_academic_url
    assert_response :success
  end

  test "should create level_academic" do
    assert_difference('LevelAcademic.count') do
      post level_academics_url, params: { level_academic: { name: @level_academic.name, rvoe: @level_academic.rvoe, status: @level_academic.status, type: @level_academic.type } }
    end

    assert_redirected_to level_academic_url(LevelAcademic.last)
  end

  test "should show level_academic" do
    get level_academic_url(@level_academic)
    assert_response :success
  end

  test "should get edit" do
    get edit_level_academic_url(@level_academic)
    assert_response :success
  end

  test "should update level_academic" do
    patch level_academic_url(@level_academic), params: { level_academic: { name: @level_academic.name, rvoe: @level_academic.rvoe, status: @level_academic.status, type: @level_academic.type } }
    assert_redirected_to level_academic_url(@level_academic)
  end

  test "should destroy level_academic" do
    assert_difference('LevelAcademic.count', -1) do
      delete level_academic_url(@level_academic)
    end

    assert_redirected_to level_academics_url
  end
end
