require 'test_helper'

class ConceptPaymentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @concept_payment = concept_payments(:one)
  end

  test "should get index" do
    get concept_payments_url
    assert_response :success
  end

  test "should get new" do
    get new_concept_payment_url
    assert_response :success
  end

  test "should create concept_payment" do
    assert_difference('ConceptPayment.count') do
      post concept_payments_url, params: { concept_payment: { cost: @concept_payment.cost, description: @concept_payment.description, name: @concept_payment.name } }
    end

    assert_redirected_to concept_payment_url(ConceptPayment.last)
  end

  test "should show concept_payment" do
    get concept_payment_url(@concept_payment)
    assert_response :success
  end

  test "should get edit" do
    get edit_concept_payment_url(@concept_payment)
    assert_response :success
  end

  test "should update concept_payment" do
    patch concept_payment_url(@concept_payment), params: { concept_payment: { cost: @concept_payment.cost, description: @concept_payment.description, name: @concept_payment.name } }
    assert_redirected_to concept_payment_url(@concept_payment)
  end

  test "should destroy concept_payment" do
    assert_difference('ConceptPayment.count', -1) do
      delete concept_payment_url(@concept_payment)
    end

    assert_redirected_to concept_payments_url
  end
end
