require 'test_helper'

class StudentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @student = students(:one)
  end

  test "should get index" do
    get students_url
    assert_response :success
  end

  test "should get new" do
    get new_student_url
    assert_response :success
  end

  test "should create student" do
    assert_difference('Student.count') do
      post students_url, params: { student: { actual_job: @student.actual_job, civil_status: @student.civil_status, cycle_id: @student.cycle_id, disability: @student.disability, disability_name: @student.disability_name, foreign_language: @student.foreign_language, foreign_language_name: @student.foreign_language_name, inscription_type: @student.inscription_type, job_center: @student.job_center, job_center_address: @student.job_center_address, nacionality: @student.nacionality, registration: @student.registration, scolarship_id: @student.scolarship_id } }
    end

    assert_redirected_to student_url(Student.last)
  end

  test "should show student" do
    get student_url(@student)
    assert_response :success
  end

  test "should get edit" do
    get edit_student_url(@student)
    assert_response :success
  end

  test "should update student" do
    patch student_url(@student), params: { student: { actual_job: @student.actual_job, civil_status: @student.civil_status, cycle_id: @student.cycle_id, disability: @student.disability, disability_name: @student.disability_name, foreign_language: @student.foreign_language, foreign_language_name: @student.foreign_language_name, inscription_type: @student.inscription_type, job_center: @student.job_center, job_center_address: @student.job_center_address, nacionality: @student.nacionality, registration: @student.registration, scolarship_id: @student.scolarship_id } }
    assert_redirected_to student_url(@student)
  end

  test "should destroy student" do
    assert_difference('Student.count', -1) do
      delete student_url(@student)
    end

    assert_redirected_to students_url
  end
end
