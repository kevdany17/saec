require 'test_helper'

class SessionModulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @session_module = session_modules(:one)
  end

  test "should get index" do
    get session_modules_url
    assert_response :success
  end

  test "should get new" do
    get new_session_module_url
    assert_response :success
  end

  test "should create session_module" do
    assert_difference('SessionModule.count') do
      post session_modules_url, params: { session_module: { date: @session_module.date, module_activity_id: @session_module.module_activity_id } }
    end

    assert_redirected_to session_module_url(SessionModule.last)
  end

  test "should show session_module" do
    get session_module_url(@session_module)
    assert_response :success
  end

  test "should get edit" do
    get edit_session_module_url(@session_module)
    assert_response :success
  end

  test "should update session_module" do
    patch session_module_url(@session_module), params: { session_module: { date: @session_module.date, module_activity_id: @session_module.module_activity_id } }
    assert_redirected_to session_module_url(@session_module)
  end

  test "should destroy session_module" do
    assert_difference('SessionModule.count', -1) do
      delete session_module_url(@session_module)
    end

    assert_redirected_to session_modules_url
  end
end
