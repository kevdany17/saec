require 'test_helper'

class ConcertedQualificationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @concerted_qualification = concerted_qualifications(:one)
  end

  test "should get index" do
    get concerted_qualifications_url
    assert_response :success
  end

  test "should get new" do
    get new_concerted_qualification_url
    assert_response :success
  end

  test "should create concerted_qualification" do
    assert_difference('ConcertedQualification.count') do
      post concerted_qualifications_url, params: { concerted_qualification: { assistance_first_partial: @concerted_qualification.assistance_first_partial, assistance_second_partial: @concerted_qualification.assistance_second_partial, assistance_third_partial: @concerted_qualification.assistance_third_partial, assistence_final: @concerted_qualification.assistence_final, extraordinary: @concerted_qualification.extraordinary, modality_id: @concerted_qualification.modality_id, ordinary: @concerted_qualification.ordinary, schedule_id: @concerted_qualification.schedule_id, score_final: @concerted_qualification.score_final, score_first_partial: @concerted_qualification.score_first_partial, score_second_partial: @concerted_qualification.score_second_partial, score_third_partial: @concerted_qualification.score_third_partial, semester: @concerted_qualification.semester, student_id: @concerted_qualification.student_id } }
    end

    assert_redirected_to concerted_qualification_url(ConcertedQualification.last)
  end

  test "should show concerted_qualification" do
    get concerted_qualification_url(@concerted_qualification)
    assert_response :success
  end

  test "should get edit" do
    get edit_concerted_qualification_url(@concerted_qualification)
    assert_response :success
  end

  test "should update concerted_qualification" do
    patch concerted_qualification_url(@concerted_qualification), params: { concerted_qualification: { assistance_first_partial: @concerted_qualification.assistance_first_partial, assistance_second_partial: @concerted_qualification.assistance_second_partial, assistance_third_partial: @concerted_qualification.assistance_third_partial, assistence_final: @concerted_qualification.assistence_final, extraordinary: @concerted_qualification.extraordinary, modality_id: @concerted_qualification.modality_id, ordinary: @concerted_qualification.ordinary, schedule_id: @concerted_qualification.schedule_id, score_final: @concerted_qualification.score_final, score_first_partial: @concerted_qualification.score_first_partial, score_second_partial: @concerted_qualification.score_second_partial, score_third_partial: @concerted_qualification.score_third_partial, semester: @concerted_qualification.semester, student_id: @concerted_qualification.student_id } }
    assert_redirected_to concerted_qualification_url(@concerted_qualification)
  end

  test "should destroy concerted_qualification" do
    assert_difference('ConcertedQualification.count', -1) do
      delete concerted_qualification_url(@concerted_qualification)
    end

    assert_redirected_to concerted_qualifications_url
  end
end
