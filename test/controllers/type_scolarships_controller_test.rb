require 'test_helper'

class TypeScolarshipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @type_scolarship = type_scolarships(:one)
  end

  test "should get index" do
    get type_scolarships_url
    assert_response :success
  end

  test "should get new" do
    get new_type_scolarship_url
    assert_response :success
  end

  test "should create type_scolarship" do
    assert_difference('TypeScolarship.count') do
      post type_scolarships_url, params: { type_scolarship: { name: @type_scolarship.name, status: @type_scolarship.status } }
    end

    assert_redirected_to type_scolarship_url(TypeScolarship.last)
  end

  test "should show type_scolarship" do
    get type_scolarship_url(@type_scolarship)
    assert_response :success
  end

  test "should get edit" do
    get edit_type_scolarship_url(@type_scolarship)
    assert_response :success
  end

  test "should update type_scolarship" do
    patch type_scolarship_url(@type_scolarship), params: { type_scolarship: { name: @type_scolarship.name, status: @type_scolarship.status } }
    assert_redirected_to type_scolarship_url(@type_scolarship)
  end

  test "should destroy type_scolarship" do
    assert_difference('TypeScolarship.count', -1) do
      delete type_scolarship_url(@type_scolarship)
    end

    assert_redirected_to type_scolarships_url
  end
end
