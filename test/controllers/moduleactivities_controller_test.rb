require 'test_helper'

class ModuleactivitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @moduleactivity = moduleactivities(:one)
  end

  test "should get index" do
    get moduleactivities_url
    assert_response :success
  end

  test "should get new" do
    get new_moduleactivity_url
    assert_response :success
  end

  test "should create moduleactivity" do
    assert_difference('Moduleactivity.count') do
      post moduleactivities_url, params: { moduleactivity: { activity_id: @moduleactivity.activity_id, fecha_activity: @moduleactivity.fecha_activity, fecha_limit_payment: @moduleactivity.fecha_limit_payment, name: @moduleactivity.name } }
    end

    assert_redirected_to moduleactivity_url(Moduleactivity.last)
  end

  test "should show moduleactivity" do
    get moduleactivity_url(@moduleactivity)
    assert_response :success
  end

  test "should get edit" do
    get edit_moduleactivity_url(@moduleactivity)
    assert_response :success
  end

  test "should update moduleactivity" do
    patch moduleactivity_url(@moduleactivity), params: { moduleactivity: { activity_id: @moduleactivity.activity_id, fecha_activity: @moduleactivity.fecha_activity, fecha_limit_payment: @moduleactivity.fecha_limit_payment, name: @moduleactivity.name } }
    assert_redirected_to moduleactivity_url(@moduleactivity)
  end

  test "should destroy moduleactivity" do
    assert_difference('Moduleactivity.count', -1) do
      delete moduleactivity_url(@moduleactivity)
    end

    assert_redirected_to moduleactivities_url
  end
end
