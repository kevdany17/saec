require 'test_helper'

class PayConceptsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pay_concept = pay_concepts(:one)
  end

  test "should get index" do
    get pay_concepts_url
    assert_response :success
  end

  test "should get new" do
    get new_pay_concept_url
    assert_response :success
  end

  test "should create pay_concept" do
    assert_difference('PayConcept.count') do
      post pay_concepts_url, params: { pay_concept: { cost: @pay_concept.cost, description: @pay_concept.description, name: @pay_concept.name } }
    end

    assert_redirected_to pay_concept_url(PayConcept.last)
  end

  test "should show pay_concept" do
    get pay_concept_url(@pay_concept)
    assert_response :success
  end

  test "should get edit" do
    get edit_pay_concept_url(@pay_concept)
    assert_response :success
  end

  test "should update pay_concept" do
    patch pay_concept_url(@pay_concept), params: { pay_concept: { cost: @pay_concept.cost, description: @pay_concept.description, name: @pay_concept.name } }
    assert_redirected_to pay_concept_url(@pay_concept)
  end

  test "should destroy pay_concept" do
    assert_difference('PayConcept.count', -1) do
      delete pay_concept_url(@pay_concept)
    end

    assert_redirected_to pay_concepts_url
  end
end
