require 'test_helper'

class ActivitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @activity = activities(:one)
  end

  test "should get index" do
    get activities_url
    assert_response :success
  end

  test "should get new" do
    get new_activity_url
    assert_response :success
  end

  test "should create activity" do
    assert_difference('Activity.count') do
      post activities_url, params: { activity: { cycle_id: @activity.cycle_id, description: @activity.description, finish_date: @activity.finish_date, inscription: @activity.inscription, name: @activity.name, number_tuition: @activity.number_tuition, start_date: @activity.start_date, status: @activity.status, total_hours: @activity.total_hours, tuition: @activity.tuition, type_activity_id: @activity.type_activity_id, type_cost_id: @activity.type_cost_id } }
    end

    assert_redirected_to activity_url(Activity.last)
  end

  test "should show activity" do
    get activity_url(@activity)
    assert_response :success
  end

  test "should get edit" do
    get edit_activity_url(@activity)
    assert_response :success
  end

  test "should update activity" do
    patch activity_url(@activity), params: { activity: { cycle_id: @activity.cycle_id, description: @activity.description, finish_date: @activity.finish_date, inscription: @activity.inscription, name: @activity.name, number_tuition: @activity.number_tuition, start_date: @activity.start_date, status: @activity.status, total_hours: @activity.total_hours, tuition: @activity.tuition, type_activity_id: @activity.type_activity_id, type_cost_id: @activity.type_cost_id } }
    assert_redirected_to activity_url(@activity)
  end

  test "should destroy activity" do
    assert_difference('Activity.count', -1) do
      delete activity_url(@activity)
    end

    assert_redirected_to activities_url
  end
end
