require 'test_helper'

class ModuleActivitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @module_activity = module_activities(:one)
  end

  test "should get index" do
    get module_activities_url
    assert_response :success
  end

  test "should get new" do
    get new_module_activity_url
    assert_response :success
  end

  test "should create module_activity" do
    assert_difference('ModuleActivity.count') do
      post module_activities_url, params: { module_activity: { activity_id: @module_activity.activity_id, fecha_activity: @module_activity.fecha_activity, fecha_limit_payment: @module_activity.fecha_limit_payment, name: @module_activity.name } }
    end

    assert_redirected_to module_activity_url(ModuleActivity.last)
  end

  test "should show module_activity" do
    get module_activity_url(@module_activity)
    assert_response :success
  end

  test "should get edit" do
    get edit_module_activity_url(@module_activity)
    assert_response :success
  end

  test "should update module_activity" do
    patch module_activity_url(@module_activity), params: { module_activity: { activity_id: @module_activity.activity_id, fecha_activity: @module_activity.fecha_activity, fecha_limit_payment: @module_activity.fecha_limit_payment, name: @module_activity.name } }
    assert_redirected_to module_activity_url(@module_activity)
  end

  test "should destroy module_activity" do
    assert_difference('ModuleActivity.count', -1) do
      delete module_activity_url(@module_activity)
    end

    assert_redirected_to module_activities_url
  end
end
