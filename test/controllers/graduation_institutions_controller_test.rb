require 'test_helper'

class GraduationInstitutionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @graduation_institution = graduation_institutions(:one)
  end

  test "should get index" do
    get graduation_institutions_url
    assert_response :success
  end

  test "should get new" do
    get new_graduation_institution_url
    assert_response :success
  end

  test "should create graduation_institution" do
    assert_difference('GraduationInstitution.count') do
      post graduation_institutions_url, params: { graduation_institution: { average: @graduation_institution.average, name: @graduation_institution.name, specialty: @graduation_institution.specialty, status: @graduation_institution.status, student_id: @graduation_institution.student_id } }
    end

    assert_redirected_to graduation_institution_url(GraduationInstitution.last)
  end

  test "should show graduation_institution" do
    get graduation_institution_url(@graduation_institution)
    assert_response :success
  end

  test "should get edit" do
    get edit_graduation_institution_url(@graduation_institution)
    assert_response :success
  end

  test "should update graduation_institution" do
    patch graduation_institution_url(@graduation_institution), params: { graduation_institution: { average: @graduation_institution.average, name: @graduation_institution.name, specialty: @graduation_institution.specialty, status: @graduation_institution.status, student_id: @graduation_institution.student_id } }
    assert_redirected_to graduation_institution_url(@graduation_institution)
  end

  test "should destroy graduation_institution" do
    assert_difference('GraduationInstitution.count', -1) do
      delete graduation_institution_url(@graduation_institution)
    end

    assert_redirected_to graduation_institutions_url
  end
end
