# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.

Rails.application.config.assets.precompile += %w(img/logo2.jpg img/logo.png img/arrows.png teachers.js  reports.js html2canvas.min.js type_activities.js type_costs.js js/pages/readyDashboard.js cycles.js js/get-users.js register_user_activity.js user.js activities.js js/module-activities.js css/layout-reports.css js/vendor/bootstrap.min.js js/plugins.js js/app.js js/pages/uiTables.js js/pages/formsWizard.js js/add-user type_payments.js css/layout-reports2.css)

