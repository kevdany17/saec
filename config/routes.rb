Rails.application.routes.draw do
  
  resources :modalities
  resources :concerted_qualifications
  resources :concept_payments
  resources :installations
  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end
  devise_for :users, controllers: {sessions: 'user/sessions', registrations: 'user/registrations'}
  root to: 'welcome#index'
  #RESOURCE
  resources :schedules
  resources :time_tables
  resources :type_scolarships
  resources :scolarships
  resources :graduation_institutions
  resources :emergency_contacts
  resources :study_plans
  resources :asignatures
  resources :students, path: "estudiantes"
  resources :departments
  resources :session_modules
  resources :tuitions
  resources :costs
  resources :workplaces
  resources :education_levels
  resources :level_studies
  resources :teachers, :path =>'profesores'
  resources :addresses, :path =>'direcciones'
  resources :level_academics, :path =>'niveles/academicos'
  resources :type_activities, :path =>'tipos/actividad'
  resources :type_costs, :path =>'tipos/costo'
  resources :pay_concepts, :path => 'conceptos/pago'
  resources :type_payments, :path => 'tipos/pago'
  resources :ingresses, :path =>'ingresos'
  resources :cycles, :path =>'ciclos'
  resources :debits, :path =>'adeudos'
  resources :activities, :path =>'actividades'
  resources :module_activities, :path =>'modulos/actividades'
  resources :administrators
  resources :assistants, :path => 'asistentes'
  #GET
  #get '/reporte/alumno', to: 'reports#students'
  get '/registrar', to: 'user#new_user'
  get '/ver/adeudos/actividades/', to: 'debits#get_user_debits', as: 'show_debits'
  get '/usuarios', to: 'user#get_users'
  get '/actividad/registro', to: 'register_user_activity#index' , as: 'register'
  get '/actividad/lista', to: 'register_user_activity#get_activity'
  post '/actividad/registrar/usuario', to: 'register_user_activity#saveUserInActivity'
  post '/pagar/adeudo', to: 'ingresses#add' 
  get '/reporte/inscrito/actividad/:id', to: 'reports#payment_report', as: 'recibo'
  get '/reporte/actividad/:id/inscritos', to: 'reports#assistants_activity_report', as: 'inscritos'
  get '/constancias/actividades/:id', to: 'reports#constancy_ac'
  post '/cargar/materias', to: 'asignatures#charge_asignatures_from_file'
  #POST
  post "/graphql", to: "graphql#execute"
  post '/registrar', to: 'user#create_user'
  post '/docente/documentos/:type', to: 'teachers#upload_files'
  post '/docente/registro/', to: 'teachers#create'
  patch '/docente/actualizar/', to: 'teachers#update'
  get '/reporte/ingresos/:id', to: 'reports#ingress_activity_report'
  get '/reporte/inscripcion/:id', to: 'reports#inscription_report'
  get '/formato/concentrado/:id', to: 'formats#grade_sheets'
  get '/formato/asistencia/:id', to: 'formats#assistant_sheets'
  get '/formato/tira/materias/:id', to: 'formats#schedule_assignment'
  get '/formato/compromiso/pagos/:id', to: 'formats#commitment_payment'
  get '/profesor/:id', to: 'teachers#get_teacher'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
