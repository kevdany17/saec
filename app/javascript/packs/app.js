import Vue from "vue/dist/vue.esm";
import VueRouter from "vue-router";
import Vuelidate from "vuelidate";

// Import component
import Loading from "vue-loading-overlay";
// Import stylesheet
import "vue-loading-overlay/dist/vue-loading.css";

import { ClientTable, Event } from "vue-tables-2";
import Routes from "./router.js";
Vue.use(VueRouter);
Vue.use(ClientTable);
Vue.use(Vuelidate);
// Init plugin
Vue.use(Loading);
//Vue.use(Swal);
// We create the router instance here.
const router = new VueRouter({
  //mode: 'history',
  routes: Routes
});
//
document.addEventListener("DOMContentLoaded", () => {
  const app = new Vue({
    el: "#app",
    router,
    data: {},
    methods: {
      recargar() {
        window.location.reload();
      }
    },
    mounted() {
      $(".menu1").on("click", function() {
        document.getElementById("menu-academico").click();
      });
      $(".menu-ingresar").on("click", function() {
        document.getElementById("menu-inscription").click();
      });
      $(".menu-ingresar2").on("click", function() {
        document.getElementById("menu-inscription2").click();
      });
      $(".menu-ingresar3").on("click", function() {
        document.getElementById("menu-inscription3").click();
      });
      $(".menu-planes").on("click", function() {
        document.getElementById("menu-planes").click();
      });
    }
  });
// setTimeout(function(){
  // const home = new Vue({
  //   el: "#home",
  //   template:`<HomeIndex v-if="show"></HomeIndex>`,
  //   data:{
  //     show:true,
  //   },
  //   mounted:function(){
  //     if(window.location.href.includes("/home")){
  //       console.log("/home");
  //       this.show = false;
  //     }else{
  //       this.show = true;
  //     }
  //   },
  //   updated:function(){
  //     if(window.location.href.includes("/home")){
  //       this.show = false;
  //     }else{
  //       this.show = true;
  //     }
  //   },
  //   components:{
  //     HomeIndex
  //   }
  // });
// },1000);
  

});
//
//
//
// If the project is using turbolinks, install 'vue-turbolinks':
//
// yarn add vue-turbolinks
//
// Then uncomment the code block below:
//
// import TurbolinksAdapter from 'vue-turbolinks'
// import Vue from 'vue/dist/vue.esm'
// import App from '../app.vue'
// Vue.use(TurbolinksAdapter)
// document.addEventListener('turbolinks:load', () => {
//   const app = new Vue({
//     el: '#hello',
//     data: () => {
//       return {
//         message: "Can you say hello?"
//       }
//     },
//     components: { App }
//   })
// })
