import Actividadregistro from "./components/activities/RegisterUser.vue";
import RegistroProfesor from "./components/teachers/RegisterTeacher.vue";
import ActualizarProfesor from "./components/teachers/UpdateTeacher.vue";
import VerProfesor from "./components/teachers/View.vue";
import ListaAsistencia from "./components/reports/AttendanceActivity.vue";
import Lista from "./components/reports/AttendanceActivityList.vue";
import ActivitiesIndex from "./components/activities/Index.vue";
import ActivityView from "./components/activities/View.vue";
import ActivityCreate from "./components/activities/Create.vue";
import EditProfile from "./components/users/EditProfile.vue";
import ActivityEdit from "./components/activities/Edit.vue";
import AssistantsList from "./components/users/AssistantsList.vue";
import AdministratorsIndex from "./components/administrators/Index.vue";
import AdministratorsView from "./components/administrators/View.vue";
import AdministratorsEdit from "./components/administrators/Edit.vue";
import AdministratorsCreate from "./components/administrators/New.vue";
import UsersDebits from "./components/debits/UsersDebits.vue";
import AcademicLevelIndex from "./components/academic_levels/Index.vue";
import AcademicLevelCreate from "./components/academic_levels/Create.vue";
import AcademicLevelView from "./components/academic_levels/View.vue";
import AcademicLevelEdit from "./components/academic_levels/Edit.vue";
import IngresosActividad from "./components/reports/IngressesActivity.vue";
import ReporteActividadIngresos from "./components/reports/IngressesActivityReport.vue";
import StudentInscription from "./components/inscription/StudentInscription.vue";

//Importación de componentes para Asiganturas
import AsignatureIndex from "./components/asignatures/Index.vue";
import AsignatureCreate from "./components/asignatures/Create.vue";
import AsignatureView from "./components/asignatures/View.vue";
import AsignatureEdit from "./components/asignatures/Edit.vue";

//Importación de componentes para Planes de Estudio
import StudyPlanIndex from "./components/study_plans/Index.vue";
import StudyPlanCreate from "./components/study_plans/Create.vue";
import StudyPlanView from "./components/study_plans/View.vue";
import StudyPlanEdit from "./components/study_plans/Edit.vue";
import HomeIndex from "./components/home/Index.vue";

//Importación de componentes de Estudiantes
import StudentsList from "./components/students/Index.vue";
import StudentView from "./components/students/View.vue";
import StudentInstitution from "./components/students/EditInstitution.vue";
import EditEmergencyContact from "./components/students/EditEmergencyContact.vue";
import EditAcademics from "./components/students/EditAcademic.vue";
import EditGenerals from "./components/students/EditGenerals.vue";

//Importación de componentes para Becas
import ScolarshipIndex from "./components/scolarships/Index.vue";
import ScolarshipCreate from "./components/scolarships/New.vue";
import ScolarshipView from "./components/scolarships/View.vue";
import ScolarshipEdit from "./components/scolarships/Edit.vue";
import ScolarshipApplication from "./components/scolarships/Application.vue";

//Importación de componentes para Tipos de Becas
import TypeScolarshipsIndex from "./components/scolarship_types/Index.vue";
import TypeScolarshipsCreate from "./components/scolarship_types/New.vue";
import TypeScolarshipsView from "./components/scolarship_types/View.vue";
import TypeScolarshipsEdit from "./components/scolarship_types/Edit.vue";

//Importación de Horarios
import ScheduleIndex from './components/schedules/Index.vue';
import ScheduleCreate from './components/schedules/Create.vue';
import ScheduleEdit from './components/schedules/Edit.vue';
import ScheduleView from './components/schedules/View.vue';
import CreateSchedule from './components/students/CreateSchedule.vue';

//Importación de componentes para Direcciones
import AddressEdit from './components/address/Edit.vue';

//importacion de componentes para rango de horas
import TimeTablesIndex from './components/time_tables/Index.vue'
import TimeTablesCreate from './components/time_tables/Create.vue'
import TimeTablesEdit from './components/time_tables/Edit.vue'
import TimeTablesView from './components/time_tables/View.vue'

//importacion de componentes de Instalaciones
import InstallationIndex from './components/installations/Index.vue'
import InstallationCreate from './components/installations/Create.vue'
import InstallationEdit from './components/installations/Edit.vue'
import InstallationView from './components/installations/View.vue'
// importacion de componentes de ciclos
import CycleIndex from './components/cycle/Index.vue'
import CycleEdit from './components/cycle/Edit.vue'
import CycleView from './components/cycle/View.vue'
import CycleCreate from './components/cycle/Create.vue'
// importacion de componentes de conceptos de pagos
import ConceptPaymentIndex from './components/concept_payments/Index.vue'
import ConceptPaymentEdit from './components/concept_payments/Edit.vue'
import ConceptPaymentView from './components/concept_payments/View.vue'
import ConceptPaymentCreate from './components/concept_payments/Create.vue'

// importación de componentes para listas y concentrados de calificaciones
import GradeSheets from './components/asignatures/GradeSheets.vue'
import AsignatureAssitantList from './components/asignatures/AsignatureAssitantList.vue'
import ConcertedQualificationIndex from './components/concerted_qualifications/Index.vue';

export default [
  // Redirects to /route-one as the default route.
  // {
  //   path: '/',
  //   redirect: '/home'
  // },
  {
    path: "/home",
    component: HomeIndex,
    props: true,
  },
  {
    path: "/planes",
    component: StudyPlanIndex,
  },
  {
    path: "/planes/crear",
    component: StudyPlanCreate,
  },
  {
    path: "/planes/:id",
    component: StudyPlanView,
    props: true,
  },
  {
    path: "/planes/editar/:id",
    component: StudyPlanEdit,
    props: true,
  },
  {
    path: "/asignaturas",
    component: AsignatureIndex,
  },
  {
    path: "/asignaturas/crear",
    component: AsignatureCreate,
  },
  {
    path: "/asignaturas/ver/:id",
    component: AsignatureView,
    props: true,
  },
  {
    path: "/asignaturas/editar/:id",
    component: AsignatureEdit,
    props: true,
  },
  {
    path: "/actividad/registro",
    component: Actividadregistro,
    // Children is just another route definition of sub-routes.
    // children: [
    //   {
    //     // Note: No leading slash. This can trip people up sometimes.
    //     path: 'route-one-child',
    //     component: RouteOneChild
    //   }
    // ]
  },
  {
    path: "/reporte/ingresos",
    component: IngresosActividad,
  },
  {
    path: "/niveles/academicos",
    component: AcademicLevelIndex,
  },
  {
    path: "/reporte/ingresos/:id",
    component: ReporteActividadIngresos,
    props: true,
  },
  {
    path: "/niveles/academicos/crear",
    component: AcademicLevelCreate,
  },
  {
    path: "/niveles/academicos/ver/:id",
    component: AcademicLevelView,
    props: true,
  },
  {
    path: "/niveles/academicos/editar/:id",
    component: AcademicLevelEdit,
    props: true,
  },
  {
    path: "/administrativos/crear",
    component: AdministratorsCreate,
  },
  {
    path: "/ver/adeudos/actividades",
    component: UsersDebits,
  },
  {
    path: "/administrativos",
    component: AdministratorsIndex,
  },
  {
    path: "/administrativos/ver/:id",
    component: AdministratorsView,
    props: true,
  },
  {
    path: "/administrativos/editar/:id",
    component: AdministratorsEdit,
    props: true,
  },
  {
    path: "/ver/asistentes",
    component: AssistantsList,
  },
  {
    path: "/estudiantes",
    component: StudentsList,
  },
  {
    path: "/estudiantes/ver/:id",
    component: StudentView,
    props: true,
  },
  {
    path: "/estudiantes/instituto/editar/:id",
    component: StudentInstitution,
    props: true,
  },
  {
    path: "/actividades",
    component: ActivitiesIndex,
  },
  {
    path: "/actividades/:id",
    component: ActivityView,
    props: true,
  },
  {
    path: "/crear/actividad",
    component: ActivityCreate,
  },
  {
    path: "/editar/actividad/:id",
    component: ActivityEdit,
    props: true,
  },

  {
    path: "/listas/asistencia",
    component: ListaAsistencia,
  },
  {
    path: "/listas",
    component: Lista,
  },
  {
    path: "/docente/registro",
    component: RegistroProfesor,
  },
  {
    path: "/docente/actualizar/:id",
    component: ActualizarProfesor,
    props: true,
  },
  {
    path: "/docente/ver/:id",
    component: VerProfesor,
    props: true,
  },
  {
    path: "/editar/perfil/:id",
    component: EditProfile,
    props: true,
  },
  {
    path: "/inscripcion",
    component: StudentInscription,
  },
  {
    path: "/becas",
    component: ScolarshipIndex,
  },
  {
    path: "/becas/crear",
    component: ScolarshipCreate,
  },
  {
    path: "/becas/solicitar",
    component: ScolarshipApplication,
  },
  {
    path: "/becas/ver/:id",
    component: ScolarshipView,
    props: true,
  },
  {
    path: "/becas/editar/:id",
    component: ScolarshipEdit,
    props: true,
  },
  {
    path: "/tipos/becas",
    component: TypeScolarshipsIndex,
  },
  {
    path: "/tipos/becas/crear",
    component: TypeScolarshipsCreate,
  },
  {
    path: "/tipos/becas/editar/:id",
    component: TypeScolarshipsEdit,
    props: true,
  },
  {
    path: "/tipos/becas/ver/:id",
    component: TypeScolarshipsView,
    props: true,
  },
  {
    path: "/direcciones/editar/:id",
    component: AddressEdit,
    props: true,
  },
  {
    path: "/estudiantes/emergencia/editar/:id",
    component: EditEmergencyContact,
    props: true,
  },
  {
    path: "/estudiantes/academicos/editar/:id",
    component: EditAcademics,
    props: true,
  },
  {
    path: "/estudiantes/generales/editar/:id",
    component: EditGenerals,
    props: true,
  },
  {
    path: "/horarios",
    component: ScheduleIndex,
    props: true
  },
  {
    path: '/horarios/crear',
    component: ScheduleCreate,
    props: false
  },
  {
    path: '/horarios/editar/:id',
    component: ScheduleEdit,
    props: true
  },
  {
    path: '/horarios/ver/:id',
    component: ScheduleView,
    props: true
  },
  {
    path:'/horario/crear/:id',
    component: CreateSchedule,
    props: true
  },
  {
    path:'/rango/horas',
    component:TimeTablesIndex,
    props:true
  },
  {
    path:'/rango/horas/editar/:id',
    component:TimeTablesEdit,
    props:true
  },
  {
    path:'/rango/horas/ver/:id',
    component:TimeTablesView,
    props:true
  },
  {
    path:'/rango/horas/crear',
    component:TimeTablesCreate
  },
  {
    path:'/instalaciones',
    component:InstallationIndex,
    props:true
  },
  {
    path:'/instalaciones/crear',
    component:InstallationCreate,
    
  },
  {
    path:'/instalaciones/editar/:id',
    component:InstallationEdit,
    props:true
  },
  {
    path:'/instalaciones/ver/:id',
    component:InstallationView,
    props:true
  },
  {
    path:'/ciclos',
    component:CycleIndex,
    props:true
  },
  {
    path:'/ciclos/crear',
    component:CycleCreate,
  },
  {
    path:'/ciclos/editar/:id',
    component:CycleEdit,
    props:true
  },
  {
    path:'/ciclos/ver/:id',
    component:CycleView,
    props:true
  },
  {
    path:'/concepto/pagos',
    component: ConceptPaymentIndex,
    props:true
  },
  {
    path:'/concepto/pagos/crear',
    component:ConceptPaymentCreate,
  },
  {
    path:'/concepto/pagos/editar/:id',
    component:ConceptPaymentEdit,
    props:true
  },
  {
    path:'/concepto/pagos/ver/:id',
    component:ConceptPaymentView,
    props:true
  },
  {
    path:'/listas/asignaturas',
    component: AsignatureAssitantList,
  },
  {
    path:'/concentrados/asignaturas',
    component: GradeSheets,
  },
  {
    path:'/concentrados/calificaciones',
    component: ConcertedQualificationIndex,
  }
  //   {
  //     // Route two takes the route parameter "id".
  //     // The parameter value can be accessed with $route.params.id in the RouteTwo component.
  //     path: '/route-two/:id',
  //     component: RouteTwo
  //   }
];
