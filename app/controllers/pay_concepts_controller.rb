class PayConceptsController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token
  before_action :set_pay_concept, only: [:show, :edit, :update, :destroy]

  # GET /pay_concepts
  # GET /pay_concepts.json
  def index
    @pay_concepts = PayConcept.all
  end

  # GET /pay_concepts/1
  # GET /pay_concepts/1.json
  def show
  end

  # GET /pay_concepts/new
  def new
    @pay_concept = PayConcept.new
  end

  # GET /pay_concepts/1/edit
  def edit
  end

  # POST /pay_concepts
  # POST /pay_concepts.json
  def create
    @pay_concept = PayConcept.new(pay_concept_params)

    respond_to do |format|
      if @pay_concept.save
        format.html { redirect_to @pay_concept, notice: 'Se agrego correctamente.' }
        format.json { render :show, status: :created, location: @pay_concept }
      else
        format.html { render :new }
        format.json { render json: @pay_concept.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pay_concepts/1
  # PATCH/PUT /pay_concepts/1.json
  def update
    respond_to do |format|
      if @pay_concept.update(pay_concept_params)
        format.html { redirect_to @pay_concept, notice: 'Su registro se actualizo correctamente.' }
        format.json { render :show, status: :ok, location: @pay_concept }
      else
        format.html { render :edit }
        format.json { render json: @pay_concept.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pay_concepts/1
  # DELETE /pay_concepts/1.json
  def destroy
    @pay_concept.destroy
    respond_to do |format|
      format.html { redirect_to pay_concepts_url, notice: 'El registro se elimino correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pay_concept
      @pay_concept = PayConcept.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pay_concept_params
      params.require(:pay_concept).permit(:name, :description, :cost)
    end
end
