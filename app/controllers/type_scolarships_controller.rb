class TypeScolarshipsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_type_scolarship, only: [:show, :edit, :update, :destroy]

  # GET /type_scolarships
  # GET /type_scolarships.json
  def index
    @type_scolarships = TypeScolarship.all
  end

  # GET /type_scolarships/1
  # GET /type_scolarships/1.json
  def show
  end

  # GET /type_scolarships/new
  def new
    @type_scolarship = TypeScolarship.new
  end

  # GET /type_scolarships/1/edit
  def edit
  end

  # POST /type_scolarships
  # POST /type_scolarships.json
  def create
    @type_scolarship = TypeScolarship.new(type_scolarship_params)

    if @type_scolarship.save
      render json: {data: @type_scolarship, status:'200', msg: 'El tipo de beca se creó correctamente'}
    else
      render json: {data: nil, status:'500', msg:  @type_scolarship.errors.full_messages}
    end
  end

  # PATCH/PUT /type_scolarships/1
  # PATCH/PUT /type_scolarships/1.json
  def update
    if @type_scolarship.update(type_scolarship_params)
      render json: {data: @type_scolarship, status:'200', msg: 'El tipo de beca se actualizó correctamente'}
    else
      render json: {data: nil, status:'500', msg:  @type_scolarship.errors.full_messages}
    end
  end

  # DELETE /type_scolarships/1
  # DELETE /type_scolarships/1.json
  def destroy
    if @type_scolarship.update(status: 0)
      render json: {data: nil, status:'200', msg: 'El tipo de beca se eliminó correctamente'}
    else
      render json: {data: nil, status:'500', msg:  @type_scolarship.errors.full_messages}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_type_scolarship
      @type_scolarship = TypeScolarship.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def type_scolarship_params
      params.require(:type_scolarship).permit(:name, :status)
    end
end
