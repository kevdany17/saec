class FormatsController < ApplicationController
    def grade_sheets
        @director = User.with_role(:direccion).first
        @datos_generales = 3
        @format = "012"
        @department = "DEPARTAMENTO ACADÉMICO"
        @disable_header = true
        @schedule = Schedule.find params[:id].first
        respond_to do |format|
        format.pdf {
            render template: 'formats/grade_sheets',pdf: "concentrado_calificaciones_lic",
            orientation: 'landscape',
            margin: {bottom: 20},
            show_as_html: params[:debug].present?
        }
        end
    end
    def assistant_sheets
        @datos_generales = 3
        @format = "011"
        @department = "DEPARTAMENTO ACADÉMICO"
        @disable_header = true
        @schedule = Schedule.find params[:id].first
        respond_to do |format|
            format.pdf {
                render template: 'formats/assistant_list',pdf: "lista_calificaciones",
                orientation: 'landscape',
                margin: {bottom: 20},
                show_as_html: params[:debug].present?
            }
        end
    end
    def schedule_assignment
        @datos_generales = 3
        @format = "008"
        @department = "DEPARTAMENTO ACADÉMICO"
        @disable_header = true
        @cycle = Cycle.where('status',1).first
        @student = Student.find params[:id]
        render template: 'formats/throw_stuffs.pdf',pdf: "lista_calificaciones",
        orientation: 'portrait',
        margin: {bottom: 20},
        show_as_html: params[:debug].present?
    end
    def commitment_payment
        @datos_generales = 3
        @format = "003"
        @department = "DEPARTAMENTO DE CONTROL ESCOLAR"
        @disable_header = true
        @student = Student.find params[:id]
        render template: 'formats/commitment_payment.pdf',pdf: "lista_calificaciones",
                layout: 'report.html',
                margin: {bottom: 20},
                show_as_html: params[:debug].present?
    end
end
