class LevelStudiesController < ApplicationController
  before_action :set_level_study, only: [:show, :edit, :update, :destroy]

  # GET /level_studies
  # GET /level_studies.json
  def index
    @level_studies = LevelStudy.all
  end

  # GET /level_studies/1
  # GET /level_studies/1.json
  def show
  end

  # GET /level_studies/new
  def new
    @level_study = LevelStudy.new
  end

  # GET /level_studies/1/edit
  def edit
  end

  # POST /level_studies
  # POST /level_studies.json
  def create
    @level_study = LevelStudy.new(level_study_params)

    respond_to do |format|
      if @level_study.save
        format.html { redirect_to @level_study, notice: 'Level study was successfully created.' }
        format.json { render :show, status: :created, location: @level_study }
      else
        format.html { render :new }
        format.json { render json: @level_study.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /level_studies/1
  # PATCH/PUT /level_studies/1.json
  def update
    respond_to do |format|
      if @level_study.update(level_study_params)
        format.html { redirect_to @level_study, notice: 'Level study was successfully updated.' }
        format.json { render :show, status: :ok, location: @level_study }
      else
        format.html { render :edit }
        format.json { render json: @level_study.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /level_studies/1
  # DELETE /level_studies/1.json
  def destroy
    @level_study.destroy
    respond_to do |format|
      format.html { redirect_to level_studies_url, notice: 'Level study was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_level_study
      @level_study = LevelStudy.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def level_study_params
      params.require(:level_study).permit(:name, :status, :type)
    end
end
