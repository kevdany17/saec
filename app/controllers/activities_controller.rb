class ActivitiesController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token
  before_action :set_activity, only: [:show, :edit, :update, :destroy]

  # GET /activities
  # GET /activities.json
  def index
    @activities = Activity.where(status: true)
  end

  # GET /activities/1
  # GET /activities/1.json
  def show
  end

  # GET /activities/new
  def new
    @activity = Activity.new
  end

  # GET /activities/1/edit
  def edit
  end

  # POST /activities
  # POST /activities.json
  def create
    @activity = Activity.new(activity_params)
      if @activity.save
        if params[:teacher_id]
          @activity.teachers << Teacher.find(params[:teacher_id])
        end  
        if params[:tuitions] != nil
            params[:tuitions].each_with_index do |single,index|
            @tuition = Tuition.new(:name => "#{@activity.name}: Colegiatura #{index + 1}",:start_date => single[0],:finish_date => single[1],:activity_id =>  @activity.id)
            @tuition.save
          end
        end
        if params[:sessions] != nil
          params[:sessions].each_with_index do |single,index|
            name = ""
            if params[:module_names][index].length > 0
              name = params[:module_names][index]
            else
              name = "Módulo #{index + 1}"  
            end
            if params[:module_teachers]
              @module = ModuleActivity.new(:name => name,:teacher_id => params[:module_teachers][index][:value].inspect,:activity_id =>  @activity.id)
            else
              @module = ModuleActivity.new(:name => name,:activity_id =>  @activity.id)
            end
            @module.save
            single.each_with_index do |single,index|
              @session = SessionModule.new(:module_activity_id => @module.id, :date => single)
              @session.save
            end
          end
        end
        render json:{data: @activity, status: '200',msg: "La actividad se agregó correctamente"}
      else
        render json:{data: @activity, status: '500',msg: "Ocurrió un error al crear la actividad"}
      end
  end

  # PATCH/PUT /activities/1
  # PATCH/PUT /activities/1.json
  def update
    @activity = Activity.find(update_params[:id])
    @activity.update( name: update_params[:name],
                      start_date: update_params[:startDate],
                      finish_date: update_params[:finishDate],
                      total_hours: update_params[:totalHours],
                      inscription: update_params[:inscription],
                      number_tuition: update_params[:numberTuition],
                      tuition: update_params[:tuition],
                      status: update_params[:status],
                      description: update_params[:description],
                      type_activity: TypeActivity.find(update_params[:typeActivity][:id]),
                      cycle: Cycle.find(update_params[:cycle][:id]),
                      type_cost: TypeCost.find(update_params[:typeCost][:id]))
    if(update_params[:typeActivity][:id].to_i != 3)
      @activity.update(teachers:Teacher.where(id: update_params[:teachers][0][:value].to_i))        
    elsif(update_params[:typeActivity][:id]==3)
      @activity.teachers.each do |teacher|
        @activity.teachers.delete(teacher.id)
      end        
    end            
    update_params[:tuitions].each do |tuition|
      @tuition = Tuition.find(tuition[:id])
      @tuition.update(start_date: tuition[:startDate],finish_date: tuition[:finishDate])
    end
    update_params[:moduleActivities].each do |module_activity|
      @module_activities =  ModuleActivity.find(module_activity[:id])
      @module_activities.update(name: module_activity[:name],teacher_id: module_activity[:teacherId])
      if(update_params[:typeActivity][:id]==3)
        @module_activities.update(teacher_id: module_activity[:teacherId])
      else
        @module_activities.update(teacher_id: 0)
      end  
      module_activity[:sessionModules].each do | session |
        @session = SessionModule.find(session[:id])
        @session.update(date: session[:date])
      end
    end
    @activity.save
    render json:{data: update_params, status: '200',msg: "La actividad se actualizó correctamente"}
  end

  # DELETE /activities/1
  # DELETE /activities/1.json
  def destroy
    #Activity.find_by_sql("update activities set status = 0 where id= #{params[:id]}")
    if Activity.update(params[:id],:status => 0)
      render json:{:status => "200",:msg => "El registro se eliminó correctamente"}
    end  
    #@activity.destroy
    # respond_to do |format|
    #   format.html { redirect_to activities_url, notice: 'El registro se elimino correctamente.' }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_activity
      @activity = Activity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def activity_params
      params.require(:activity).permit(:name, :type_activity_id, :start_date, :finish_date, :total_hours, :type_cost_id, :inscription, :tuition, :number_tuition, :status, :cycle_id, :description)
    end
    #
    def update_params
      params.require(:activity)
    end
end
