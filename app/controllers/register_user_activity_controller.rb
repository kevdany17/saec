class RegisterUserActivityController < ApplicationController
    before_action :authenticate_user!
    skip_before_action :verify_authenticity_token
    @json = {}    
    def index
       @type_payments = TypePayment.all
    end
    def get_activitys
        @activitys = Activity.all()
        render 'activities/get_activitys'
    end
    def saveUserInActivity()
        request = params[:register_user_activity][:data]
        if validateParamsNewUserActivity(request)
            #Generar Adeudo con Beca
            begin
                @user = Assistant.find(request[:user_id].to_i)
                @user.transaction do
                    @activity = Activity.find(request[:activity_id])
                    if @activity.assistants.where(id: @user.id) != []
                        raise "Este usuario ya esta registrado en el taller."
                    end
                    debit = Debit.new
                    debit.transaction do 
                        debit.user_id = @user.acting_as.id
                        debit.concept = @activity
                        debit.amount = @activity.inscription
                        debit.limit_date = @activity.finish_date
                        debit.total_cost = getTotaCost(request[:beca_percent_inscription],@activity.inscription)
                        debit.beca = request[:beca_percent_inscription]
                        @cycle = Cycle.where(status: 1)
                        if @cycle.size <= 0
                            raise "Se requiere tener un ciclo activo"
                        end
                        debit.cycle_id = @cycle.first.id
                        debit.status=1
                        debit.save
                        @user.activities << @activity
                        ingres = Ingress.new
                        ingres.debit_id = debit.id
                        ingres.type_payment_id = request[:type_payment]
                        ingres.date = Time.now.strftime("%Y-%m-%d")
                        ingres.cycle_id = @activity.cycle_id
                        #Ajustes del 17-03-2020 -Kevin Hernández
                        #Generar Folio en Caso de que la Actividad Sea Diplomado     
                        if request[:invoice] == "true"
                            @invoice = Invoice.where(name: 'ActualizacionProfesional').first
                            ingres.invoice_number = @invoice.number + 1
                            @invoice.number += 1
                            @invoice.save
                        else
                            ingres.invoice_number = 0
                        end
                        ingres.save
                        id_ingress = ingres.id
                        if @activity.number_tuition > 0
                            @activity.tuitions.each do |payment|
                                tuition = Debit.new
                                tuition.user_id = @user.acting_as.id
                                tuition.concept = payment
                                tuition.amount = @activity.tuition
                                tuition.limit_date = payment.finish_date
                                tuition.beca = request[:beca_percent_tuition]
                                tuition.total_cost = getTotaCost(request[:beca_percent_tuition],@activity.tuition)
                                tuition.cycle_id = @activity.cycle_id
                                tuition.status=0
                                tuition.save
                            end  
                        end
                        @json = {:status => 200, :data => {:id => id_ingress}, :msg => "Los datos se han guardado correctamente."}
                    end
                end
            rescue StandardError => e  
                @json = {:status => 500, :data =>e, :msg => e.message } 
            end            
            
        end
        #Generar Adeudo con Beca
        render :json => @json
    end

    private
        def validateParamsNewUserActivity(params)
            request = params
            regex = /[1-9]{1,10}/
            errors = Array.new
            if request[:user_id] == ""
               errors << 'No ha seleccionado un usuario.'
            end
            if request[:activity_id] == ""
                errors << 'No ha seleccionado una actividad.'
            end
            if request[:beca_percent] == ""
                errors << 'Error al procesar la Beca.'
            end
            if request[:type_payment].to_i <= 0
                errors << 'Seleccione al menos un tipo de pago.'    
            end
            @json =  {:status => 500, :data => nil, :msg => errors}
            if errors.length <= 0
                true
            else
                false
            end
        end
        def getTotaCost(beca,amount)
            percent = (beca.to_f/100)*amount
            total = amount - percent
            if total > 0
               return total
            else 
                return 0
            end
        end
end
