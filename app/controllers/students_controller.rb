class StudentsController < ApplicationController
  skip_before_action :verify_authenticity_token
  include UserControllerHelper
  before_action :set_student, only: [:show, :edit, :update, :destroy]

  # GET /students
  # GET /students.json
  def index
    @students = Student.all
  end

  # GET /students/1
  # GET /students/1.json
  def show
  end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students
  # POST /students.json
  def create
    params.permit!
    @student = Student.new(params[:student])
    if @student.save
      @student.registration = Enrollment.getEnrollment(@student.id,@student.study_plan.suffix,@student.cycle_id)
      @student.save
      @address = Address.new(params[:address])
      @address.owner = @student.acting_as
      @address.save

      @emergency_contact = EmergencyContact.new(params[:emergency_contact])
      @emergency_contact.student = @student
      @emergency_contact.save

      @graduation_institution = GraduationInstitution.new(params[:graduation_institution])
      @graduation_institution.student = @student
      @graduation_institution.save

      render json:{data: @student.id,msg: "El estudiante se creo correctamente", status: '200'}
    else
      render json:{data: @student.errors.full_messages ,status: '500'}
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    if @student.update(student_params)
      render json: {data: @student.id, status:'200', msg: 'El estudiante se actualizó correctamente'}
    else
      render json: {data: nil, status:'500', msg:  @student.errors.full_messages}
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    if @student.update(status: 0)
      render json:{:status => "200",:msg => "El registro se eliminó correctamente"}
    else
      render json: {data: nil, status:'500', msg:  @student.errors.full_messages}
    end  
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit(:phone,:cell_phone,:birth_day,:first_name, :second_name,:curp, :last_name,:email,:sexo,:civil_status, :nacionality, :foreign_language, :foreign_language_name, :disability, :disability_name, :actual_job, :job_center, :job_center_address, :scolarship_id, :registration, :cycle_id, :inscription_type,:study_plan_id, :semester)
    end
end
