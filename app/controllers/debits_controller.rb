class DebitsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_debit, only: [:show, :edit, :update, :destroy]

  # GET /debits
  # GET /debits.json
  def index
    @debits = Debit.find_by_sql("SELECT users.first_name,users.last_name,users.second_name,activities.name as nombre_actividad,cycles.name as nombre_ciclo,type_costs.name as nombre_costo,
      debits.id, debits.amount, debits.limit_date, debits.total_cost, level_academics.name as nombre_nivel, pay_concepts.name as nombre_concepto,
      debits.beca, debits.module_activity_id
    FROM debits
    join cycles on debits.cycle_id = cycles.id
    join users on debits.user_id = users.id
    join activities on debits.activity_id=activities.id
    join type_costs on debits.type_cost_id=type_costs.id
    left join level_academics on debits.level_academic_id =level_academics.id
    left join pay_concepts on debits.concept_id=pay_concepts.id
      ");
  end

  # GET /debits/1
  # GET /debits/1.json
  def show
    @debit = Debit.find_by_sql("SELECT users.first_name,users.last_name,users.second_name,activities.name as nombre_actividad,cycles.name as nombre_ciclo,type_costs.name as nombre_costo,
      debits.id, debits.amount, debits.limit_date, debits.total_cost, level_academics.name as nombre_nivel, pay_concepts.name as nombre_concepto,
      debits.beca, debits.module_activity_id
    FROM debits
    join cycles on debits.cycle_id = cycles.id
    join users on debits.user_id = users.id
    join activities on debits.activity_id=activities.id
    join type_costs on debits.type_cost_id=type_costs.id
    join level_academics on debits.level_academic_id =level_academics.id
    join pay_concepts on debits.concept_id=pay_concepts.id
    where debits.id=#{params[:id]}
      ");
  end

  # GET /debits/new
  def new
    @debit = Debit.new
  end

  # GET /debits/1/edit
  def edit
  end

  # POST /debits
  # POST /debits.json
  def create
    @debit = Debit.new(debit_params)

    respond_to do |format|
      if @debit.save
        format.html { redirect_to @debit, notice: 'El adeudo fue creado correctamente.' }
        format.json { render :show, status: :created, location: @debit }
      else
        format.html { render :new }
        format.json { render json: @debit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /debits/1
  # PATCH/PUT /debits/1.json
  def update
    respond_to do |format|
      if @debit.update(debit_params)
        format.html { redirect_to @debit, notice: 'El adeudo fue actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @debit }
      else
        format.html { render :edit }
        format.json { render json: @debit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /debits/1
  # DELETE /debits/1.json
  def destroy
    @status = Activity.find_by_sql("update activities set status = 0 
      where id= #{params[:id]}");
    -#@debit.destroy
    respond_to do |format|
      format.html { redirect_to debits_url, notice: 'El adeudo fue eliminado correctamente.' }
      format.json { head :no_content }
    end
  end

  def get_user_debits
    if request.xhr?
      respond_to do |format|
        # actividades = User.find_by_sql("SELECT p.id,p.name,p.amount from 
        #   (select debits.id as id,activities.name as name,debits.amount as amount 
        #   from users inner join debits on debits.user_id = users.id inner join activities on activities.id = debits.activity_id where users.id = #{params[:datos][:id]}) as p 
        #   where not exists (select * from ingresses where ingresses.debit_id=p.id)");
          
        # #resultados = User.find_by_sql("select debits.id,type_costs.name,debits.amount from debits inner join users on debits.user_id = users.id inner join type_costs on type_costs.id = debits.type_cost_id where users.id =#{params[:datos][:id]}")
        # resultados = User.find_by_sql("SELECT p.id,p.name,p.amount from 
        # (select debits.id as id,activities.name as name,debits.amount as amount 
        # from users inner join debits on debits.user_id = users.id inner join activities on activities.id = debits.activity_id where users.id = #{params[:datos][:id]}) as p 
        # where not exists (select * from ingresses where ingresses.debit_id=p.id)");
        respuesta =[]
        actividades = User.find_by_sql("SELECT distinct activities.id from activities 
          inner join debits  on debits.activity_id = activities.id where debits.user_id=#{params[:datos][:id]}");
        actividades.each do |single|
          resultados = User.find_by_sql("select distinct debits.id as id,module_activities.name,debits.beca,debits.total_cost,debits.amount from module_activities 
            inner join activities on activities.id = module_activities.activity_id 
            inner join debits on debits.module_activity_id=module_activities.id
            inner join users on debits.user_id = #{params[:datos][:id]}
            where activities.id = #{single.id} and not exists (select * from ingresses where ingresses.debit_id=debits.id)");
          if(resultados.count>0)
            resultados.each do |uno|
              respuesta.push(uno);
            end 
          # else
          #   resultados = User.find_by_sql("SELECT p.id,p.name,p.beca,p.total_cost,p.amount from 
          #     (select debits.id as id,activities.name as name,debits.beca,debits.total_cost,debits.amount as amount 
          #     from users inner join debits on debits.user_id = users.id 
          #     inner join activities on activities.id = debits.activity_id
          #     where users.id = #{params[:datos][:id]} and debits.activity_id = #{single.id}) as p 
          #     where not exists (select * from ingresses where ingresses.debit_id=p.id)");
          #     resultados.each do |uno|
          #       respuesta.push(uno);
          #     end 
          end 
        end
        format.json  { render :json => respuesta}
        #format.json  { render :json => Debit.joins(:user).where("user_id = #{params[:datos][:id]}")}
      end  
    end  
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_debit
      @debit = Debit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def debit_params
      params.require(:debit).permit(:user_id, :concept_id, :activity_id, :level_academic_id, :amount, :type_cost_id, :limit_date, :total_cost, :cycle_id, :beca, :module_activity_id)
    end
end
