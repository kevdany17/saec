class SchedulesController < ApplicationController
  before_action :set_schedule, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token

  include SchedulesHelper
  # GET /schedules
  # GET /schedules.json
  def index
    @schedules = Schedule.all
  end

  # GET /schedules/1
  # GET /schedules/1.json
  def show
  end

  # GET /schedules/new
  def new
    @schedule = Schedule.new
  end

  # GET /schedules/1/edit
  def edit
  end

  # POST /schedules
  # POST /schedules.json
  def create    
    begin
      @schedule = Schedule.new(ValidateSchedule.exist(schedule_params))
        if @schedule.save
          render json:{data: '', status: 200,msg: 'El registro se creó correctamente'}
        else
          render json:{data: '', status: 500,msg: @schedule.errors}
        end

    rescue StandardError => e
      render json:{data: '', status: 500,msg: e }
    end
  end

  # PATCH/PUT /schedules/1
  # PATCH/PUT /schedules/1.json
  def update
    begin
      
        if @schedule.update(ValidateSchedule.exist(schedule_params))
          render json:{data: '', status: 200,msg: 'El actualizó se creó correctamente'}
        else
          render json:{data: '', status: 500,msg: @schedule.errors}
        end

    rescue StandardError => e
      render json:{data: '', status: 500,msg: e }
    end
    # respond_to do |format|
    #   if @schedule.update(schedule_params)
    #     format.html { redirect_to @schedule, notice: 'Schedule was successfully updated.' }
    #     format.json { render :show, status: 200, location: @schedule }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @schedule.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /schedules/1
  # DELETE /schedules/1.json
  def destroy
    @schedule.destroy
    respond_to do |format|
      format.html { redirect_to installations_url, notice: 'Installation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schedule
      @schedule = Schedule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schedule_params
      params.require(:schedule).permit(:cycle_id, :study_plan_id, :asignature_id, :teacher_id, :time_table_id, :day, :group, :installation_id, :conflict)
    end
end
