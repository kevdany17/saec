class ConceptPaymentsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_concept_payment, only: [:show, :edit, :update, :destroy]

  # GET /concept_payments
  # GET /concept_payments.json
  def index
    @concept_payments = ConceptPayment.all
  end

  # GET /concept_payments/1
  # GET /concept_payments/1.json
  def show
  end

  # GET /concept_payments/new
  def new
    @concept_payment = ConceptPayment.new
  end

  # GET /concept_payments/1/edit
  def edit
  end

  # POST /concept_payments
  # POST /concept_payments.json
  def create
    @concept_payment = ConceptPayment.new(concept_payment_params)

    respond_to do |format|
      if @concept_payment.save
        format.html { redirect_to @concept_payment, notice: 'Concept payment was successfully created.' }
        format.json { render :show, status: 200, location: @concept_payment }
      else
        format.html { render :new }
        format.json { render json: @concept_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /concept_payments/1
  # PATCH/PUT /concept_payments/1.json
  def update
    respond_to do |format|
      if @concept_payment.update(concept_payment_params)
        format.html { redirect_to @concept_payment, notice: 'Concept payment was successfully updated.' }
        format.json { render :show, status: 200, location: @concept_payment }
      else
        format.html { render :edit }
        format.json { render json: @concept_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /concept_payments/1
  # DELETE /concept_payments/1.json
  def destroy
    @concept_payment.destroy
    respond_to do |format|
      format.html { redirect_to concept_payments_url, notice: 'Concept payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_concept_payment
      @concept_payment = ConceptPayment.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def concept_payment_params
      params.require(:concept_payment).permit(:name, :description, :cost)
    end
end
