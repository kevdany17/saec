class TypePaymentsController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token
  before_action :set_type_payment, only: [:show, :edit, :update, :destroy]

  # GET /type_payments
  # GET /type_payments.json
  def index
    @type_payments = TypePayment.where(status: true)
  end

  # GET /type_payments/1
  # GET /type_payments/1.json
  def show
  end

  # GET /type_payments/new
  def new
    @type_payment = TypePayment.new
  end

  # GET /type_payments/1/edit
  def edit
  end

  # POST /type_payments
  # POST /type_payments.json
  def create
    @type_payment = TypePayment.new(type_payment_params)

    respond_to do |format|
      if @type_payment.save
        format.html { redirect_to type_payments_path, notice: 'Se agrego correctamente.' }
        format.json { render :show, status: :created, location: @type_payment }
      else
        format.html { render :new }
        format.json { render json: @type_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /type_payments/1
  # PATCH/PUT /type_payments/1.json
  def update
    respond_to do |format|
      if @type_payment.update(type_payment_params)
        format.html { redirect_to type_payments_path, notice: 'Su registro se actualizo correctamente.' }
        format.json { render :show, status: :ok, location: @type_payment }
      else
        format.html { render :edit }
        format.json { render json: @type_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /type_payments/1
  # DELETE /type_payments/1.json
  def destroy
    if TypePayment.update(params[:id],:status => 0)
      render json:{:status => "200",:msg => "El registro se eliminó correctamente"}
    end  
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_type_payment
      @type_payment = TypePayment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def type_payment_params
      params.require(:type_payment).permit(:name)
    end
end
