class StudyPlansController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_study_plan, only: [:show, :edit, :update, :destroy]

  # GET /study_plans
  # GET /study_plans.json
  def index
    @study_plans = StudyPlan.all
  end

  # GET /study_plans/1
  # GET /study_plans/1.json
  def show
  end

  # GET /study_plans/new
  def new
    @study_plan = StudyPlan.new
  end

  # GET /study_plans/1/edit
  def edit
  end

  # POST /study_plans
  # POST /study_plans.json
  def create
    @study_plan = StudyPlan.new(study_plan_params)

    if @study_plan.save
      @csp = CyclesStudyPlans.new(study_plan_id: @study_plan.id,cycles_id: params[:cycle_id])
      @csp.save
      render json: {data: @study_plan, status:'200', msg: 'El plan de estudio se creó correctamente'}
    else
      render json: {data: nil, status:'500', msg:  @study_plan.errors.full_messages}
    end
  end

  # PATCH/PUT /study_plans/1
  # PATCH/PUT /study_plans/1.json
  def update
    if @study_plan.update(study_plan_params)
      render json: {data: @study_plan, status:'200', msg: 'El plan de estudio se actualizó correctamente'}
    else
      render json: {data: nil, status:'500', msg:  @study_plan.errors.full_messages}
    end
  end

  # DELETE /study_plans/1
  # DELETE /study_plans/1.json
  def destroy
    if @study_plan.update(status: 0)
      render json: {data: '', status:'200', msg: 'El plan de estudio se eliminó correctamente'}
    else
      render json:{data: "", status: '500',msg: "Ocurrio un error al eliminar el registro"}
    end   
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_study_plan
      @study_plan = StudyPlan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def study_plan_params
      params.require(:study_plan).permit(:name,:grade,:suffix,:rvoe,:start_valid_date, :finish_valid_date, :status)
    end
end
