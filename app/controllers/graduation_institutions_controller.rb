class GraduationInstitutionsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_graduation_institution, only: [:show, :edit, :update, :destroy]

  # GET /graduation_institutions
  # GET /graduation_institutions.json
  def index
    @graduation_institutions = GraduationInstitution.all
  end

  # GET /graduation_institutions/1
  # GET /graduation_institutions/1.json
  def show
  end

  # GET /graduation_institutions/new
  def new
    @graduation_institution = GraduationInstitution.new
  end

  # GET /graduation_institutions/1/edit
  def edit
  end

  # POST /graduation_institutions
  # POST /graduation_institutions.json
  def create
    @graduation_institution = GraduationInstitution.new(graduation_institution_params)

    respond_to do |format|
      if @graduation_institution.save
        format.html { redirect_to @graduation_institution, notice: 'Graduation institution was successfully created.' }
        format.json { render :show, status: :created, location: @graduation_institution }
      else
        format.html { render :new }
        format.json { render json: @graduation_institution.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /graduation_institutions/1
  # PATCH/PUT /graduation_institutions/1.json
  def update
    if @graduation_institution.update(graduation_institution_params)
      render json: {data: @graduation_institution, status:'200', msg: 'La institución de egreso se actualizó correctamente'}
    else
      render json: {data: nil, status:'500', msg:  @graduation_institution.errors.full_messages}
    end
  end

  # DELETE /graduation_institutions/1
  # DELETE /graduation_institutions/1.json
  def destroy
    @graduation_institution.destroy
    respond_to do |format|
      format.html { redirect_to graduation_institutions_url, notice: 'Graduation institution was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_graduation_institution
      @graduation_institution = GraduationInstitution.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def graduation_institution_params
      params.require(:graduation_institution).permit(:name, :average, :specialty, :status, :student_id)
    end
end
