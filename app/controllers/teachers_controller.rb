class TeachersController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token
  before_action :set_teacher, only: [:show, :edit, :update, :destroy]

  # GET /teachers
  # GET /teachers.json
  def index
    @teachers = Teacher.where(status: true)
  end

  # GET /teachers/1
  # GET /teachers/1.json
  def show
  end

  # GET /teachers/new
  def new
    @teacher = Teacher.new
  end

  # GET /teachers/1/edit
  def edit
  end

  def get_teacher
    render json: {:data => rails_blob_path(Teacher.find(params['id']).document_birth,only_path: true)}
  end 
  # POST /teachers
  # POST /teachers.json
  def create
      @teacher = Teacher.new(teacher_params)
      @teacher.password = "h4ml*html5"
      @teacher.password_confirmation = "h4ml*html5"
      @teacher.status = 1
      if @teacher.save
        render json: {data: {teacher: @teacher,user_id: @teacher.acting_as.id }, status:'200', msg: 'Se creó el docente correctamente'}
      else
        render json: {:data => @teacher.errors.full_messages , :status => '500', :msg => 'Error al Guardar Datos'}
      end
  end

  # PATCH/PUT /teachers/1
  # PATCH/PUT /teachers/1.json
  def update
      if @teacher.update(teacher_params)
        render json: {data: {teacher: @teacher,user_id: @teacher.acting_as.id }, status:'200', msg: 'success'}
      else
        render json: {:data => @teacher.errors.full_messages , :status => '500', :msg => 'Error al Guardar Datos'}
      end
  end

  # DELETE /teachers/1
  # DELETE /teachers/1.json
  def destroy
    if Teacher.update(params[:id],:status => 0)
      render json:{:status => "200",:msg => "El registro se eliminó correctamente"}
    end  
  end
  #Upload Files
  def upload_files
    @json = {}
    if params[:file].content_type == "application/pdf" 
      begin
        @teacher = Teacher::find(params[:id])
        case params[:type]
        when "acta"
          if @teacher.document_birth.attached?
            @teacher.document_birth.purge
          end  
          @teacher.document_birth.attach(params[:file])  
        when "ine"
          if @teacher.document_ine.attached?
            @teacher.document_ine.purge
          end  
          @teacher.document_ine.attach(params[:file])
        when "curp"
          if @teacher.document_curp.attached?
            @teacher.document_curp.purge
          end  
          @teacher.document_curp.attach(params[:file])
        when "curriculum"
          if @teacher.document_curriculum.attached?
            @teacher.document_curriculum.purge
          end
          @teacher.document_curriculum.attach(params[:file])
        when "doc_licenciatura"
          if @teacher.document_lic.attached?
            @teacher.document_lic.purge
          end
          @teacher.document_lic.attach(params[:file])
        when "doc_especialidad"
          if @teacher.document_specialty.attached?
            @teacher.document_specialty.purge
          end
          @teacher.document_specialty.attach(params[:file])
        when "doc_maestria"
          if @teacher.document_master.attached?
            @teacher.document_master.purge
          end
          @teacher.document_master.attach(params[:file])
        when "doc_doctorado"
          if @teacher.document_doctorate.attached?
            @teacher.document_doctorate.purge
          end
          @teacher.document_doctorate.attach(params[:file])
        end
        @json = {:status => 200, :data => params[:type] , :msg => "Documento Cargado Correctamente."}
      rescue StandardError => e  
        @json = {:status => 500, :data =>"personalizado error", :msg => "no sirviooo" } 
      end
    else
      @json = {:status => 500, :data =>"", :msg => "Solo se admiten documentos PDF" }
    end       
    render :json => @json
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_teacher
      @teacher = Teacher.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def   teacher_params
      params.require(:teacher).permit(:first_name, :second_name, :last_name,:email,:sexo,:password,:password_confirmation,:curp,:birth_day,:cell_phone,:phone,:master, :doctorate,:licenciatura,:specialty,:identification_specialty,:identification_master,:identification_doctorate,:identification_licenciatura)
    end
end