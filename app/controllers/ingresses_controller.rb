class IngressesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_ingress, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token

  # GET /ingresses
  # GET /ingresses.json
  def index
    @ingresses= Ingress.all()
  end

  # GET /ingresses/1
  # GET /ingresses/1.json
  def show
    @ingresses = Ingress.find(params[:id])
  end

  # GET /ingresses/new
  def new
    @ingress = Ingress.new
  end

  def add 
    @debit = Debit.find(params[:ingress][:id])
    if( params[:ingress][:new_beca] && params[:ingress][:new_total])
      @debit.update(beca: params[:ingress][:new_beca],total_cost: params[:ingress][:new_total])
    end
    @debit.status = 1
    @debit.save
    @ingress = Ingress.new({cycle_id: params[:ingress][:cycle_id],debit_id: params[:ingress][:id],type_payment_id: params[:ingress][:type_payment_id],date: Time.now.strftime("%Y-%m-%d")})
    #Ajustes del 17-03-2020 - Kevin Hernández
    #consultar colegiatura
    @tuition = Tuition.find(@debit.concept.id)
    #verificar si la colegiatura pertenece a una actividad que requiera folio  
    #if @tuition.activity.type_activity.id == 3
    if params[:ingress][:invoice] == "true"
      #Generar Folio en Caso de que la Actividad sea Diplomado
      @invoice = Invoice.where(name: 'ActualizacionProfesional').first
      @ingress.invoice_number = @invoice.number + 1
      #Aumentar el Contador de la Actividad
      @invoice.number += 1
      @invoice.save
    else
      @ingress.invoice_number = 0
    end
    if @ingress.save
      render json:{data: @ingress.id, status: '200',msg: "EL pago se procesó correctamente"}
    else
      render json:{data: @ingress.id, status: '500',msg: "Ocurrió un error al procesar el pago"}
    end
  end  

  # GET /ingresses/1/edit
  def edit
  end

  # POST /ingresses
  # POST /ingresses.json
  def create
    @ingress = Ingress.new(ingress_params)
    respond_to do |format|
      if @ingress.save
        format.html { redirect_to @ingress, notice: 'El ingreso fue creado correctamente.' }
        format.json { render :show, status: :created, location: @ingress }
      else
        format.html { render :new }
        format.json { render json: @ingress.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ingresses/1
  # PATCH/PUT /ingresses/1.json
  def update
    respond_to do |format|
      if @ingress.update(ingress_params)
        format.html { redirect_to @ingress, notice: 'El ingreso fue actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @ingress }
      else
        format.html { render :edit }
        format.json { render json: @ingress.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ingresses/1
  # DELETE /ingresses/1.json
  def destroy
    @ingress.destroy
    respond_to do |format|
      format.html { redirect_to ingresses_url, notice: 'El ingreso fue eliminado correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ingress
      @ingress = Ingress.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ingress_params
      params.require(:ingress).permit(:debit_id, :type_payment_id, :date)
    end
end
