class CyclesController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token
  before_action :set_cycle, only: [:show, :edit, :update, :destroy]

  # GET /cycles
  # GET /cycles.json
  def index
    @cycles = Cycle.all
  end

  # GET /cycles/1
  # GET /cycles/1.json
  def show
  end

  # GET /cycles/new
  def new
    @cycle = Cycle.new
  end

  # GET /cycles/1/edit
  def edit
  end

  # POST /cycles
  # POST /cycles.json
  def create
    @cycle = Cycle.new(cycle_params)
    respond_to do |format|
      if @cycle.save
        format.html { redirect_to cycles_path, notice: 'El ciclo fue creado correctamente.' }
        format.json { render :show, status: 200, location: @cycle }
      else
        format.html { render :new }
        format.json { render json: @cycle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cycles/1
  # PATCH/PUT /cycles/1.json
  def update
    respond_to do |format|
      if @cycle.update(cycle_params)
        format.html { redirect_to cycles_path, notice: 'El ciclo fue actualizado correctamente.' }
        format.json { render :show, status: 200, location: @cycle }
      else
        format.html { render :edit }
        format.json { render json: @cycle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cycles/1
  # DELETE /cycles/1.json
 def destroy
    if Cycle.update(params[:id],:status => 0)
     render json:{:status => "200",:msg => "El registro se eliminó correctamente"}
    end 
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cycle
      @cycle = Cycle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cycle_params
      params.require(:cycle).permit(:name, :status, :start_date, :finish_date, :inscription_lic, :inscription_master, :reinscription_lic, :reinscription_master, :tuition_lic, :tuition_master )
    end
end
