class ScolarshipsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_scolarship, only: [:show, :edit, :update, :destroy]

  # GET /scolarships
  # GET /scolarships.json
  def index
    @scolarships = Scolarship.all
  end

  # GET /scolarships/1
  # GET /scolarships/1.json
  def show
  end

  # GET /scolarships/new
  def new
    @scolarship = Scolarship.new
  end

  # GET /scolarships/1/edit
  def edit
  end

  # POST /scolarships
  # POST /scolarships.json
  def create
    @scolarship = Scolarship.new(scolarship_params)

    if @scolarship.save
      render json: {data: @scolarship, status:'200', msg: 'La Beca Escolar se creó correctamente'}
    else
      render json: {data: nil, status:'500', msg:  @scolarship.errors.full_messages}
    end
  end

  # PATCH/PUT /scolarships/1
  # PATCH/PUT /scolarships/1.json
  def update
    if @scolarship.update(scolarship_params)
      render json: {data: @scolarship, status:'200', msg: 'La Beca Escolar se actualizó correctamente'}
    else
      render json: {data: nil, status:'500', msg:  @scolarship.errors.full_messages}
    end
  end

  # DELETE /scolarships/1
  # DELETE /scolarships/1.json
  def destroy
    if @scolarship.update(status: 0)
      render json: {data: nil, status:'200', msg: 'La Beca Escolar se eliminó correctamente'}
    else
      render json: {data: nil, status:'500', msg:  @scolarship.errors.full_messages}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_scolarship
      @scolarship = Scolarship.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def scolarship_params
      params.require(:scolarship).permit(:amount, :description, :status, :type_scolarship_id)
    end
end
