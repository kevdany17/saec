class LevelAcademicsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_level_academic, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
  # GET /level_academics
  # GET /level_academics.json
  def index
    @level_academics = LevelAcademic.all
  end

  # GET /level_academics/1
  # GET /level_academics/1.json
  def show
  end

  # GET /level_academics/new
  def new
    @level_academic = LevelAcademic.new
  end

  # GET /level_academics/1/edit
  def edit
  end

  # POST /level_academics
  # POST /level_academics.json
  def create
    @level_academic = LevelAcademic.new(name: params[:level_academic][:name],rvoe: params[:level_academic][:rvoe],type: params[:level_academic][:type],status: 1)
    if(@level_academic.save)
      if(params[:level_academic][:type] == 'Licenciatura')
        @cost = Cost.new(inscription_lic: params[:cost][:inscription],reinscription_lic: params[:cost][:reinscription],tuition_lic: params[:cost][:tuition],level_academic_id: @level_academic.id)
        if(@cost.save)
          render json:{data: '', status: '200',msg: 'El registro se creó correctamente'}
        else
          render json:{data: '', status: '500',msg: @cost.errors}
        end
      else
        @cost = Cost.new(inscription_master: params[:cost][:inscription],reinscription_master: params[:cost][:reinscription],tuition_master: params[:cost][:tuition],level_academic_id: @level_academic.id)
        if(@cost.save)
          render json:{data: '', status: '200',msg: 'El registro se creó correctamente'}
        else
          render json:{data: '', status: '500',msg: @cost.errors}
        end
      end
    else
      render json:{data: '', status: '500',msg: @level_academic.errors}
    end  

    # if @level_academic.save
    #   render json:{data: @level_academic, status: '200',msg: "El nivel académico se generó correctamente"}
    # else
    #   render json:{data: '', status: '500',msg: @level_academic.errors}
    # end
  end

  # PATCH/PUT /level_academics/1
  # PATCH/PUT /level_academics/1.json
  def update
    if @level_academic.update(name: params[:level_academic][:name],rvoe: params[:level_academic][:rvoe],type: params[:level_academic][:type])

      if(params[:level_academic][:type] == 'Licenciatura')
        if(@level_academic.cost.update(inscription_lic: params[:cost][:inscription],reinscription_lic: params[:cost][:reinscription],tuition_lic: params[:cost][:tuition],level_academic_id: @level_academic.id,inscription_master: '',reinscription_master: '',tuition_master: ''))
          render json:{data: '', status: '200',msg: 'El registro se actualizó correctamente'}
        else
          render json:{data: '', status: '500',msg: @cost.errors}
        end
      else
        if(@level_academic.cost.update(inscription_master: params[:cost][:inscription],reinscription_master: params[:cost][:reinscription],tuition_master: params[:cost][:tuition],level_academic_id: @level_academic.id,inscription_lic: '',reinscription_lic: '',tuition_lic: ''))
          render json:{data: '', status: '200',msg: 'El registro se actualizó correctamente'}
        else
          render json:{data: '', status: '500',msg: @cost.errors}
        end
      end
    else
      render json:{data: '', status: '500',msg: @level_academic.errors}
    end
  end

  # DELETE /level_academics/1
  # DELETE /level_academics/1.json
  def destroy
    if @level_academic.update(status: 0)
      render json:{data: "", status: '200',msg: "El nivel académico se eliminó correctamente"}
    else
      render json:{data: "", status: '500',msg: "Ocurrio un error al eliminar el registro"}
    end     
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_level_academic
      @level_academic = LevelAcademic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def level_academic_params
      params.require(:level_academic).permit(:name, :rvoe, :type, :status)
    end
end
