class AdministratorsController < ApplicationController
  before_action :set_administrator, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token

  # GET /administrators
  # GET /administrators.json
  def index
    @administrators = Administrator.all
  end

  # GET /administrators/1
  # GET /administrators/1.json
  def show
  end

  # GET /administrators/new
  def new
    @administrator = Administrator.new
  end

  # GET /administrators/1/edit
  def edit
  end

  # POST /administrators
  # POST /administrators.json
  def create
    administrator = params[:administrator]
    @administrator = Administrator.new(
      {first_name: administrator[:first_name], 
        second_name: administrator[:second_name],
        last_name: administrator[:last_name],
        email: administrator[:email],
        password: administrator[:password],
        rfc: administrator[:rfc],
        number_imss: administrator[:number_imss],
        area_id: administrator[:area_id],
        position: administrator[:position],
        curp: administrator[:curp],
        sexo: administrator[:sex]
        })

    # respond_to do |format|
      if @administrator.save
        @administrator.add_role administrator[:rol]
        render json:{data: @administrator, status: '200',msg: "El administrator se agregó correctamente"}
      else
        render json:{data: "", status: '500',msg: @administrator.errors}
      end
    # end
  end

  # PATCH/PUT /administrators/1
  # PATCH/PUT /administrators/1.json
  def update
    administrator = params[:administrator]
    if @administrator.update(
      first_name: administrator[:actingAs][:firstName],
      second_name: administrator[:actingAs][:secondName],
      last_name: administrator[:actingAs][:lastName],
      email: administrator[:actingAs][:email],
      curp: administrator[:actingAs][:curp],
      sexo: administrator[:actingAs][:sexo],
      rfc:  administrator[:rfc],
      number_imss: administrator[:numberImss],
      area_id: administrator[:department][:id],
      position: administrator[:position]
      )
      @administrator.roles.each do |item|
        @administrator.remove_role item.name
      end
      @administrator.add_role administrator[:actingAs][:roles][0][:name]
      render json:{data: "", status: '200',msg: "El administrador se actualizó correctamente"}
    else 
      render json:{data: "", status: '500',msg: @administrator.errors}
    end
  end

  # DELETE /administrators/1
  # DELETE /administrators/1.json
  def destroy
    if @administrator.update(status: 0)
      render json:{data: "", status: '200',msg: "El administrador se eliminó correctamente"}
    else
      render json:{data: "", status: '500',msg: "Ocurrio un error al eliminar el registro"}
    end     
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_administrator
      @administrator = Administrator.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def administrator_params
      params.require(:administrator).permit(:rfc, :number_imss, :position)
    end
end
