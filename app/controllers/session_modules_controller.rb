class SessionModulesController < ApplicationController
  before_action :set_session_module, only: [:show, :edit, :update, :destroy]

  # GET /session_modules
  # GET /session_modules.json
  def index
    @session_modules = SessionModule.all
  end

  # GET /session_modules/1
  # GET /session_modules/1.json
  def show
  end

  # GET /session_modules/new
  def new
    @session_module = SessionModule.new
  end

  # GET /session_modules/1/edit
  def edit
  end

  # POST /session_modules
  # POST /session_modules.json
  def create
    @session_module = SessionModule.new(session_module_params)

    respond_to do |format|
      if @session_module.save
        format.html { redirect_to @session_module, notice: 'Session module was successfully created.' }
        format.json { render :show, status: :created, location: @session_module }
      else
        format.html { render :new }
        format.json { render json: @session_module.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /session_modules/1
  # PATCH/PUT /session_modules/1.json
  def update
    respond_to do |format|
      if @session_module.update(session_module_params)
        format.html { redirect_to @session_module, notice: 'Session module was successfully updated.' }
        format.json { render :show, status: :ok, location: @session_module }
      else
        format.html { render :edit }
        format.json { render json: @session_module.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /session_modules/1
  # DELETE /session_modules/1.json
  def destroy
    @session_module.destroy
    respond_to do |format|
      format.html { redirect_to session_modules_url, notice: 'Session module was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_session_module
      @session_module = SessionModule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def session_module_params
      params.require(:session_module).permit(:module_activity_id, :date)
    end
end
