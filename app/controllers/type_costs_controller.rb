class TypeCostsController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token
  before_action :set_type_cost, only: [:show, :edit, :update, :destroy]

  # GET /type_costs
  # GET /type_costs.json
  def index
    @type_costs = TypeCost.where(status: true)
  end

  # GET /type_costs/1
  # GET /type_costs/1.json
  def show
  end

  # GET /type_costs/new
  def new
    @type_cost = TypeCost.new
  end

  # GET /type_costs/1/edit
  def edit
  end

  # POST /type_costs
  # POST /type_costs.json
  def create
    @type_cost = TypeCost.new(type_cost_params)
    respond_to do |format|
      if @type_cost.save
        format.html { redirect_to type_costs_path, notice: 'Tipo de costo creado correctamente.' }
        format.json { render :show, status: :created, location: @type_cost }
      else
        format.html { render :new }
        format.json { render json: @type_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /type_costs/1
  # PATCH/PUT /type_costs/1.json
  def update
    respond_to do |format|
      if @type_cost.update(type_cost_params)
        format.html { redirect_to type_costs_path, notice: 'Tipo de costo actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @type_cost }
      else
        format.html { render :edit }
        format.json { render json: @type_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /type_costs/1
  # DELETE /type_costs/1.json
  def destroy
    if TypeCost.update(params[:id],:status => 0)
      render json:{:status => "200",:msg => "El registro se eliminó correctamente"}
    end  
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_type_cost
      @type_cost = TypeCost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def type_cost_params
      params.require(:type_cost).permit(:id, :name, :description)
    end
end
