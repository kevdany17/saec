class ReportsController < ApplicationController
  before_action :authenticate_user!
  include ReportsHelper
  def payment_report
    @datos_generales = 1
    @ingresos = Ingress.find(params[:id])
    respond_to  do |format|
      format.pdf {render template: 'reports/payment_report', pdf: "reporte_pago", layout: 'report.html'}
    end
  end 
  def ingress_activity_report
    @datos_generales = 1
    @activity = Activity.find(params[:id])
    respond_to do |format|
      format.pdf {render template: 'reports/ingress_report',pdf: "ingresos_actividad_#{@activity.name.delete(" ")}",layout: 'report.html',orientation: 'landscape'}
    end
  end
  def inscription_report
  @datos_generales = 3
    @student = Student.find(params[:id])
    respond_to do |format|
      # format.pdf {render template: 'reports/assistant_report',pdf: 'reporte_asistencia_actividad',show_as_html: params[:debug].present?,layout: 'report.html',footer: {right: '[page] de [topage]' }}
      format.pdf {
        render template: 'reports/inscription_report',pdf: "reporte_inscripcion_#{@student.registration}",
          layout: 'report.html',
          show_as_html: params[:debug].present?
      }
    end
  end
  def assistants_activity_report
    @datos_generales = 2
    @activity = Activity.find(params[:id])
    respond_to do |format|
      format.pdf {
        render footer: {html: {template: 'layouts/signature',layout: 'pdf_plain.html'}},
          template: 'reports/assistant_report',pdf: 'reporte_asistencia_actividad',
          layout: 'report.html',
          margin: {bottom: 20},
          show_as_html: params.key?('debug')
      }
    end
  end
  def constancy_ac
    @activity = Activity.find(params[:id])
    @user = @activity.assistants
    @director = User.with_role(:direccion)
    if @activity.type_activity.id != 3
      @date_activity = DateFormatter.date_activity_to_string(@activity)
    end
    @month = DateFormatter.month_in_spanish(@activity.finish_date)
    if(@activity.type_activity.id==3)
      respond_to  do |format|
        format.pdf {render pdf: 'constancia', template: 'reports/constancy_dp',margin: {bottom: 0},show_as_html: params.key?('debug')}
      end
    else  
      respond_to  do |format|
        format.pdf {render pdf: 'constancia', template: 'reports/constancy_ac',orientation: 'landscape',show_as_html: params.key?('debug')}
      end
    end  
  end
end
