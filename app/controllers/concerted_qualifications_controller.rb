class ConcertedQualificationsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_concerted_qualification, only: [:show, :edit, :update, :destroy]
  include ConcertedQualificationsHelper
  # GET /concerted_qualifications
  # GET /concerted_qualifications.json
  def index
    @concerted_qualifications = ConcertedQualification.all
  end

  # GET /concerted_qualifications/1
  # GET /concerted_qualifications/1.json
  def show
  end

  # GET /concerted_qualifications/new
  def new
    @concerted_qualification = ConcertedQualification.new
  end

  # GET /concerted_qualifications/1/edit
  def edit
  end

  # POST /concerted_qualifications
  # POST /concerted_qualifications.json
  def create
    begin
      params['schedules'].each do |item|
        @concerted_qualification = ConcertedQualification.new(:student_id => params[:concerted_qualification]['student_id'], :modality_id => params[:concerted_qualification]['modality_id'], :schedule_id => item['id'])
        if ValidateConcertedQualification.exist?(@concerted_qualification.student_id,@concerted_qualification.modality_id,@concerted_qualification.schedule_id)
          if @concerted_qualification.save
          
          else
            raise "Error al Guardar las Materias"
          end
        else
          raise "Las Materias ya han sido Registradas para este alumno."
        end
      end
      render json:{data: nil, status: '200',msg: "Materias Guardadas Correctamente."}
    rescue StandardError => e  
      render json:{data: nil, status: '500',msg: e.message }  
    end


    
   
    # respond_to do |format|
    #   if @concerted_qualification.save
    #     format.html { redirect_to @concerted_qualification, notice: 'Concerted qualification was successfully created.' }
    #     format.json { render :show, status: :created, location: @concerted_qualification }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @concerted_qualification.errors, status: :unprocessable_entity }
    #   end
    # end
    
  end

  # PATCH/PUT /concerted_qualifications/1
  # PATCH/PUT /concerted_qualifications/1.json
  def update
    respond_to do |format|
      if @concerted_qualification.update(concerted_qualification_params)
        format.html { redirect_to @concerted_qualification, notice: 'Concerted qualification was successfully updated.' }
        format.json { render :show, status: :ok, location: @concerted_qualification }
      else
        format.html { render :edit }
        format.json { render json: @concerted_qualification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /concerted_qualifications/1
  # DELETE /concerted_qualifications/1.json
  def destroy
    @concerted_qualification.destroy
    respond_to do |format|
      format.html { redirect_to concerted_qualifications_url, notice: 'Concerted qualification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_concerted_qualification
      @concerted_qualification = ConcertedQualification.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def concerted_qualification_params
      params.require(:concerted_qualification).permit(:student_id, :schedule_id, :modality_id,:assistance_first_partial, :assistance_second_partial, :assistance_third_partial, :score_first_partial, :score_second_partial, :score_third_partial, :ordinary, :extraordinary, :assistence_final, :score_final, :semester)
    end
end
