class AsignaturesController < ApplicationController
  skip_before_action :verify_authenticity_token, except: [:create, :update]
  before_action :set_asignature, only: [:show, :edit, :update, :destroy]

  # GET /asignatures
  # GET /asignatures.json
  def index
    @asignatures = Asignature.all
  end

  # GET /asignatures/1
  # GET /asignatures/1.json
  def show
  end

  # GET /asignatures/new
  def new
    @asignature = Asignature.new
  end

  # GET /asignatures/1/edit
  def edit
  end

  # POST /asignatures
  # POST /asignatures.json
  def create
    @asignature = Asignature.new(asignature_params)
    if params[:study_plan_id] != 0
      if @asignature.save
        @asignature.study_plans << StudyPlan.find(params[:study_plan_id])
        render json: {data: @asignature, status:'200', msg: 'La asignatura se creó correctamente'}
      else
        render json: {data: nil, status:'500', msg:  @asignature.errors.full_messages}
      end
    else
      render json: {data: nil, status:'500', msg:  'Falta Plan de Estudios'}
    end
  end

  # PATCH/PUT /asignatures/1
  # PATCH/PUT /asignatures/1.json
  def update
    if @asignature.update(asignature_params)
      render json: {data: @asignature, status:'200', msg: 'La asignatura se actualizó correctamente'}
    else
      render json: {data: nil, status:'500', msg:  @asignature.errors.full_messages}
    end
  end

  # DELETE /asignatures/1
  # DELETE /asignatures/1.json
  def destroy
    @asignature.update(status: 0)
    render json: {data: @asignature, status:'200', msg: 'La asignatura se eliminó correctamente'}
  end

  def charge_asignatures_from_file

    if File.extname(params['study_plans_file'].original_filename) != ".xlsx"
      render json: {status:'500', msg: 'Archivo no compatible'}
    else
      require 'rubyXL'
      require 'rubyXL/convenience_methods'

      asignatures_book = RubyXL::Parser.parse(params['study_plans_file'])

      asignatures = []
      asignature_id = 0
      study_plan_id = params['study_plan_id']

      asignatures_book.worksheets.first.sheet_data.rows.each do |row|
          if !empty_row?(row)
              if !headers_row?(row)
                  asignatures[asignature_id] << [row[1].value,row[2].value,row[3].value,row[4].value,row[5].value,row[6].value,row[7].value,row[8].value]
              else
                  if asignatures.size > 0
                      asignature_id += 1
                  end
                  asignatures << []  
              end            
          end          
      end
      if asignatures.size > 0
          insert_asignatures(study_plan_id,asignatures)
          asignatures = []
          asignature_id = 0
      end 

      render json: {status:'200', msg: 'Se agregaron correctamente las materías'}
    end  
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_asignature
      @asignature = Asignature.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def asignature_params
      params.require(:asignature).permit(:asignature_type, :asignature_name, :key, :academic_hours, :independent_hours, :credits, :classroom, :semester, :previous_id)
    end

    def empty_row?(row)
      if row[3] == nil && row[8] == nil
          return true
      else
          if row[3].value == nil && row[8].value == nil
              return true
          else    
              return false
          end    
      end 
    end

    def headers_row?(row)
        if row[3] != nil && row[8] != nil
            if row[3].value == "CLAVE" && row[8].value == "INSTALACIONES"
                return true
            else
                return false
            end
        end     
    end
    
    def insert_asignatures(study_plan_id,asignatures)
        asignatures.each_with_index do |semester,index|
            semester.each do |asignature|
                previous_id = nil
                if !asignature[3].nil?
                    previous_id = Asignature.find_by(key: asignature[3]).id
                end
                asignatu = Asignature.new(
                    asignature_type: asignature[0],
                    asignature_name: asignature[1],
                    key: asignature[2],
                    previous_id: previous_id,
                    academic_hours: asignature[4],
                    independent_hours: asignature[5],
                    credits: asignature[6],
                    classroom: asignature[7],
                    semester: index + 1
                )
                if asignatu.save
                    asigna_study = AsignaturesStudyPlan.new(asignature_id: asignatu.id,study_plan_id: study_plan_id)
                    asigna_study.save
                else
                  render json: {status:'500', msg: 'Error intente de nuevo o revise el documento subido'} 
                end    
            end    
        end
    end
end
