# frozen_string_literal: true
class User::RegistrationsController < Devise::RegistrationsController
  before_action :authenticate_user!
  layout 'layouts/application'
  protect_from_forgery with: :exception
  before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]
  #before_filter :resource_name

  # GET /resource/sign_up
  def new
    render 'new'
  end

  # POST /resource
  def create
    #super
    render '<h1>Cretae</h1>'
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end
  #def resource_name
  #  :user
  #end

  #def new  
  #  @user = User.new
  #end
  protected
  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name,:second_name,:last_name,:email,:password,:curp,:sexo,:cell_phone])
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  #def after_sign_up_path_for(resource)
    #super(resource)
    #flash[:status] = 'Registrado Correctamente'
    #redirect_to :back
  #end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
