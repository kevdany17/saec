class AddressesController < ApplicationController
  before_action :set_address, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
  # GET /adresses
  # GET /adresses.json
  def index
    @addresses = Address.all
  end

  # GET /adresses/1
  # GET /adresses/1.json
  def show
  end

  # GET /adresses/new
  def new
    @address = Address.new
  end

  # GET /adresses/1/edit
  def edit
  end

  # POST /adresses
  # POST /adresses.json
  def create
    @address = Address.new(address_params)
    @user = User.find(address_params[:user_id])
    @address.owner = @user
    
      if @address.save
        render json:{data: @address , status: '200', msg: 'La dirección se registró correctamente'}
      else
        render json:{data: nil , status: '500', msg: @address.errors.full_messages}
      end
  end

  # PATCH/PUT /adresses/1
  # PATCH/PUT /adresses/1.json
  def update
      if @address.update(address_params)
        render json:{data: @address , status: '200', msg: 'La dirección se registró correctamente'}
      else
        render json:{data: @address , status: '500', msg: @address.errors.full_messages}
      end
  end

  # DELETE /adresses/1
  # DELETE /adresses/1.json
  def destroy
    @address.destroy
    respond_to do |format|
      format.html { redirect_to addresses_url, notice: 'Adress was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address
      @address = Address.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def address_params
      params.require(:address).permit(:user_id, :street, :number_out, :number_in, :code_postal, :colony, :municipality, :state, :city, :status)
    end
end
