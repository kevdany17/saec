json.extract! level_study, :id, :name, :status, :type, :created_at, :updated_at
json.url level_study_url(level_study, format: :json)
