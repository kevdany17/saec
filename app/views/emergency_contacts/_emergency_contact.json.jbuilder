json.extract! emergency_contact, :id, :name, :cell_phone, :telephone, :relationship, :student_id, :status, :created_at, :updated_at
json.url emergency_contact_url(emergency_contact, format: :json)
