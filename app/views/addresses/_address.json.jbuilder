json.extract! address, :id, :user_id, :street, :number_out, :number_in, :code_postal, :colony, :municipality, :state, :city, :status, :created_at, :updated_at
json.url address_url(address, format: :json)
