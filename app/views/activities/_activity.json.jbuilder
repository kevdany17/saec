json.extract! activity, :id, :name, :type_activity_id, :start_date, :finish_date, :total_hours, :type_cost_id, :inscription, :tuition, :number_tuition, :status, :cycle_id, :description, :created_at, :updated_at
json.url activity_url(activity, format: :json)
