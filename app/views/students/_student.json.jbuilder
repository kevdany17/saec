json.extract! student, :id, :civil_status, :nacionality, :foreign_language, :foreign_language_name, :disability, :disability_name, :actual_job, :job_center, :job_center_address, :scolarship_id, :registration, :cycle_id, :inscription_type, :created_at, :updated_at
json.url student_url(student, format: :json)
