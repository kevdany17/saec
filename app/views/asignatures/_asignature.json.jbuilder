json.extract! asignature, :id, :asignature_type, :asignature_name, :key, :academic_hours, :independent_hours, :credits, :classroom, :semester, :created_at, :updated_at
json.url asignature_url(asignature, format: :json)
