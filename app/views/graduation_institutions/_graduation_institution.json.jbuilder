json.extract! graduation_institution, :id, :name, :average, :specialty, :status, :student_id, :created_at, :updated_at
json.url graduation_institution_url(graduation_institution, format: :json)
