json.extract! education_level, :id, :name, :status, :created_at, :updated_at
json.url education_level_url(education_level, format: :json)
