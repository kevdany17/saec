json.extract! administrator, :id, :rfc, :number_imss, :position, :created_at, :updated_at
json.url administrator_url(administrator, format: :json)
