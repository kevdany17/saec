json.extract! scolarship, :id, :amount, :description, :status, :scolarship_type_id, :created_at, :updated_at
json.url scolarship_url(scolarship, format: :json)
