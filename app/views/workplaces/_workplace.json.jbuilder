json.extract! workplace, :id, :name, :phone, :status, :created_at, :updated_at
json.url workplace_url(workplace, format: :json)
