json.extract! cycle, :id, :name, :status, :created_at, :updated_at
json.url cycle_url(cycle, format: :json)
