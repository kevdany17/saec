json.extract! pay_concept, :id, :name, :description, :cost, :created_at, :updated_at
json.url pay_concept_url(pay_concept, format: :json)
