json.extract! schedule, :id, :cycle_id, :study_plan_id, :asignature_id, :teacher_id, :time_table_id, :day, :group, :installation_id, :conflict, :created_at, :updated_at
json.url schedule_url(schedule, format: :json)
