json.extract! cost, :id, :inscription_lic, :inscription_master, :reinscription_lic, :reinscription_master, :tuition_lic, :tuition_master, :created_at, :updated_at
json.url cost_url(cost, format: :json)
