json.extract! module_activity, :id, :name, :fecha_limit_payment, :fecha_activity, :activity_id, :created_at, :updated_at
json.url module_activity_url(module_activity, format: :json)
