json.extract! type_scolarship, :id, :name, :status, :created_at, :updated_at
json.url type_scolarship_url(type_scolarship, format: :json)
