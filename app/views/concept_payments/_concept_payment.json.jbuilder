json.extract! concept_payment, :id, :name, :description, :cost, :created_at, :updated_at
json.url concept_payment_url(concept_payment, format: :json)
