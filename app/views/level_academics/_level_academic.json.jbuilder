json.extract! level_academic, :id, :name, :rvoe, :type, :status, :created_at, :updated_at
json.url level_academic_url(level_academic, format: :json)
