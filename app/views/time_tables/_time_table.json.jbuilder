json.extract! time_table, :id, :range_hours, :created_at, :updated_at
json.url time_table_url(time_table, format: :json)
