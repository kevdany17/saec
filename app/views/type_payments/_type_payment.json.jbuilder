json.extract! type_payment, :id, :name, :created_at, :updated_at
json.url type_payment_url(type_payment, format: :json)
