json.extract! debit, :id, :user_id, :concept_id, :activity_id, :level_academic_id, :amount, :type_cost_id, :limit_date, :total_cost, :cycle_id, :created_at, :updated_at
json.url debit_url(debit, format: :json)
