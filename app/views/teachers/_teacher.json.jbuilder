json.extract! teacher, :id, :master, :doctorate, :created_at, :updated_at
json.url teacher_url(teacher, format: :json)
