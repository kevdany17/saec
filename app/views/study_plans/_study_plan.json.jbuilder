json.extract! study_plan, :id, :name, :start_valid_date, :finish_valid_date, :status, :created_at, :updated_at
json.url study_plan_url(study_plan, format: :json)
