json.extract! session_module, :id, :module_activity_id, :date, :created_at, :updated_at
json.url session_module_url(session_module, format: :json)
