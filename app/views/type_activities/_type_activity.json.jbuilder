json.extract! type_activity, :id, :id, :name, :description, :created_at, :updated_at
json.url type_activity_url(type_activity, format: :json)
