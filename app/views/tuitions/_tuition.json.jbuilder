json.extract! tuition, :id, :name, :activity_id, :start_date, :finish_date, :created_at, :updated_at
json.url tuition_url(tuition, format: :json)
