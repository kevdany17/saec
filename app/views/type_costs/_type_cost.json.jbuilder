json.extract! type_cost, :id, :id, :name, :description, :created_at, :updated_at
json.url type_cost_url(type_cost, format: :json)
