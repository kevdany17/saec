json.extract! ingress, :id, :debit_id, :type_pay, :date, :created_at, :updated_at
json.url ingress_url(ingress, format: :json)
