class TypeActivity < ApplicationRecord
    has_many :activities
	validates :name, 
    presence: { message: "es requerido" },
    length: {maximum:50, message: "debe tener menos de 50 caracteres"}
    validates :description,
    length: {maximum:255, message: "debe ser mas corta"}
end
