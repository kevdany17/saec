class Asignature < ApplicationRecord
    has_and_belongs_to_many :study_plans

    has_one :next, class_name: "Asignature", foreign_key: "previous_id"

    belongs_to :previous, class_name: "Asignature", optional: true

    has_many :schedule

    validates :asignature_type, 
        presence: { message: "es requerido" }
    validates :asignature_name,
        presence: {message: "es requerida"},
        length: {maximum:255, message: "debe tener menos de 255 caracteres"}
    validates :key,
        presence: {message: "es requerida"}
    validates :academic_hours,
        presence: {message: "es requerido"}
    validates :independent_hours,
        presence: {message: "es requerido"}
    validates :credits,
        presence: {message: "es requerido"}
    validates :classroom,
        presence: {message: "es requerido"}
    validates :semester,
        presence: {message: "es requerido"}
end
