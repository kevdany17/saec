class Ingress < ApplicationRecord
    belongs_to :cycle
    belongs_to :debit
    belongs_to :type_payment
    validates :debit_id, 
    presence: { message: "es requerido" }
    validates :type_payment_id, 
    presence: { message: "es requerido" }
    validates :date, 
    presence: { message: "es requerido" }
end
