class StudyPlan < ApplicationRecord
    has_and_belongs_to_many :asignatures
    has_many :debits, as: :concept
    has_and_belongs_to_many :cycles, foreign_key: "cycles_id"
    has_one :cost
    has_many :schedule 
    
    self.inheritance_column = "heredity"

    validates :name, 
        presence: { message: "es requerido" },
        length: {maximum:255, message: "debe tener menos de 255 caracteres"}
    validates :start_valid_date,
        presence: {message: "es requerida"}
    validates :finish_valid_date,
        presence: {message: "es requerida"}
    validates :suffix,
        presence: {message: "es requerido"}
    validates :grade,
        presence: {message: "es requerido"}
    validates :rvoe,
        presence: {message: "es requerido"}
end
