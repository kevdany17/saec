class Cycle < ApplicationRecord
    has_many :activities
    has_many :debits
    has_many :ingresses
    has_many :assistants
    has_and_belongs_to_many :study_plan, foreign_key: "cycles_id"
    has_many :schedules
    validates :name, 
        presence: { message: "es requerido" },
        length: {maximum:120, message: "debe tener menos de 120 caracteres"}
    validates :start_date,
        presence: {message: "es requerida"}
    validates :finish_date,
        presence: { message: "es requerida" }
    validates :status,
        uniqueness: { message: "Solo puede existir un ciclo activo en el sistema" },
        if: :unico?     
    def unico?
        status == 1    
    end    
end