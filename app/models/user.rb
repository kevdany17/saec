class User < ApplicationRecord
  actable
  rolify
  validates :curp , 
    uniqueness: { message: "ya se encuentra registrada"},
    presence: { message: "es requerida" }
  validates :first_name, :second_name, :last_name, presence: { message: "es requerido" }
  
  validates :email, uniqueness: { message: "ya se encuentra registrado" }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :debits
  has_many :ingresses, through: :debits
  has_one :address, as: :owner
end
