class Address < ApplicationRecord
    belongs_to :owner, polymorphic: true

    validates :street,
        presence: {message: "es requerida"}
    validates :number_out,
        presence: {message: "es requerido"}
    validates :code_postal,
        presence: {message: "es requerido"}
    validates :colony,
        presence: {message: "es requerida"}  
    validates :municipality,
        presence: {message: "es requerido"}
    validates :city,
        presence: {message: "es requerida"}
    validates :state,
        presence: {message: "es requerido"}         
end  
