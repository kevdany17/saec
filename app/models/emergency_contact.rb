class EmergencyContact < ApplicationRecord
    belongs_to :student

    validates :name, 
        presence: { message: "es requerido" }
    validates :cell_phone,
        presence: {message: "es requerida"}
    validates :telephone,
        presence: {message: "es requerida"}
    validates :student_id,
        presence: {message: "es requerido"}
end
