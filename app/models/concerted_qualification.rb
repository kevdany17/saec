class ConcertedQualification < ApplicationRecord
    belongs_to :student
    belongs_to :schedule
    belongs_to :modality
    scope :approved, -> { where("score_final >= 6") }
end
