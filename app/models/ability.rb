class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)
      alias_action :create, :read, :update, :to => :crud
      if user.has_role? :admin
        can :manage, :all
      elsif user.has_role? :direccion
        can :crud, [Assistant,Cycle,Activity,TypeActivity,Debit, Teacher, TypePayment, TypeCost]
        can :destroy, [Assistant,Cycle,Activity,TypeActivity,Debit,Teacher]
        can :report, [Activity]
      elsif user.has_role? :coordinate_ac
        can :crud, [Assistant,Cycle,Activity,TypeActivity,Debit, Teacher]
        can :report, [Activity]
      elsif user.has_role? :regain
        can :crud, [Assistant,Debit,Teacher]
        can :read, [Cycle,Activity,TypeActivity,TypePayment,TypeCost]
      elsif user.has_role? :teacher
        #can :read, :create, :update, Activity
      elsif user.has_role? :estudent
        #can :read, :create, :update, Activity
      elsif user.has_role? :user
        #can :read, :create, :update, Activity
    end
    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
