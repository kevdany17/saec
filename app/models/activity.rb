class Activity < ApplicationRecord
    belongs_to :type_activity
    belongs_to :cycle
    belongs_to :type_cost
    has_many :module_activities
    has_many :tuitions
    has_and_belongs_to_many :assistants
    has_and_belongs_to_many :teachers
    has_many :debits, as: :concept
    validates :name, 
    presence: { message: "es requerido" },
    length: {maximum:150, message: "debe tener menos de 150 caracteres"}
    validates :type_activity_id,
    presence: { message: "es requerido" }
    validates :start_date,
    presence: { message: "es requerida" }
    validates :finish_date,
    presence: { message: "es requerida" }
    validates :total_hours,
    presence: { message: "es requerido" },
    numericality: { message: "debe ser un número mayor igual a 0", greater_than_or_equal_to: 0}
    validates :type_cost_id,
    presence: { message: "es requerido" }
    validates :inscription,
    presence: { message: "es requerido" },
    numericality: { message: "debe ser un número mayor igual a 0", greater_than_or_equal_to: 0}
    validates :tuition,
    presence: { message: "es requerido" },
    numericality: { message: "debe ser un número mayor igual a 0", greater_than_or_equal_to: 0}
    validates :number_tuition,
    presence: { message: "es requerido" },
    numericality: { message: "debe ser un número mayor igual a 0", greater_than_or_equal_to: 0}
    validates :status,
    presence: { message: "es requerido" }
    validates :cycle_id,
    presence: { message: "es requerido" }
end
