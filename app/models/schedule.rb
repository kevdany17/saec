class Schedule < ApplicationRecord
    belongs_to :cycle
    belongs_to :study_plan
    belongs_to :asignature
    belongs_to :teacher
    belongs_to :time_table
    belongs_to :installation
    has_many :concerted_qualifications
    
    def self.schedule_for_study_plan_and_semester(study_plan_id,semester,student_id)
        Schedule.joins(:cycle,:asignature).where(study_plan_id: study_plan_id).where(cycles: {status: 1})
            .where(asignatures: { semester: semester })
            .select {   |item| 
                (item.asignature.previous_id == nil && !item.check_asignature_prev(student_id,item.asignature.id)) || 
                (item.asignature.previous_id != nil && item.check_asignature_prev(student_id,item.asignature.previous_id))}
    end
    def check_asignature_prev(student_id,asignature_id)
        Student.find(student_id).concerted_qualifications.approved.each do |item|
            if item.schedule.asignature.id == asignature_id
                return true
            end
        end
        return false
    end
end