class TypeCost < ApplicationRecord
    has_many :activities
	validates :name, 
    presence: { message: "Es requerido." },
    length: {maximum:30, message: "Debe tener menos de 20 caracteres"}
    validates :description,
    presence: { message: "Es requerido" },
    length: {maximum:200, message: "Debe tener menos de 200 caracteres"}
end
