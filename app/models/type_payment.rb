class TypePayment < ApplicationRecord
    validates :name, presence: { message: "es requerido" }, 
    length: {minimum:0, maximum:20, message: "debe tener menos de 20 caracteres"}
end
