class Debit < ApplicationRecord
    belongs_to :cycle
    has_one :ingress
    belongs_to :user
    belongs_to :concept, polymorphic: true
    validates :user_id, 
    presence: { message: "es requerido" }
    validates :amount,
    presence: { message: "es requerido" },
    numericality: { message: "debe ser un número mayor a 0", greater_than: 0}
    validates :total_cost,
    presence: { message: "es requerido" },
    numericality: { message: "debe ser un número mayor a 0", greater_than: -1}
    validates :limit_date,
    presence: { message: "es requerido" }
    validates :cycle_id,
    presence: { message: "es requerido" }
end
