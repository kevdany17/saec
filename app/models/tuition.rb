class Tuition < ApplicationRecord
    belongs_to :activity
    has_many :debits, as: :concept
end
