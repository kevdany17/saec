class Teacher < ApplicationRecord
    include Rails.application.routes.url_helpers

    acts_as :user
    has_and_belongs_to_many :activities
    has_one_attached :document_birth
    has_one_attached :document_ine
    has_one_attached :document_curp
    has_one_attached :document_curriculum
    has_one_attached :document_lic
    has_one_attached :document_specialty
    has_one_attached :document_master
    has_one_attached :document_doctorate
    has_many :schedules

    validates :licenciatura , 
        presence: { message: "es requerida" }
    validates :identification_licenciatura , 
        presence: { message: "es requerida" }     
    # Métodos que devuelven la ruta de los documentos subidos
    def document_birth_path
        if document_birth.attached?
            rails_blob_path(document_birth, only_path: true)
        else     
            nil
        end    
    end
    def document_ine_path
        if document_ine.attached?
            rails_blob_path(document_ine, only_path: true)
        else
            nil
        end        
    end
    def document_curp_path
        if document_curp.attached?
            rails_blob_path(document_curp, only_path: true)
        else
            nil
        end  
    end
    def document_curriculum_path
        if document_curriculum.attached?    
            rails_blob_path(document_curriculum, only_path: true)
        else
            nil
        end  
    end
    def document_lic_path
        if document_lic.attached?
            rails_blob_path(document_lic, only_path: true)
        else     
            nil
        end        
    end
    def document_specialty_path
        if document_specialty.attached?
            rails_blob_path(document_specialty, only_path: true)
        else     
            nil
        end        
    end
    def document_master_path
        if document_master.attached?
            rails_blob_path(document_master, only_path: true)
        else     
            nil
        end        
    end
    def document_doctorate_path
        if document_doctorate.attached?
            rails_blob_path(document_doctorate, only_path: true)
        else     
            nil
        end        
    end
end
