class Student < ApplicationRecord
    acts_as :user
    belongs_to :cycle
    has_one :emergency_contact
    belongs_to :scolarship, optional: true
    has_one :graduation_institution
    belongs_to :study_plan
    has_many :concerted_qualifications

    def get_subject_list
        self.concerted_qualifications.select{|item | item.schedule.cycle.status == 1}
    end
end
