class TimeTable < ApplicationRecord
    validates :range_hours,
        presence: { message: "es requerido" },
        length: {maximum:255, message: "debe tener menos de 255 caracteres"}
    has_many :schedule
end
