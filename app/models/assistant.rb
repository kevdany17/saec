class Assistant < ApplicationRecord
    acts_as :user
    belongs_to :cycle
    has_and_belongs_to_many :activities
    #belongs_to :level_study
    has_and_belongs_to_many :workplaces
end
