class Scolarship < ApplicationRecord
    belongs_to :type_scolarship
    has_many :students

    validates :amount,
        numericality: { message: "debe ser un número mayor a 0 y menor a 100", greater_than_or_equal_to: 0,less_than_or_equal_to: 100}

    validates :description,
        presence: {message: "es requerida"}

    validates :type_scolarship_id,
        presence: {message: "es requerido"}    
end
