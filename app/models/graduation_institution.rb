class GraduationInstitution < ApplicationRecord
    belongs_to :student

    validates :name, 
        presence: { message: "es requerido" }
    validates :specialty,
        presence: {message: "es requerida"}
    validates :student_id,
        presence: {message: "es requerido"}
end
