class PayConcept < ApplicationRecord
    has_many :debits, as: :concept
    validates :name, 
    presence: { message: "es requerido" },
    length: {maximum:30, message: "debe tener menos de 30 caracteres"}
    validates :description,
    length: {maximum:255, message: "debe ser mas corta"}
    validates :cost,
    presence:  { message: "es requerido" },
    numericality: { message: "debe ser un número mayor a 0", greater_than: 0}
end
