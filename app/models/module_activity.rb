class ModuleActivity < ApplicationRecord
    belongs_to :activity
    has_many :session_modules
    belongs_to :teacher
end
