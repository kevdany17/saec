class Workplace < ApplicationRecord
    has_one :address, as: :owner
    has_and_belongs_to_many :assistants
end
