class Administrator < ApplicationRecord
    acts_as :user
    belongs_to :department, foreign_key: "area_id"
end
