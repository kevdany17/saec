$(document).ready(function () {
            $('#number-tuition').on('change', function () {
                $('#modulos').empty();
                if ($('#number-tuition').val() != 0)
                    $('#modulos').show();
                else
                    $('#modulos').hide();
                for (var i = 0; i < $('#number-tuition').val(); i++)
                    $('#modulos').append(`
        <h4 class='col-md-12' style='margin:0'>Modulo ${i+1}</h4>
        <label class='col-md-2 control-label'>Nombre:</label>
        <div class='col-md-4'>
        <input type='text' name='modulo[${i}][nombre]' class='form-control'>
        </div>
        <label class='col-md-1 control-label'>Fecha de Actividad:</label>
        <div class='col-md-4'>
        <input type='date' name='modulo[${i}][fecha]' class='form-control'>
        </div>
        `);
            });
        });