// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// require jquery2
// require jquery_ujs
//= require rails-ujs
// require activestorage
// require turbolinks
//= require js/vendor/modernizr-3.3.1.min.js
// require vendor/jquery-2.2.4.min.js
// require js/vendor/bootstrap.min.js
// require js/plugins.js
// require js/app.js
// require js/pages/uiTables.js
// require js/pages/formsWizard.js
// require js/pages/readyDashboard.js
// require_tree
