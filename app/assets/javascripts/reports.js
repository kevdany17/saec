function downloadWithCSS() {
    const doc = new jsPDF('portrait','mm','A4');
    /** WITH CSS */
    var canvasElement = document.createElement('canvas');
    html2canvas(document.getElementById('container'), { canvas: canvasElement }).then(function (canvas) {
      const img = canvas.toDataURL("image/jpeg",1.0);
      doc.addImage(img,'JPEG',10,10);
      //doc.autoPrint();
      window.open(doc.output('bloburl'));
      //doc.save("sample.pdf");
    });
}