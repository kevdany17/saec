$(document).ready(function() {
  var inscription = 0.0;
  var tuition = 0.0;
  var numberTuition = 0;
  $(".select-activity").on("click", function() {
    var valores = [];
    // Obtenemos todos los valores contenidos en los <td> de la fila
    // seleccionada
    $(this)
      .parents("td")
      .parents("tr")
      .find(".valor")
      .each(function() {
        valores.push($(this).html());
      });
    $("#activity-name").text(valores[1]);
    $("#activity-finish-date").text(valores[2]);
    $("#activity-total-hours").text(valores[4]);
    $("#activity-id").val(valores[0]);
    $("#cycle-id").val($(this).data("cycle-id"));
    inscription = parseFloat($(this).data("inscription"));
    tuition = parseFloat($(this).data("tuition"));
    numberTuition = parseInt($(this).data("number-tuition"));
    $("#activity-inscription").html(" <b>$" + inscription.toFixed(2) + "</b>");
    if (numberTuition > 0) {
      $("#activity-tuition").html(" <b>$" + tuition.toFixed(2) + "</b>");
      $("#number-tuition").html("<b>" + numberTuition + "</b>");
      $("#colegiatura").css("display", "block");
    } else {
      $("#colegiatura").css("display", "none");
    }
    var row =
      "<tr><td>Inscripción</td><td>$ " + inscription.toFixed(2) + "</td></tr>";
    for (var i = 0; i < numberTuition; i++) {
      row +=
        "<tr><td>Colegiatura</td><td>$ " + tuition.toFixed(2) + "</td></tr>";
    }
    $("#log-payments").html(row);
    $("#modal-activitys").modal("hide");
  });
  function init() {
    /* Initialize Bootstrap Datatables Integration */
    App.datatables();
    /* Initialize Datatables */
    $("#table-2").dataTable({
      columnDefs: [
        { orderable: false, targets: [$("#table-2 tr:last td").length - 1] }
      ],
      pageLength: 10,
      lengthMenu: [
        [5, 10, 20],
        [5, 10, 20]
      ]
    });
    /* Add placeholder attribute to the search input */
    $(".dataTables_filter input").attr("placeholder", "Buscar");

    /* Select/Deselect all checkboxes in tables */
    $("thead input:checkbox").click(function() {
      var checkedStatus = $(this).prop("checked");
      var table = $(this).closest("table");

      $("tbody input:checkbox", table).each(function() {
        $(this).prop("checked", checkedStatus);
      });
    });

    /* Table Styles Switcher */
    var genTable = $("#general-table");
    var styleBorders = $("#style-borders");

    $("#style-default").on("click", function() {
      styleBorders.find(".btn").removeClass("active");
      $(this).addClass("active");

      genTable.removeClass("table-bordered").removeClass("table-borderless");
    });

    $("#style-bordered").on("click", function() {
      styleBorders.find(".btn").removeClass("active");
      $(this).addClass("active");

      genTable.removeClass("table-borderless").addClass("table-bordered");
    });

    $("#style-borderless").on("click", function() {
      styleBorders.find(".btn").removeClass("active");
      $(this).addClass("active");

      genTable.removeClass("table-bordered").addClass("table-borderless");
    });

    $("#style-striped").on("click", function() {
      $(this).toggleClass("active");

      if ($(this).hasClass("active")) {
        genTable.addClass("table-striped");
      } else {
        genTable.removeClass("table-striped");
      }
    });

    $("#style-condensed").on("click", function() {
      $(this).toggleClass("active");

      if ($(this).hasClass("active")) {
        genTable.addClass("table-condensed");
      } else {
        genTable.removeClass("table-condensed");
      }
    });

    $("#style-hover").on("click", function() {
      $(this).toggleClass("active");

      if ($(this).hasClass("active")) {
        genTable.addClass("table-hover");
      } else {
        genTable.removeClass("table-hover");
      }
    });
    setTimeout(function() {
      $("#table-2_wrapper>.filter-responsive").css(
        "width",
        $("#table-2").width() + 2
      );
    }, 2000);
  }
  init();
  $(".btn-eliminar").on("click", function(elem) {
    Swal.fire({
      title: "¿Estas seguro?",
      text: "¡No podras deshacer este cambio!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      cancelButtonText: "Cancelar",
      confirmButtonText: "Si, eliminar!"
    }).then(result => {
      if (result.value) {
        axios
          .delete("/actividades/" + elem.target.id)
          .then(function(response) {
            if (response.data.status == "200")
              Swal.fire("Éxito!", response.data.msg, "success").then(result => {
                if (result.value) {
                  location.reload();
                }
              });
          })
          .catch(function(response) {
            Swal.fire("Error!", "El registro no se pudo eliminar!", "error");
          });
      }
    });
  });
});
