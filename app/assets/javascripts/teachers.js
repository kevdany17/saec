// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$( document ).ready(function() {
$(".btn-eliminar").on("click",function(elem){
    Swal.fire({
        title: '¿Estas seguro?',
        text: "¡No podras deshacer este cambio!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, eliminar!'
      }).then((result) => {
        if (result.value) {
          axios.delete('/profesores/'+elem.target.id).then(
            function(response) {
              if(response.data.status == "200")
                Swal.fire(
                  'Éxito!',
                  response.data.msg,
                  'success'
                ).then((result) => {
                  if (result.value) {
                  location.reload()
                }
              }
                )
          }
          ).catch(
            function(response) {
              Swal.fire(
                  'Error!',
                  'El registro no se pudo eliminar!',
                  'error'
                )
            }
          );
        }
      })
})
})