module Mutations
  class CreateTypePayment < BaseMutation
    # arguments passed to the `resolved` method
    argument :name, String, required: true
    argument :status, Integer, required: false

    # return type from the mutation
    type Types::TypePaymentType

    def resolve(name: nil, status: 1)
      TypePayment.create!(
        name: name,
        status: status,
      )
    end
  end
end