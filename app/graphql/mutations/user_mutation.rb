module Mutations
    module UserMutation
        class Update < Mutations::BaseMutation
            # arguments passed to the `resolved` method
            description "Actualizar Datos del Usuario"
            argument :id, Integer, required: true
            argument :firstName, String, required: true
            argument :secondName, String, required: true
            argument :lastName, String, required: true
            argument :email, String, required: true
            argument :curp, String, required: true
            argument :cellPhone, String, required: true
            argument :sexo, String, required: true
            argument :birthDay, String, required: true
            # return type from the mutation
            type Types::UserType
    
            def resolve(id: nil, **attributes)
                User.find(id).tap do |user|
                    user.update!(attributes)
                end
            end
        end
    end
end