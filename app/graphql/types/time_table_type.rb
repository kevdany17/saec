module Types
  class TimeTableType < Types::BaseObject
    field :id, ID, null: false
    field :range_hours, String, null: false
  end
end
