module Types
  class MutationType < BaseObject
    field :create_payment, mutation: Mutations::CreateTypePayment
    field :update_user, mutation: Mutations::UserMutation::Update
  end
end
