module Types
  class SessionType < Types::BaseObject
    field :id, ID, null: false
    field :date, String, null: false
    field :module_activity , Types::ModuleType, null: false
  end
end
