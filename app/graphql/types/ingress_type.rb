module Types
  class IngressType < Types::BaseObject
    field :id, ID, null: false
    field :debit, Types::DebitType, null: false
    field :date, String, null: false
    field :cycle, Types::CycleType, null: false
    field :type_payment, Types::TypePaymentType, null: false
  end
end
