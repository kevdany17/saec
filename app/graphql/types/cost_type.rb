module Types
  class CostType < Types::BaseObject
    field :id, ID, null: false
    field :inscription_lic , Float, null: true
    field :inscription_master, Float, null: true
    field :reinscription_lic , Float, null: true
    field :reinscription_master, Float, null: true
    field :tuition_lic , Float, null: true
    field :tuition_master, Float, null: true
    field :study_plan, Types::StudyPlanType, null: false
  end
end
