module Types
  class TeacherType < Types::BaseObject
    field :id, ID, null: false
    field :licenciatura, String, null: false
    field :identification_licenciatura, String, null: false
    field :specialty, String, null: false
    field :identification_specialty, String, null: false
    field :master, String, null: false
    field :identification_master, String, null: false
    field :doctorate, String, null: false
    field :identification_doctorate, String, null: false
    field :acting_as,Types::UserType, null: false
    field :address,Types::AddressType, null: true
    field :document_birth_path, String, null: true
    field :document_curp_path, String, null: true
    field :document_ine_path, String, null: true
    field :document_curriculum_path, String, null: true
    field :document_lic_path, String, null: true
    field :document_specialty_path, String, null: true
    field :document_master_path, String, null: true
    field :document_doctorate_path, String,null: true
    def first_name
      return object.acting_as.first_name
    end
    
    field :last_name, String, null: false

    def last_name
      return object.acting_as.last_name
    end 

    field :second_name, String, null: false

    def second_name
      return object.acting_as.second_name
    end

    field :email, String, null: false

    def email
      return object.acting_as.email
    end  

    field :curp, String, null: false

    def curp
      return object.acting_as.curp
    end

    field :cellphone, String, null: false

    def cellphone
      return object.acting_as.cell_phone
    end
  end
end
