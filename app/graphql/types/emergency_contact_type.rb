module Types
  class EmergencyContactType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :cell_phone, String, null: false
    field :telephone, String, null: false
  end
end
