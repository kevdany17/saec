module Types
  class ScolarshipType < Types::BaseObject
    field :id, ID, null: false
    field :amount, Float, null: false
    field :description,String, null: false
    field :type_scolarship, Types::TypeScolarshipType, null: false
    field :type_scolarship_id, Int, null: false
  end
end
