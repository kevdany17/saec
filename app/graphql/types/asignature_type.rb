module Types
  class AsignatureType < Types::BaseObject
    field :id, ID, null: false
    field :asignature_type, String, null: true
    field :asignature_name, String, null: true
    field :key, String , null: true
    field :academic_hours, Int, null: true
    field :independent_hours, Int, null: true
    field :credits, Int , null: true
    field :classroom, String, null: true
    field :semester, Int, null: true
    field :study_plans, [Types::StudyPlanType], null: true
    field :previous, Types::AsignatureType,null: true
    field :next, Types::AsignatureType, null: true
    field :previous_id, Int, null: true 
  end
end
