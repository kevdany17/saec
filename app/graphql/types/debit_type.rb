module Types
  class DebitType < Types::BaseObject
    field :id, ID, null: false
    field :amount, Float, null: false
    field :beca, Float, null: false
    field :total_cost, Float, null: false
    field :concept, Types::TuitionType, null: false
    field :cycle_id, Int, null: false
    field :user_id, Int, null: false
  end
end
