module Types
  class ScheduleType < Types::BaseObject
    field :id, ID, null: false
    field :cycle, Types::CycleType, null: false
    field :study_plan, Types::StudyPlanType, null: false
    field :asignature, Types::AsignatureType, null: false
    field :teacher, Types::TeacherType, null: false
    field :time_table, Types::TimeTableType, null: false
    field :day, String, null: false
    field :group, String, null: true
    field :installation, Types::InstallationType, null: false
    field :conflict, Boolean, null: true
  end
end
