module Types
  class TypeActivityType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :activities, [Types::ActivityType], null: false   
  end
end
