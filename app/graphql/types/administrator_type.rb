module Types
  class AdministratorType < Types::BaseObject
    field :id, ID, null: false
    field :rfc, String, null: true
    field :number_imss, String, null: true
    field :position, String, null: false
    field :area_id, String, null:false
    field :department, Types::DepartmentType, null: false
    field :acting_as, Types::UserType, null: false
  end
end
