module Types
  class TuitionType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :start_date, String, null: false
    field :finish_date, String, null: false
  end
end
