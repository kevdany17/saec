module Types
  class ModuleType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: true
    field :session_modules, [Types::SessionType], null: true
    field :activity, Types::ActivityType, null: false
    field :teacher, Types::TeacherType, null: true
    field :teacher_id, Int, null: true
  end
end
