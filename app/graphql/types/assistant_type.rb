module Types
  class AssistantType < Types::BaseObject
    field :id,ID, null: false
    field :registration, String, null: false
    field :ingresses, [Types::IngressType], null: false
    field :acting_as, Types::UserType, null: false
  end
end
