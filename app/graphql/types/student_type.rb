module Types
  class StudentType < Types::BaseObject
    field :id, ID, null: false
    field :study_plan, Types::StudyPlanType, null: false
    field :cycle, Types::CycleType, null: false
    field :inscription_type, Int, null: false
    field :acting_as,Types::UserType, null: false
    field :civil_status, String, null: false
    field :nacionality, String, null: false
    field :foreign_language, Boolean, null: false
    field :foreign_language_name, String, null: true
    field :disability, Boolean, null: false
    field :disability_name, String, null: true
    field :actual_job, String, null: true
    field :job_center, String, null: true
    field :job_center_address, String, null: true
    field :scolarship, Types::ScolarshipType, null: true
    field :registration, String , null: false
    field :emergency_contact, Types::EmergencyContactType, null: true
    field :graduation_institution, Types::GraduationInstitutionType, null: true
    field :semester, Int, null: false
    field :study_plan_id, Int, null: false
    field :scolarship_id, Int, null: true
    field :first_name, String, null: false
    field :schedule_for_study_plan_and_semester,[Types::ScheduleType], null: true

    def schedule_for_study_plan_and_semester
      Schedule.schedule_for_study_plan_and_semester(object.study_plan.id,object.semester,object.id)
    end

    def first_name
      return object.acting_as.first_name
    end
    
    field :last_name, String, null: false

    def last_name
      return object.acting_as.last_name
    end 

    field :second_name, String, null: false

    def second_name
      return object.acting_as.second_name
    end

    field :email, String, null: false

    def email
      return object.acting_as.email
    end  

    field :curp, String, null: false

    def curp
      return object.acting_as.curp
    end

    field :cellphone, String, null: false

    def cellphone
      return object.acting_as.cell_phone
    end
  end
end
