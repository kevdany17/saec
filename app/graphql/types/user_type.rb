module Types
  class UserType < Types::BaseObject
    description "Clase Padre User -> Hereda A ['Administrator','Teacher','Assistant','']"
    field :id, ID, null: false
    field :user_name, String, null: true
    field :first_name, String, null: false
    field :second_name, String, null: false
    field :last_name, String, null: false
    field :birth_day, String, null: true
    field :email, String, null: false
    field :curp, String, null: false
    field :phone, String, null: false
    field :cell_phone, String, null: false
    field :sexo, String, null: false
    field :roles, [Types::RoleType], null: false
    field :address, Types::AddressType, null: true
  end
end
