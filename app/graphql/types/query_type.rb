module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    field :users, [Types::UserType], null: false
    def users
      User.all
    end
    field :activities, [Types::ActivityType], null: false
    def activities
      Activity.where(status: true)
    end
    field :type_activities, [Types::TypeActivityType],null: false
    def type_activities
      TypeActivity.where(status: true)
    end

    field :type_activity, Types::TypeActivityType,null: false do
      argument :id, ID, required: true
    end

    def type_activity(id:)
      TypeActivity.find(id)
    end

    field :activity, Types::ActivityType, null: false do
      argument :id, ID, required: true
    end
    
    def activity(id:)
      Activity.find(id)
    end

    field :type_payments,[Types::TypePaymentType], null: false
    def type_payments
      TypePayment.where(status: true)
    end
    
    field :cycles,[Types::CycleType],null: false
    def cycles
      Cycle.all
    end 
    
    field :cycle, Types::CycleType,null: false do
      argument :id, ID, required: true
    end
    def cycle(id:)
      Cycle.find(id)
    end
    
    field :type_costs, [Types::TypeCostType],null: false
    def type_costs
      TypeCost.where(status: true)
    end
    
    field :user, Types::UserType, null: false do
      argument :id, ID, required: true
    end
    def user(id:)
      User.find(id)
    end
  
    field :teacher , Types::TeacherType, null: false do
      argument :id, ID, required: true
    end

    def teacher(id:)
      Teacher.find(id)
    end

    field :teachers, [Types::TeacherType], null: false

    def teachers
      Teacher.where(status: true)
    end

    field :address , Types::AddressType, null: false do
      argument :id, ID, required: true
    end

    def address(id:)
      Address.find(id)
    end

    field :debit, [Types::DebitType], null: false do 
      argument :id, ID, required: true
    end
    
    def debit(id:)
      Debit.where(user_id: id).where(status: 0)
    end
    

    field :ingress, Types::IngressType, null: false do
      argument :id, ID, required: true
    end  
    def ingress(id:)
      Ingress.find(id)
    end
    
    field :assistants,[Types::AssistantType], null: false
    def assistants
      Assistant.all
    end  

    field :assistant, Types::AssistantType, null: false do 
      argument :id, ID, required: true
    end
    def assistant(id:)
      Assistant.find(id)
    end
    
    field :administrators, [Types::AdministratorType], null: false
    def administrators
      Administrator.where(status: 1)
    end
    
    field :administrator, Types::AdministratorType, null: false do
      argument :id, ID, required: true
    end
    def administrator(id:)
      Administrator.find(id)
    end
    
    field :departments, [Types::DepartmentType], null: false 
    def departments
      Department.all
    end

    field :asignatures, [Types::AsignatureType], null: false
    def asignatures
      Asignature.where(status: 1)
    end
    
    field :asignature, Types::AsignatureType, null: false do
      argument :id, ID, required: true
    end
    def asignature(id:)
      Asignature.find(id)
    end

    field :study_plan_for_asignature, [Types::AsignatureType], null: false do
      argument :id, ID, required: true
    end
    def study_plan_for_asignature(id:)
      StudyPlan.find(id).asignatures
    end

    field :study_plans, [Types::StudyPlanType], null: false
    def study_plans 
      StudyPlan.where(status: 1)
    end
    field :study_plan, Types::StudyPlanType, null: false do
      argument :id, ID, required: true
    end
    def study_plan(id:)
      StudyPlan.find(id)
    end
    
    field :students, [Types::StudentType], null: false
    def students
      Student.where(status: 1)
    end
    
    field :student, Types::StudentType, null: false do
      argument :id, ID, required: true
    end  
    def student(id:)
      Student.find(id)
    end
    
    field :type_scolarships,[Types::TypeScolarshipType], null: false
    def type_scolarships
      TypeScolarship.where(status: 1)
    end

    field :type_scolarship, Types::TypeScolarshipType, null: false do
      argument :id, ID, required: true
    end
    def type_scolarship(id:)
      TypeScolarship.find(id)
    end    

    field :scolarships,[Types::ScolarshipType],null: false
    def scolarships
      Scolarship.where(status: 1)
    end 
    field :scolarship, Types::ScolarshipType, null: false do
      argument :id, ID, required: true
    end
    def scolarship(id:)
      Scolarship.find(id)
    end  
    
    field :emergency_contact, Types::EmergencyContactType, null: false do
      argument :id, ID, required: true
    end
    def emergency_contact(id:)
      EmergencyContact.find(id)
    end

    field :graduation_institution, Types::GraduationInstitutionType, null: false do
      argument :id, ID, required: true
    end
    def graduation_institution(id:)
      GraduationInstitution.find(id)
    end
    field :schedules, [Types::ScheduleType], null: false
    def schedules
      Schedule.all
    end
    field :schedule, Types::ScheduleType, null: false do
      argument :id, ID, required: true
    end
    def schedule(id:)
      Schedule.find(id)
    end
    # field :schedule_for_study_plan_and_semester, [Types::ScheduleType], null: false do
    #   argument :id_level_study, ID, required: true
    #   argument :semester, ID, required: true
    #   argument :student_id, ID, required: true
    # end
    # def schedule_for_study_plan_and_semester(id_level_study:,semester:,student_id:)
    #   Schedule.schedule_for_study_plan_and_semester(id_level_study,semester,student_id)
    # end
    field :timetables, [Types::TimeTableType], null: false
    def timetables
      TimeTable.all
    end
    field :time_table, Types::TimeTableType, null: false do
      argument :id, ID, required: true
    end
    def time_table(id:)
      TimeTable.find(id)
    end
    field :installations, [Types::InstallationType], null: false
    def installations
      Installation.all
    end
    field :installation, Types::InstallationType, null: false do
      argument :id, ID, required: true
    end
    def installation(id:)
      Installation.find(id)
    end 
    field :concepts_payments, [Types::ConceptPaymentType], null: false
    def concepts_payments
      ConceptPayment.all
    end
    field :concept_payment, Types::ConceptPaymentType, null: false do
      argument :id, ID, required: true
    end
    def concept_payment(id:)
      ConceptPayment.find(id)
    end
    field :modalities, [Types::ModalityType], null: false
    def modalities
      Modality.all
    end
    field :modality, Types::ModalityType, null: false do
      argument :id, ID, required: true
    end
    def modality(id:)
      Modality.find(id)
    end  
  end
end
