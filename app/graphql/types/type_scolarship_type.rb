module Types
  class TypeScolarshipType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :scolarships, [Types::ScolarshipType], null: false
  end
end
