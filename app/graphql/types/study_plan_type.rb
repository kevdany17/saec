module Types
  class StudyPlanType < Types::BaseObject
    field :id,ID,null: false
    field :name, String, null: false
    field :suffix, String, null: false
    field :grade, String, null: false
    field :rvoe, String, null: false
    field :start_valid_date, String,null: false
    field :finish_valid_date, String,null: false
    field :status, Int, null: false
    field :asignatures, [Types::AsignatureType], null: true
  end
end
