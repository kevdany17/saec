module Types
  class AddressType < Types::BaseObject
    field :id, ID, null: false
    field :street, String, null: true
    field :number_out, String, null: true
    field :number_in, String, null: true
    field :code_postal, String, null: true
    field :colony, String, null: true
    field :municipality, String, null: true
    field :state, String, null: true
    field :city, String, null: true
    field :status,Int, null: true
  end
end
