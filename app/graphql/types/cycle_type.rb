module Types
  class CycleType < Types::BaseObject
    field :id,ID,null: false
    field :name, String, null: false
    field :status, Integer, null: false
    field :startDate, String, null: false
    field :finishDate, String, null: false
    field :activities, [Types::ActivityType],null: false
    field :study_plan, [Types::StudyPlanType], null: true

    def activities
      return object.activities.where(status: 1)
    end
    
    field :assistants, [Types::AssistantType], null: false
  end
end
