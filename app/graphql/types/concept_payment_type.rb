module Types
  class ConceptPaymentType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :description, String, null: false
    field :cost, Float, null: false
  end
end
