module Types
  class GraduationInstitutionType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :average, Float, null: true
    field :specialty, String, null: true
  end
end
