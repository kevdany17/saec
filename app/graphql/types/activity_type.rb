module Types
  class ActivityType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :start_date, String, null: false
    field :finish_date, String, null: false
    field :total_hours, Int, null: false
    field :inscription, Float, null: true
    field :tuition, Float, null: true
    field :number_tuition, Int, null: true
    field :description, String, null: true
    field :status, Int, null: true
    field :type_cost, Types::TypeCostType, null: true
    field :type_activity, Types::TypeActivityType,null: false
    field :cycle, Types::CycleType,null: false
    field :module_activities, [Types::ModuleType], null: false
    field :tuitions, [Types::TuitionType], null: false
    field :teachers, [Types::TeacherType], null: true
    field :assistants, [Types::AssistantType], null: true
  end
end
