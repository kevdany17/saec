module Types
  class RoleType < Types::BaseObject
    field :name, String, null: false
  end
end
