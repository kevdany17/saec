module ActivitiesHelper
    def options_for_status
        [
            ['Activo',1],
            ['Inactivo',2]
        ]
    end
end
