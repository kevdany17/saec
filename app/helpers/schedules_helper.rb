module SchedulesHelper
    class ValidateSchedule
        def self.exist(params)
            #Todos los Datos Iguales
            @schedule = Schedule.where(cycle_id: params['cycle_id'],study_plan_id: params['study_plan_id'],asignature_id: params['asignature_id'],time_table_id: params['time_table_id'],day: params['day'],teacher_id: params['teacher_id'],group: params['group'],installation_id: params['installation_id'])
            if @schedule.size > 0
                raise 'El horario ya existe.'
            end
            #Horario Dia, Grupo e Instalaciones Iguales
            @schedule = Schedule.where(cycle_id: params['cycle_id'],study_plan_id: params['study_plan_id'],asignature_id: params['asignature_id'],time_table_id: params['time_table_id'],day: params['day'],group: params['group'],installation_id: params['installation_id'])
            if @schedule.size > 0
                raise 'La asignatura esta duplicando horario, día, grupo e instalaciones.'
            end
            #Horario, Dia e Instalaciones Iguales, Grupo y Docente Diferentes
            @schedule = Schedule.where(cycle_id: params['cycle_id'],study_plan_id: params['study_plan_id'],asignature_id: params['asignature_id'],time_table_id: params['time_table_id'],day: params['day'],installation_id: params['installation_id'])
            if @schedule.size > 0
                raise 'La asignatura esta duplicando horario, día, e instalaciones.'
            end
            #Horario,Docente, Dia e Instalaciones Iguales, Diferente Grupo
            @schedule = Schedule.where(cycle_id: params['cycle_id'],study_plan_id: params['study_plan_id'],asignature_id: params['asignature_id'],time_table_id: params['time_table_id'],day: params['day'],installation_id: params['installation_id'],teacher_id: params['teacher_id'])
            if @schedule.size > 0
                raise 'La asignatura esta duplicando horario, día, docente e instalaciones.'
            end
            #hHorario, y Docente Iguales. Diferente Grupo e Instalaciones
            @schedule = Schedule.where(cycle_id: params['cycle_id'],study_plan_id: params['study_plan_id'],asignature_id: params['asignature_id'],time_table_id: params['time_table_id'],day: params['day'],teacher_id: params['teacher_id'])
            if @schedule.size > 0
                raise 'La asignatura esta duplicando horario, día, docente.'
            end
            return params
        end
    end
end
