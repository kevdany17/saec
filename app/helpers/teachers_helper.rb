module TeachersHelper
    def options_for_master
        [
            ['Si',true],
            ['No',false]
        ]
    end
    def options_for_doctorate
        [
            ['Si',true],
            ['No',false]
        ]
    end
    def options_for_sex
        [
            ['Masculino','Masculino'],
            ['Femenino','Femenino']
        ]
    end
    def options_for_estado
        [
            ['Alta',1],
            ['Baja',0]
        ]
    end
end
