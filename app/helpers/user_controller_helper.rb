module UserControllerHelper
    class Enrollment
        def self.getEnrollment(id,type,cycle_id)
            self.cycle!(cycle_id)
            if type == 'AP'
                return "AP/#{ self.formater(id) }/#{@ciclo}"
            else
                ## Crear Una Matricula para alumnos todas las licenciaturas comparten la serialización
                ## Popsible cambio una númeración por licenciatura
                return "#{ type }/#{ id }/#{@ciclo}"
            end
        end
        private
            def self.cycle!(id)
                #month = Time.now.strftime("%m")
                @cycle = Cycle::find(id)
                month = @cycle.start_date.strftime("%m")
                if month.to_i < 7
                    @ciclo = "#{ Time.now.strftime("%g") }-2"
                else
                    @ciclo = "#{ Time.now.strftime("%g").to_i + 1 }-1"
                end
            end
            def self.formater(id)
                if id < 10
                return  "000#{ id }"
                elsif id >= 10 && id < 100
                return  "00#{ id }"
                elsif id >=100 && id < 1000
                return  "0#{ id }"
                elsif id >=1000 && id < 10000
                return "#{ id }"
                end  
            end
    end
end
