module ReportsHelper
    class DateFormatter
        def self.date_activity_to_string(activity)
            if(activity.module_activities.length < 1)
                return "el día #{activity.finish_date.strftime('%d')} de #{month_in_spanish(activity.finish_date)}"
            else
                @dates = []
                @string = 'los días '
                for module_activity in activity.module_activities
                    for session in module_activity.session_modules
                        @dates << session.date
                    end
                end
                @dateForAnio = []
                @year = @dates[0].strftime('%Y')
                for anio in @dates
                    if anio.strftime('%Y') == @year
                        @dateForAnio << anio
                    end
                    if @dateForAnio.length == @dates.length || anio.strftime('%Y') != @year
                        @listOne = @dateForAnio
                        @dateForAnio =  []
                        if anio.strftime('%Y') != @year                          
                            @dateForAnio << anio
                            @year = anio.strftime('%Y')
                        end
                        #Separar por Mes
                        @dateForMonth = []
                        @month = @listOne[0].strftime('%B')
                        for month in @listOne
                            if month.strftime('%B') == @month
                                @dateForMonth << month
                            end
                            
                            if @dateForMonth.length == @listOne.length || month.strftime('%B') != @month
                                @listTwo = @dateForMonth
                                @dateForMonth = []
                                if month.strftime('%B') != @month
                                    @dateForMonth << month
                                    @month = month.strftime('%B')
                                end
                                #Imprimir Días
                                @limit = @listTwo.length
                                @i = 0
                                for day in @listTwo
                                    if(@i == (@limit - 2))
                                        @string += day.strftime('%d') + ' y '
                                    elsif (@i == (@limit - 1))
                                        @string += day.strftime('%d') + ' de '
                                    else
                                        @string += day.strftime('%d') + ', '
                                    end
                                    @i += 1
                                end
                                if month.strftime('%B') != @month
                                    @string += ' ' + month_in_spanish(@listTwo[0]) + ','
                                elsif @dateForMonth.length > 0
                                    @string += ' ' + month_in_spanish(@listTwo[0]) + ' y '
                                else
                                    @string += ' ' + month_in_spanish(@listTwo[0])
                                end
                                 
                            end
                        end
                        if @dateForMonth.length > 0
                            @listTwo = @dateForMonth
                            #Imprimir Días
                            @limit = @listTwo.length
                            @i = 0
                            for day in @listTwo
                                if(@i == (@limit - 2))
                                    @string += day.strftime('%d') + ' y '
                                elsif (@i == (@limit - 1))
                                    @string += day.strftime('%d') + ' de '
                                else
                                    @string += day.strftime('%d') + ', '
                                end
                                @i += 1
                            end
                            @string += ' ' + month_in_spanish(@listTwo[0]) 
                        end 
                        if anio.strftime('%Y') != @year
                            @string += ' ' + @listOne[0].strftime('%Y') + ','
                        else
                            @string += ' ' + @listOne[0].strftime('%Y') 
                        end                        
                    end
                end
                if @dateForAnio.length > 0
                    @listOne = @dateForAnio
                    #Separar por Mes
                    @dateForMonth = []
                    @month = @listOne[0].strftime('%B')
                    for month in @listOne
                        if month.strftime('%B') == @month
                            @dateForMonth << month
                        end
                        
                        if @dateForMonth.length == @listOne.length || month.strftime('%B') != @month
                            @listTwo = @dateForMonth
                            @dateForMonth = []
                            if month.strftime('%B') != @month
                                @dateForMonth << month
                                @month = month.strftime('%B')
                            end
                            #Imprimir Días
                            @limit = @listTwo.length
                            @i = 0
                            for day in @listTwo
                                if(@i == (@limit - 2))
                                    @string += day.strftime('%d') + ' y '
                                elsif (@i == (@limit - 1))
                                    @string += day.strftime('%d') + ' de '
                                else
                                    @string += day.strftime('%d') + ', '
                                end
                                @i += 1
                            end
                            if month.strftime('%B') != @month 
                                @string += ' de ' + month_in_spanish(@listTwo[0]) + ','
                            elsif @dateForMonth.length > 0
                                @string += ' ' + month_in_spanish(@listTwo[0]) + ' y '
                            else
                                @string += ' de ' + month_in_spanish(@listTwo[0]) 
                            end
                            
                        end
                    end
                    if @dateForMonth.length > 0
                        @listTwo = @dateForMonth
                        #Imprimir Días
                        @limit = @listTwo.length
                        @i = 0
                        for day in @listTwo
                            if(@i == (@limit - 2))
                                @string += day.strftime('%d') + ' y '
                            elsif (@i == (@limit - 1))
                                @string += day.strftime('%d')
                            else
                                @string += day.strftime('%d') + ', '
                            end
                            @i += 1
                        end
                        @string += ' ' + month_in_spanish(@listTwo[0]) 
                    end 
                    @string += ' de ' + @listOne[0].strftime('%Y') + ' '
                end
                return @string
            end
            
        end
        def self.month_in_spanish(month)
            if month.strftime('%B') == 'January' 
                return 'Enero'
            elsif month.strftime('%B') == 'February'
                return 'Febrero'
            elsif month.strftime('%B') == 'March'
                return 'Marzo'
            elsif month.strftime('%B') == 'April'
                return 'Abril'
            elsif month.strftime('%B') == 'May'
                return 'Mayo'
            elsif month.strftime('%B') == 'June'
                return 'Junio'
            elsif month.strftime('%B') == 'July'
                return 'Julio'
            elsif month.strftime('%B') == 'August'
                return 'Agosto'
            elsif month.strftime('%B') == 'September'
                return 'Septiembre'
            elsif month.strftime('%B') == 'October'
                return 'Octubre'
            elsif month.strftime('%B') == 'November'
                return 'Noviembre'
            elsif month.strftime('%B') == 'Dicember'
                return 'Diciembre'
            end
        end
    end
end
