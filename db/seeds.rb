# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
begin
    Cycle.create(name: "Julio-Diciembre",status: 1,start_date: "2020-06-01",finish_date: "2020-12-31")
    Assistant.create!(email: "kevhernandez@gmail.com",password: "123456",password_confirmation: "123456", first_name: "Manuel", second_name: "Jimenez", last_name: "Rodriguez",birth_day: "1993-05-11",curp: "AAAA1",cell_phone: "9511428983", sexo: "Masculino", registration: "AP/0001/20-1",cycle_id:1)
    Role.create!([{:name => "admin"},{:name => "direccion"},{:name => "coordinate_ac"},{:name => "regain"}])
    Department.create!([{:name => 'Dirección'},{:name => "Actualización Profesional"},{:name => "Cobranzas"}])
    user = Administrator.create!(email: "admin@impulzo.com",password: "comesi.2018",password_confirmation: "comesi.2018", first_name: "Jaime", second_name: "Padilla", last_name: "Ramírez",birth_day: "1993-05-11",curp: "AAAAAA2",cell_phone: "9511428983", sexo: "Masculino",position: "Director General", area_id: 1)
    user.add_role :admin
    user = Administrator.create!(email: "direccion@comesi.com",password: "comesi2018",password_confirmation: "comesi2018", first_name: "Jaime", second_name: "Padilla", last_name: "Ramírez",birth_day: "1993-05-11",curp: "AAAAAAA3",cell_phone: "9511428983", sexo: "Masculino",position: "Director General", area_id: 1)
    user.add_role :direccion
    TypeCost.create!([{:name => "Inscripción",:description => "Pago en Una sola Exhibición"},{:name => "Inscripción y Colegiaturas",:description => "Varios Pagos"}])
    TypePayment.create!([{:name => "Efectivo"},{:name => "Depósito Bancario"},{:name => "Transferencia"},{:name => "Tarjeta de Crédito"}])
    TypeActivity.create!([{:name => "Taller",:description=>"Taller",:status=>1},{:name => "Curso",:description=>"Curso",:status=>1},{:name => "Diplomado",:description=>"Diplomado",:status=>1}])
    user = Administrator.create!(email: "coordinador@comesi.com",password: "comesi2018",password_confirmation: "comesi2018", first_name: "A", second_name: "A", last_name: "A",birth_day: "1993-05-11",curp: "AAAAAA",cell_phone: "A", sexo: "Masculino",position: "A", area_id: 2)
    user.add_role :coordinate_ac
    user = Administrator.create!(email: "cobranza@comesi.com",password: "comesi2018",password_confirmation: "comesi2018", first_name: "A", second_name: "A", last_name: "A",birth_day: "1993-05-11",curp: "BBBBBB",cell_phone: "A", sexo: "Masculino",position: "A", area_id: 1)
    user.add_role :regain
    Invoice.create!(name: 'ActualizacionProfesional',number: 0)
    Invoice.create!(name: 'NivelSuperior',number: 0)
rescue StandardError => e  
    puts e.message  
end
