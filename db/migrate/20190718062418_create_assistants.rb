class CreateAssistants < ActiveRecord::Migration[5.2]
  def change
    create_table :assistants do |t|
      t.string :registration
      t.integer :cycle_id
      t.integer :level_study_id, null: true
      t.timestamps
    end
  end
end
