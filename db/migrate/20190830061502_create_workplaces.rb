class CreateWorkplaces < ActiveRecord::Migration[5.2]
  def change
    create_table :workplaces do |t|
      t.string :name
      t.string :phone
      t.integer :status

      t.timestamps
    end
  end
end
