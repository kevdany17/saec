class CreateCyclesStudyPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :cycles_study_plans do |t|
      t.integer :cycles_id
      t.integer :study_plan_id
    end
  end
end
