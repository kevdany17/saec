class CreateTypeScolarships < ActiveRecord::Migration[5.2]
  def change
    create_table :type_scolarships do |t|
      t.string :name
      t.integer :status, default: 1

      t.timestamps
    end
  end
end
