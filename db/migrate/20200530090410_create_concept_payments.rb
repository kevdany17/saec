class CreateConceptPayments < ActiveRecord::Migration[5.2]
  def change
    create_table :concept_payments do |t|
      t.string :name
      t.string :description
      t.decimal :cost, :precision => 10, :scale => 2

      t.timestamps
    end
  end
end
