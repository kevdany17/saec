class CreateTeachers < ActiveRecord::Migration[5.2]
  def change
    create_table :teachers do |t|
      t.string :licenciatura
      t.string :specialty
      t.string :master
      t.string :doctorate
      t.string :identification_licenciatura
      t.string :identification_specialty
      t.string :identification_master
      t.string :identification_doctorate
      t.timestamps
    end
  end
end
