class CreateAsignatures < ActiveRecord::Migration[5.2]
  def change
    create_table :asignatures do |t|
      t.string :asignature_type
      t.string :asignature_name
      t.string :key
      t.integer :academic_hours
      t.integer :independent_hours
      t.integer :credits
      t.string :classroom
      t.integer :semester
      t.integer :status, default: 1
      t.references :previous
      t.timestamps
    end
  end
end
