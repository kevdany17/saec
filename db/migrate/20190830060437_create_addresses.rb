class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.integer :user_id
      t.string :street
      t.string :number_out
      t.string :number_in, null: true
      t.string :code_postal
      t.string :colony
      t.string :municipality
      t.string :state
      t.string :city
      t.integer :status, default: 1
      t.bigint :owner_id
      t.string :owner_type
      t.timestamps
    end
    add_index :addresses, [:owner_type, :owner_id]
  end
end
