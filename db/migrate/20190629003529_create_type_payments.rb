class CreateTypePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :type_payments do |t|
      t.string :name
      t.integer :status, null: false, default: 1
      t.timestamps
    end
  end
end
