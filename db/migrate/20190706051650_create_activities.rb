class CreateActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :activities do |t|
      t.string :name
      t.integer :type_activity_id
      t.date :start_date
      t.date :finish_date
      t.integer :total_hours
      t.integer :type_cost_id
      t.decimal :inscription
      t.decimal :tuition,:default => 0 
      t.integer :number_tuition,:default => 0 
      t.integer :status, null: false, default: 1
      
      t.integer :cycle_id
      t.text :description

      t.timestamps
    end
  end
end
