class CreateSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :schedules do |t|
      t.integer :cycle_id
      t.integer :study_plan_id
      t.integer :asignature_id
      t.integer :teacher_id
      t.integer :time_table_id
      t.string :day
      t.string :group
      t.string :installation_id
      t.boolean :conflict

      t.timestamps
    end
  end
end
