class CreateTimeTables < ActiveRecord::Migration[5.2]
  def change
    create_table :time_tables do |t|
      t.string :range_hours

      t.timestamps
    end
  end
end
