class CreateAsignaturesStudyPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :asignatures_study_plans, id: false do |t|
      t.belongs_to :asignature 
      t.belongs_to :study_plan
      t.timestamps
    end
  end
end
