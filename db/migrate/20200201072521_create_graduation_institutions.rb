class CreateGraduationInstitutions < ActiveRecord::Migration[5.2]
  def change
    create_table :graduation_institutions do |t|
      t.string :name
      t.float :average
      t.string :specialty
      t.integer :status, default: 1
      t.integer :student_id

      t.timestamps
    end
  end
end
