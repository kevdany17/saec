class CreateAdministrators < ActiveRecord::Migration[5.2]
  def change
    create_table :administrators do |t|
      t.string :rfc, null: true
      t.string :number_imss, null: true
      t.string :position, null: false
      t.string :area_id, null: false
      t.timestamps
    end
  end
end
