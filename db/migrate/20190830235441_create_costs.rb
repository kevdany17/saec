class CreateCosts < ActiveRecord::Migration[5.2]
  def change
    create_table :costs do |t|
      t.decimal :inscription_lic
      t.decimal :inscription_master
      t.decimal :reinscription_lic
      t.decimal :reinscription_master
      t.decimal :tuition_lic
      t.decimal :tuition_master
      t.integer :study_plan_id
      t.timestamps
    end
  end
end
