class CreateSessionModules < ActiveRecord::Migration[5.2]
  def change
    create_table :session_modules do |t|
      t.integer :module_activity_id
      t.date :date

      t.timestamps
    end
  end
end
