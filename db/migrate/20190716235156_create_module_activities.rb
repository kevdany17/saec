class CreateModuleActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :module_activities do |t|
      t.string :name
      t.integer :activity_id
      t.integer :teacher_id, default: 0
      t.timestamps
    end
  end
end
