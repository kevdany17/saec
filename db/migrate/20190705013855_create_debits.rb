class CreateDebits < ActiveRecord::Migration[5.2]
  def change
    create_table :debits do |t|
      t.integer :user_id
      t.bigint  :concept_id
      t.string  :concept_type
      t.decimal :amount
      t.date :limit_date
      t.decimal :total_cost
      t.integer :cycle_id
      t.integer :beca
      #t.integer :module_activity_id
      t.integer :status, null: false, default: 1
      t.timestamps
    end
  end
end
