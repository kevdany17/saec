class CreateTuitions < ActiveRecord::Migration[5.2]
  def change
    create_table :tuitions do |t|
      t.string :name
      t.integer :activity_id
      t.date :start_date
      t.date :finish_date

      t.timestamps
    end
  end
end
