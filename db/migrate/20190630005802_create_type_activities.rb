class CreateTypeActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :type_activities do |t|
      t.string :name
      t.string :description, null: true
      t.integer :status, null: false, default: 1
      t.timestamps
    end
  end
end
