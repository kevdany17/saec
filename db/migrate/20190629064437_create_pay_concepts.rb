class CreatePayConcepts < ActiveRecord::Migration[5.2]
  def change
    create_table :pay_concepts do |t|
      t.string :name
      t.string :description
      t.decimal :cost
      t.integer :status, null: false, default: 1
      t.timestamps
    end
  end
end
