class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :civil_status
      t.string :nacionality
      t.boolean :foreign_language
      t.string :foreign_language_name
      t.boolean :disability
      t.string :disability_name
      t.string :actual_job
      t.string :job_center
      t.string :job_center_address
      t.integer :scolarship_id
      t.string :registration
      t.integer :cycle_id
      t.string :inscription_type
      t.integer :study_plan_id
      t.integer :semester, default: 1
      
      t.timestamps
    end
  end
end
