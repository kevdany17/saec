class CreateCycles < ActiveRecord::Migration[5.2]
  def change
    create_table :cycles do |t|
      t.string :name
      t.integer :status, null: false, default: 1
      t.date :start_date
      t.date :finish_date
      t.timestamps
    end
  end
end
