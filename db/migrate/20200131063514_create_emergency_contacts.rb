class CreateEmergencyContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :emergency_contacts do |t|
      t.string :name
      t.string :cell_phone
      t.string :telephone
      t.string :relationship
      t.integer :student_id
      t.integer :status, default: 1

      t.timestamps
    end
  end
end
