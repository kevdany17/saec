class CreateAssistantsWorkplaces < ActiveRecord::Migration[5.2]
  def change
    create_table :assistants_workplaces do |t|
      t.integer :assistant_id
      t.integer :workplace_id
      t.string :position
    end
  end
end
