class CreateActivitiesTeachers < ActiveRecord::Migration[5.2]
  def change
    create_table :activities_teachers do |t|
      t.integer :activity_id
      t.integer :teacher_id
    end
  end
end
