class CreateConcertedQualifications < ActiveRecord::Migration[5.2]
  def change
    create_table :concerted_qualifications do |t|
      t.integer :student_id
      t.integer :schedule_id
      t.integer :modality_id
      t.decimal :assistance_first_partial, precision: 10, scale: 2
      t.decimal :assistance_second_partial, precision: 10, scale: 2
      t.decimal :assistance_third_partial, precision: 10, scale: 2
      t.decimal :score_first_partial, precision: 10, scale: 2
      t.decimal :score_second_partial, precision: 10, scale: 2
      t.decimal :score_third_partial, precision: 10, scale: 2
      t.integer :ordinary
      t.integer :extraordinary
      t.integer :assistence_final
      t.integer :score_final
      t.integer :semester

      t.timestamps
    end
  end
end
