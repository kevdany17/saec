class CreateStudyPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :study_plans do |t|
      t.string :name
      t.string :suffix
      t.string :grade
      t.string :rvoe
      t.date :start_valid_date
      t.date :finish_valid_date
      t.integer :status, default: 1

      t.timestamps
    end
  end
end
