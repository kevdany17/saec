class CreateScolarships < ActiveRecord::Migration[5.2]
  def change
    create_table :scolarships do |t|
      t.float :amount, default: 0
      t.string :description
      t.integer :status, default: 1
      t.integer :type_scolarship_id

      t.timestamps
    end
  end
end
