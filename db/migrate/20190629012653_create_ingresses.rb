class CreateIngresses < ActiveRecord::Migration[5.2]
  def change
    create_table :ingresses do |t|
      t.integer :debit_id
      t.integer :type_payment_id
      t.date :date
      t.integer :cycle_id
      t.integer :invoice_number
      t.timestamps
    end
  end
end
