class CreateActivitiesAssistants < ActiveRecord::Migration[5.2]
  def change
    create_table :activities_assistants do |t|
      t.integer :activity_id
      t.integer :assistant_id
      t.timestamps
    end
  end
end
