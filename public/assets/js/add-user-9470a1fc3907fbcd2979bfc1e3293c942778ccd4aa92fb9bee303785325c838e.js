function sendFormUser(){
    var Model =  {
        first_name: $("#first-name").val(),
        second_name: $("#second-name").val(),
        last_name: $("#last-name").val(),
        email:$("#email").val(),
        curp:$("#curp").val(),
        cell_phone:$("#cell-phone").val(),
        sexo:$("#sexo").val(),
        password:$("#password").val()
    };
    $.ajax({
        url: BASE_URL + '/registrar.json',
        data: {user:Model},
        async:true,
        dataType:'json',
        type: 'post',
        success:function(response){
            //response.attr
            if(response.status==200){
                $("#error-div").css('display','none');
                $("#success-message").css('display','block');
                var data = response.data;
                if($("#form-first-name") != undefined){
                    $("#form-first-name").text(data.first_name);
                }
                if($("#form-second-name") != undefined){
                    $("#form-second-name").text(data.second_name);
                }
                if($("#form-last-name") != undefined){
                    $("#form-last-name").text(data.last_name);
                }
                if($("#form-email") != undefined){
                    $("#form-email").text(data.email);
                }
                if($("#form-curp") != undefined){
                    $("#form-curp").text(data.curp);
                }
                if($("#form-cell-phone") != undefined){
                    $("#form-cell-phone").text(data.cell_phone);
                }
                $("input[type!=password]").each(function(){
                    $(this).val('');
                });
                Swal.fire(
                    'Éxito!',
                    'El usuario se registro correctamente',
                    'success'
                ).then((result) => {
                    if (result.value) {
                    location.reload()
                  }
                });
            }else{
                alert("Ha Ocurrido un Error. Contácte al Administrador.");
            }
        },
        error:function(response){
            $("#success-message").css('display','none');
            var data = JSON.parse(response.responseText).data;
            $("#errors").html("");
            for(var i in data){
                $("#errors").append("<li>"+ data[i]+"</li>");
            }
            $("#error-div").css('display','block');
        }
    });
}
;
