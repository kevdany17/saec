$(document).ready(function () {
    $('#frame').on('click', function () {
        $('#modal-usuario').modal('show');
    });
    document.getElementById("modal-pagar").onsubmit = function(){
        location.reload(true);
    }
    $('.select-user').on('click', function () {

        var valores = [];
        // Obtenemos todos los valores contenidos en los <td> de la fila
        // seleccionada
        $(this).parents("td").parents('tr').find(".valor").each(function () {
            valores.push($(this).html());
        });

        $('#form-first-name').text(valores[1]);
        $('#form-second-name').text(valores[2]);
        $('#form-last-name').text(valores[3]);
        $('#form-email').text(valores[4]);
        $('#form-curp').text(valores[5]);
        $('#form-cell-phone').text(valores[6]);
        $('#form-matricula').text(valores[7])
        $('#user-id').val(valores[0]);
        $('#modal-usuario').modal('hide');
        axios.post("/graphql", {
            query: `{
        debit(id:${ $('#user-id').val()}) {
          id
          amount
          beca
          totalCost
          cycleId
          concept{
            name
          }
        }
      }`}).then(response => {
                $('#table-body').empty();
                response.data.data.debit.forEach(function (element) {
                    $('#table-body').append(`<tr>
                <td id="id${element['id']}" style='display:none'>${element['id']}</td>
                <td id="cycle${element['id']}" style='display:none'>${element['cycleId']}</td>
                <td class='text-uppercase' id="nombre${element['id']}">${element['concept']['name']}</td>
                <td id="cantidad${element['id']}">$ ${element['amount']} </td>
                <td id="beca${element['id']}">${element['beca']}</td>
                <td id="total${element['id']}">$ ${element['totalCost']} </td>
                <td> <button id='${element['id']}' class='adeudo btn btn-primary'>Pagar</button> </td></tr>`);
                });
                pagar();
            })
            .catch(error => {
                Swal.fire("Error!", "Ocurrio un error al comunicarse con el servidor", "error");
            });
        // $.ajax({
        //     url: "/ver/adeudos/actividades/",
        //     type: "get",
        //     data: `datos[id]=${$('#user-id').val()}`,
        //     success: function (response) {
        //         $('#table-body').empty();
        //         response.forEach(function (element) {
        //             $('#table-body').append(`<tr>
        //         <td id="id${element['id']}" style='display:none'>${element['id']}</td>
        //         <td id="nombre${element['id']}">${element['name']}</td>
        //         <td id="cantidad${element['id']}">$ ${element['amount']} </td>
        //         <td id="beca${element['id']}">${element['beca']}% </td>
        //         <td id="total${element['id']}">$ ${element['total_cost']} </td>
        //         <td> <button id='${element['id']}' class='adeudo btn btn-primary'>Pagar</button> </td></tr>`);
        //         });
        //         pagar();
        //     }
        // })
    });
    var pagar = function () {
        $('.adeudo').click(function () {
            id_adeudo = $(this).attr('id');
            $('#modal-confirmar').modal().show();
            $('#nombre-modal').val(`${$('#form-first-name').text()} ${$('#form-second-name').text()} ${$('#form-last-name').text()}`);
            $('#concept-modal').val($(`#nombre${id_adeudo}`).text());
            $('#monto-modal').val($(`#cantidad${id_adeudo}`).text());
            $('#beca-modal').val($(`#beca${id_adeudo}`).text());
            $('#total-modal').val($(`#total${id_adeudo}`).text());
            $('#id-modal').val($(`#id${id_adeudo}`).text());
            $('#id-cycle').val($(`#cycle${id_adeudo}`).text());
        })
    };
})
;
