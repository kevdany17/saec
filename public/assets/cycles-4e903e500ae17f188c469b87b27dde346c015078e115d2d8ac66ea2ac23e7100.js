$( document ).ready(function() {
// se asignan fechas por default
var fecha = new Date();
if(document.getElementById("fecha-inicial"))
  if((document.getElementById("fecha-inicial").value=="") && (document.getElementById("cycle_finish_date").value=="")){
      if((fecha.getMonth()+1)<=6){
          document.getElementById("fecha-inicial").value = "01/01/2019";
          document.getElementById("cycle_finish_date").value = "30/06/2019";
      }else{
          document.getElementById("fecha-inicial").value = fecha.getFullYear()+"-07-01";
          document.getElementById("cycle_finish_date").value = fecha.getFullYear()+"-12-30";
      }
  }

// Función que muestra un modal de confirmación antes de eliminar un ciclo

$(".btn-eliminar").on("click",function(elem){
  Swal.fire({
        title: '¿Estas seguro?',
        text: "¡No podras deshacer este cambio!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, eliminar!'
      }).then((result) => {
        //console.log(result.value);
        if (result) {
          axios.delete('/ciclos/'+elem.target.id).then(
            function(response) {
              if(response.data.status == "200")
              Swal.fire(
                  'Éxito!',
                  response.data.msg,
                  'success'
                ).then((result) => {
                  if (result) {
                  location.reload()
                }
              }
                )
          }
          ).catch(
            function(response) {
              Swal.fire(
                  'Error!',
                  'El registro no se pudo eliminar!',
                  'error'
                )
            }
          );
        }
      })
})
});
