var inscription = 0;
$("#other-inscription").val(0);
$("#other-tuition").val(0);
$("#user-id").val(0);
$("#activity-id").val(0);
$("#cycle-id").val(0);
var radios = document.getElementsByClassName("radio-button");
for(i in radios){
    radios[i].onclick = function(){
        showOtherBeca();
        inscription = parseFloat($("#activity-inscription").text().replace("$",'').trim());
        if(!isNaN(inscription)){
            var idOne = $("input[name=beca-inscription]:checked").attr('id');
            var idTwo = $("input[name=beca-tuition]:checked").attr('id');
            if(idOne=="beca-radio-inscription"){
                $("#other-inscription").on('change',calculate);
            }else{
                calculate();
            }
            if(idTwo=="beca-radio-tuition"){
                $("#other-tuition").on('change',calculate);
            }else{
                calculate();
            }
        }else{
            $("#log-payments").html('');
        }
    };
    if(i==0 || i==6){
        radios[i].checked = true;
    }             
}
$("#clickable-wizard").on('submit',function(e){
    e.preventDefault();
    inscriptionUser();
    
});

function showOtherBeca(){
    var radioInscription = document.getElementById("beca-radio-inscription");
    var radioTuition = document.getElementById("beca-radio-tuition");
    if(radioInscription.checked){
        document.getElementById("input-other-inscription").style.display = 'block';
    }else{
        document.getElementById("input-other-inscription").style.display = 'none';
        $("#other-inscription").val(0);
    }
    if(radioTuition.checked){
        document.getElementById("input-other-tuition").style.display = 'block';
    }else{
        document.getElementById("input-other-tuition").style.display = 'none';
        $("#other-tuition").val(0);
    }
}
function calculate(){
    var row = '';
    var subtotal = new Array();
    var becaIns = $("input[name=beca-inscription]:checked").val();
    if(becaIns==-1){
        var otherIns = $("#other-inscription").val();
        subtotal.push(getCost(otherIns,inscription));
    }else{
        subtotal.push(getCost(becaIns,inscription));
    }
    var numberTuition = parseInt($("#number-tuition").text().trim());
    if(!isNaN(numberTuition) && numberTuition>0){
        var tuition = parseFloat($("#activity-tuition").text().replace("$",'').trim());
        for(var i = 0;i < numberTuition;i++){
            var becaCol = $("input[name=beca-tuition]:checked").val();
            var costo = 0;
            if(becaCol==-1){
                var otherCol = $("#other-tuition").val();   
                costo = getCost(otherCol,tuition);
            }else{
                costo= getCost(becaCol,tuition);
            }
            if(costo>-1){
                subtotal.push(costo);
            }else{
                subtotal.push(0);
            }
        }
    }
    for(var i in subtotal){
        if(i==0){
            row = "<tr><td>Inscripción</td><td>$ "+subtotal[i].toFixed(2)+"</td></tr>";
        }else{
            row += "<tr><td>Colegiatura</td><td>$ "+subtotal[i].toFixed(2)+"</td></tr>";
        }
    }
    $("#log-payments").html(row); 
}
function inscriptionUser(){
    var becaOne = $("input[name=beca-inscription]:checked").val();
    if(becaOne==-1){
        becaOne = $("#other-inscription").val();
    }
    var becaTwo = $("input[name=beca-tuition]:checked").val();
    if(becaTwo==-1){
        becaTwo = $("#other-tuition").val();
    }
    var data = {
        user_id:$("#user-id").val(),
        activity_id:$("#activity-id").val(),
        beca_percent_inscription:becaOne,
        beca_percent_tuition:becaTwo,
        type_payment:$("#type-payment option:selected").val(),
        cycle_id:$("#cycle-id").val()
    };
    $.ajax({
        url: "/actividad/registrar/usuario",
        type: "post",
        contentType: "application/json",
        data: JSON.stringify(data),
        success:function(response) {
            if(response.status==200){
                $("#body-notify").html('<div class="alert alert-success">'+response.msg+'</div>');
                $("#url-recibo").attr('href',BASE_URL+'/reporte/inscrito/actividad/'+response.data.id);
            }else if(response.status == 500){
                $("#body-notify").html("");
                $("#body-notify").html('<div class="alert alert-warning"><ul>');
                for(var i in response.msg){
                    $("#body-notify").append('<li>'+response.msg[i]+'</li>');
                }
                $("#body-notify").html('</ul></div>');
            }
            $("#modal-notify").modal('show');
        }
    });
}
function getCost(valor,total){
    var percent = (valor/100)*total;
    return (total - percent)>0?total - percent:0;
}
;
